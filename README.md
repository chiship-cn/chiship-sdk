# chiship-sdk

#### 介绍
  当前版本为:4.1
  版本号说明：第一位代表开源年数，第二位代表本年迭代次数

  Chiship SDK是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率。
  
  包含以下组件：

    1.chiship-sdk-all         所有包聚合依赖
    2.chiship-sdk-core        Chiship公共资源
    3.chiship-sdk-framework   框架
    4.chiship-sdk-cache       缓存
    5.chiship-sdk-third       三方组件
    6.chiship-sdk-pay         聚合支付组件
    7.chiship-swagger-ui      Swagger-UI二次封装
    

#### 安装教程

1. 使用任何一个组件，必须依赖注入核心代码包(chiship-sdk-core)
2. 若组件采用配置文件的形式注入，则将application.properties自行放入到自己的项目中，并按照模板进行配置各项参数
3. redis.properties配合core包RedisUtil工具类使用
#### 命令
mvn install -P release
mvn clean deploy -P release

#### .properties说明

1. application.properties

   （1）.邮箱配置

   ```
   EMAIL_CONFIG_HOST=#####                      要连接的SMTP服务器
   EMAIL_CONFIG_TRANSPORT_PROTOCOL=#####        要装入session的协议（smtp、pop3、imap、nntp）
   EMAIL_CONFIG_SMTP_AUTH=#####                 缺省是false，如果为true，尝试使用AUTH命令认证用户
   EMAIL_CONFIG_SMTP_PORT=#####                 要连接的SMTP服务器的端口号，如果connect没有指明端口号就使用它，缺省值25
   EMAIL_CONFIG_IS_SSL=#####                    是否开启安全协议  默认false
   EMAIL_CONFIG_SMTP_SSL_PORT=#####             安全协议端口
   EMAIL_CONFIG_SEND_ADDRESS=#####              SMTP的缺省用户名
   EMAIL_CONFIG_SEND_ADDRESS_PASSWORD=#####     SMTP的缺省用户密码(AES加密后的)
   ```

   （2）.云存储
   
   ```
   #阿里云存储
   chiship.third.oss.ali.accessKey=******
   chiship.third.oss.ali.accessSecret=******
   chiship.third.oss.ali.endPort=oss-cn-shanghai.aliyuncs.com
   chiship.third.oss.ali.buckName=chiship
   chiship.third.oss.ali.root=chiship
   #腾讯云存储
   chiship.third.oss.tencent.accessKey=******
   chiship.third.oss.tencent.accessSecret=******
   chiship.third.oss.tencent.endPort=ap-beijing
   chiship.third.oss.tencent.buckName=chiship-1251388006
   chiship.third.oss.tencent.root=chiship
    ```

    （3）.支付
    
    ```
      #通知主机
      chiship.pay.domain.notify=${服务主机}/pay
      #商戶主体
      chiship.pay.ali.mch-entity=*******
      #支付宝应用
      chiship.pay.ali.app-id=*******
      #支付宝公钥
      chiship.pay.ali.public-key=*******
      #支付宝应用私钥
      chiship.pay.ali.primary-key=*******
      #微信
      #商户主体
      chiship.pay.wx.mch-entity=*******
      #商户号
      chiship.pay.wx.mch-id=*******
      #v3秘钥
      chiship.pay.wx.v3.key=*******
      #证书序列号，v3支付
      chiship.pay.wx.v3.serial-number=*******
      #支付证书私钥串,v3支付
      chiship.pay.wx.v3.primary-key=*******
      #微信APP_ID(小程序、公众号、APP)
      chiship.pay.wx.app-id=*******
      chiship.pay.wx.app-id2=*******
      chiship.pay.wx.app-id3=*******
    ```
    ```
      通知完整路径自动拼接，拼接规则：通知主机+各种异步通知
      TRADE_TYPE参数是支付方式，通过TradeTypeEnum枚举类获得    
   
      微信支付异步通知
      /wxV3/payNotify/{TRADE_TYPE}
      微信退款异步通知
      /wxV3/refundNotify
      支付宝支付异步通知
      /ali/payNotify/{TRADE_TYPE}
    ```   
   （4）.其他
   
   ```
   #云短信
   chiship.third.sms.ali.accessKey=******
   chiship.third.sms.ali.accessSecret=******
   
   #百度AI对接
   chiship.third.ai.baidu.appId=******
   chiship.third.ai.baidu.apiKey=******
   chiship.third.ai.baidu.secretKey=******
   
   #钉钉对接
   chiship.third.ding-talk.server-domain=${服务主机}/sso
   #Client ID
   chiship.third.ding-talk.app-key=******
   #Client Secret
   chiship.third.ding-talk.app-secret=******
   #企业corpId
   chiship.third.ding-talk.corp-id=******
   #插件的appId
   chiship.third.ding-talk.mini-app-id=******
   #原企业内部应用ID
   chiship.third.ding-talk.agent-id=******
   
   #支付宝小程序
   chiship.third.zfb-mp.server-domain=${服务主机}/zfbMp
   chiship.third.zfb-mp.app-id=******
   chiship.third.zfb-mp.private-key=******
   chiship.third.zfb-mp.public-key=******
   chiship.third.zfb-mp.aes-decrypt-key=******
   
   #微信公众号
   chiship.third.wx-pub.server-domain=${服务主机}/wxPub
   chiship.third.wx-pub.app-key=******
   chiship.third.wx-pub.app-secret=******
   
   #微信小程序
   chiship.third.wx-mini.app-key=******
   chiship.third.wx-mini.app-secret=******
   
   #企业微信
   chiship.third.wx-work.server-domain=${服务主机}/sso/wxWork
   chiship.third.wx-work.corp-id=******
   chiship.third.wx-work.app-key=******
   chiship.third.wx-work.app-secret=******
   
   #萤石
   chiship.third.ys.app-key=******
   chiship.third.ys.app-secret=******
   ```
2. 部分代码示例

  （1）.云存储
   ```
    1.构建句柄，默认配置支付或通过DfsDTO构造，ALI、TENCENT、BAI_DU
    2.通过句柄调用各种方法，比如上传upload
    FileStorageService fileStorageService = DfsFactory.getFileStorageService(DfsEnum.ALI);
    fileStorageService.upload(InputStream, fileName);
   ```

  （2）.支付
   ```
    1.构建句柄，默认配置支付或通过PayDTO构造，PAY_WX_V3、PAY_ZFB
    2.通过句柄调用各种方法，比如支付doPay
    PaymentService paymentService = PayFactory.getPaymentService(PayEnum.PAY_WX_V3);
    paymentService.doPay(PayParamsModel, TradeTypeEnum.QR_CODE);
   ```
