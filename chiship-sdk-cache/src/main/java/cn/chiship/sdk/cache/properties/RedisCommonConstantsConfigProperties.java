package cn.chiship.sdk.cache.properties;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.PropertiesFileUtil;
import cn.chiship.sdk.core.util.StringUtil;
import org.springframework.stereotype.Component;

import java.util.MissingResourceException;

/**
 * 常量配置
 *
 * @author LiJian
 */
@Component
public class RedisCommonConstantsConfigProperties {

	private String prefix = "chiship.";

	private static RedisCommonConstantsConfigProperties instance;

	private static PropertiesFileUtil propertiesFileUtil;

	private RedisCommonConstantsConfigProperties() {

	}

	static {
		try {
			propertiesFileUtil = PropertiesFileUtil.getInstance("application");
		}
		catch (MissingResourceException e) {
			throw new BusinessException("兄弟,请确保'application.properties'文件存在!");
		}
	}

	public static synchronized RedisCommonConstantsConfigProperties getInstance() {
		if (null == instance) {
			instance = new RedisCommonConstantsConfigProperties();
		}
		return instance;
	}

	/**
	 * 获得项目名称
	 * <p>
	 * Value("${chiship.name}")
	 */
	public String getProjectName() {
		String projectName = null;
		projectName = propertiesFileUtil.get(prefix + "name");
		if (StringUtil.isNullOrEmpty(projectName)) {
			throw new BusinessException("请配置{" + prefix + "name}属性");
		}
		return projectName;
	}

}
