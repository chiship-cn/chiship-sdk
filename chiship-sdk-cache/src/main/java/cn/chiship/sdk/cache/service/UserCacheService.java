package cn.chiship.sdk.cache.service;

import cn.chiship.sdk.cache.vo.CacheUserVO;

/**
 * @author LiJian
 */
public interface UserCacheService {

	/**
	 * 获取令牌
	 * @return 令牌
	 */
	String getToken();

	/**
	 * 根据模块名获取当期用户
	 * @return 用户
	 */
	CacheUserVO getUser();

	/**
	 * 根据模块名与令牌获取当期用户
	 * @param token 令牌
	 * @return 用户
	 */
	CacheUserVO getUserByToken(String token);

	/**
	 * 缓存用户
	 * @param user 用户
	 * @return 标识
	 */
	boolean cacheUser(CacheUserVO user);

	/**
	 * 缓存用户
	 * @param token 令牌
	 * @param user 用户
	 * @return 标识
	 */
	boolean cacheUser(String token, CacheUserVO user);

	/**
	 * 移除用户
	 * @return 标识
	 */
	boolean removeUser();

	/**
	 * 根据令牌移除用户
	 * @param token 令牌
	 * @return 标识
	 */
	boolean removeUserByToken(String token);

	/**
	 * 验证是否有效
	 * @return 标识
	 */
	boolean isValid();

	/**
	 * 验证是否有效
	 * @param token 令牌
	 * @return 标识
	 */
	boolean isValid(String token);

	/**
	 * 移除所有令牌
	 */
	void removeUserAll();

}
