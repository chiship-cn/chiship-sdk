package cn.chiship.sdk.cache.util;

import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.enums.UserTypeEnum;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LiJian
 */
public class JwtUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(JwtUtil.class);

	/**
	 * Token默认失效时间（24小时=1440分钟）
	 */
	public static final int TIMEOUT_MINUTES = 1440;

	public static final String SECRET_KEY = "JWT_SECRET";

	private JwtUtil() {
	}

	/**
	 * 创建失效时间为TIMEOUT_MINUTES分钟的TOKEN
	 * @param cacheUserVO 当前登录用户
	 * @return 返回token
	 */
	public static String createToken(CacheUserVO cacheUserVO) {
		return createToken(cacheUserVO, TIMEOUT_MINUTES);
	}

	/**
	 * 生成Token
	 * @param timeoutMinutes 超时时间，单位：分钟
	 * @param cacheUserVO 用户信息
	 * @return 返回token
	 */
	public static String createToken(CacheUserVO cacheUserVO, Integer timeoutMinutes) {

		// 指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		// 当前时间
		long currentMills = System.currentTimeMillis();

		// 附加用户信息
		Map<String, Object> claims = new HashMap<>(2);
		claims.put("id", cacheUserVO.getId());
		claims.put("username", cacheUserVO.getUsername());

		//
		JwtBuilder builder = Jwts.builder()
				// 设置附加信息
				.setClaims(claims)
				// 设置当前登录用户 id
				.setId(new SnowflakeIdUtil(10L, 20L).nextId() + "")
				// 生成的时间
				.setIssuedAt(new Date(currentMills)).setSubject(cacheUserVO.getUsername())
				.signWith(signatureAlgorithm, SECRET_KEY);

		// 设置有效期
		if (null == timeoutMinutes || timeoutMinutes < 1) {
			timeoutMinutes = TIMEOUT_MINUTES;
		}
		builder.setExpiration(new Date(currentMills + (timeoutMinutes * 60 * 1000)));

		return builder.compact();
	}

	/**
	 * 解析Token
	 * @param token 令牌
	 * @return 返回用户名
	 */
	public static CacheUserVO parseToken(String token) {
		try {
			// 得到DefaultJwtParser
			Claims claims = Jwts.parser()
					// 设置签名的秘钥
					.setSigningKey(SECRET_KEY)
					// 设置需要解析的jwt
					.parseClaimsJws(token).getBody();

			CacheUserVO cacheUserVo = new CacheUserVO(UserTypeEnum.ADMIN);
			cacheUserVo.setId(claims.get("id").toString());
			cacheUserVo.setUsername(claims.get("username").toString());
			return cacheUserVo;

		}
		catch (Exception e) {
			LOGGER.error("发生异常", e);
			return null;
		}
	}

}
