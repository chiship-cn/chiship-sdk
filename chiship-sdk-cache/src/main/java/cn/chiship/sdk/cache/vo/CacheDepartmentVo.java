package cn.chiship.sdk.cache.vo;

import java.io.Serializable;

/**
 * @author lijian
 */
public class CacheDepartmentVo implements Serializable {

	/**
	 * 企业唯一标识
	 */
	private String corpId;

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 上级
	 */
	private String pid;

	/**
	 * 部门编号
	 */
	private String departmentCode;

	/**
	 * 部门名称
	 */
	private String departmentName;

	/**
	 * 部门LOGO
	 */
	private String logo;

	/**
	 * 祖级列表
	 */
	private String treeNumber;

	public CacheDepartmentVo() {
	}

	public CacheDepartmentVo(String corpId,String id, String departmentCode, String departmentName, String logo, String treeNumber) {
		this.corpId=corpId;
		this.id = id;
		this.pid = "0";
		this.departmentCode = departmentCode;
		this.departmentName = departmentName;
		this.logo = logo;
		this.treeNumber = treeNumber;
	}

	public CacheDepartmentVo(String corpId,String id, String pid, String departmentCode, String departmentName, String logo,
			String treeNumber) {
		this.corpId=corpId;
		this.id = id;
		this.pid = pid;
		this.departmentCode = departmentCode;
		this.departmentName = departmentName;
		this.logo = logo;
		this.treeNumber = treeNumber;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getTreeNumber() {
		return treeNumber;
	}

	public void setTreeNumber(String treeNumber) {
		this.treeNumber = treeNumber;
	}

	@Override
	public String toString() {
		return "CacheDepartmentVo{" + "id='" + id + '\'' + ", pid='" + pid + '\'' + ", departmentCode='"
				+ departmentCode + '\'' + ", departmentName='" + departmentName + '\'' + ", logo='" + logo + '\''
				+ ", treeNumber='" + treeNumber + '\'' + '}';
	}

}
