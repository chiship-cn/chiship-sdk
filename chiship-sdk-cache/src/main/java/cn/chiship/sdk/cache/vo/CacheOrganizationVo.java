package cn.chiship.sdk.cache.vo;

import java.io.Serializable;

/**
 * @author lijian
 */
public class CacheOrganizationVo implements Serializable {

    /**
     * 企业唯一标识
     */
    private String corpId;

    /**
     * 主键
     */
    private String id;

    /**
     * 上级
     */
    private String pid;

    /**
     * 组织编号
     */
    private String code;

    /**
     * 组织简称
     */
    private String name;

    /**
     * 祖级列表
     */
    private String treeNumber;

    /**
     * 组织LOGO
     */
    private String logo;

    /**
     * 组织隶属层级
     */
    private String level;

    /**
     * 组织类型
     */
    private Byte type;

    /**
     * 组织分类
     */
    private String category;

    /**
     * 组织标签
     */
    private String tag;

    /**
     * 企业负责人
     */
    private String chargePerson;

    /**
     * 省
     */
    private Long regionLevel1;

    /**
     * 市
     */
    private Long regionLevel2;

    /**
     * 县/区
     */
    private Long regionLevel3;

    /**
     * 街道
     */
    private Long regionLevel4;

    /**
     * 地区
     */
    private String regionAddress;

    /**
     * 扩展
     */
    private Object ext;

    public CacheOrganizationVo() {
    }

    public CacheOrganizationVo(String corpId, String id, String code, String name, Byte type, String level, String category,
                               String tag, String treeNumber, String logo, String chargePerson, Long regionLevel1, Long regionLevel2,
                               Long regionLevel3, Long regionLevel4, String regionAddress, Object ext) {
        this.corpId = corpId;
        this.id = id;
        this.pid = "0";
        this.code = code;
        this.name = name;
        this.treeNumber = treeNumber;
        this.logo = logo;
        this.level = level;
        this.type = type;
        this.category = category;
        this.tag = tag;
        this.chargePerson = chargePerson;
        this.regionLevel1 = regionLevel1;
        this.regionLevel2 = regionLevel2;
        this.regionLevel3 = regionLevel3;
        this.regionLevel4 = regionLevel4;
        this.regionAddress = regionAddress;
        this.ext = ext;
    }

    public CacheOrganizationVo(String corpId, String id, String pid, String code, String name, Byte type, String level,
                               String category, String tag, String treeNumber, String logo, String chargePerson, Long regionLevel1,
                               Long regionLevel2, Long regionLevel3, Long regionLevel4, String regionAddress, Object ext) {
        this.corpId = corpId;
        this.id = id;
        this.pid = pid;
        this.code = code;
        this.name = name;
        this.treeNumber = treeNumber;
        this.logo = logo;
        this.level = level;
        this.type = type;
        this.category = category;
        this.tag = tag;
        this.chargePerson = chargePerson;
        this.regionLevel1 = regionLevel1;
        this.regionLevel2 = regionLevel2;
        this.regionLevel3 = regionLevel3;
        this.regionLevel4 = regionLevel4;
        this.regionAddress = regionAddress;
        this.ext = ext;
    }

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTreeNumber() {
        return treeNumber;
    }

    public void setTreeNumber(String treeNumber) {
        this.treeNumber = treeNumber;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getChargePerson() {
        return chargePerson;
    }

    public void setChargePerson(String chargePerson) {
        this.chargePerson = chargePerson;
    }

    public Long getRegionLevel1() {
        return regionLevel1;
    }

    public void setRegionLevel1(Long regionLevel1) {
        this.regionLevel1 = regionLevel1;
    }

    public Long getRegionLevel2() {
        return regionLevel2;
    }

    public void setRegionLevel2(Long regionLevel2) {
        this.regionLevel2 = regionLevel2;
    }

    public Long getRegionLevel3() {
        return regionLevel3;
    }

    public void setRegionLevel3(Long regionLevel3) {
        this.regionLevel3 = regionLevel3;
    }

    public Long getRegionLevel4() {
        return regionLevel4;
    }

    public void setRegionLevel4(Long regionLevel4) {
        this.regionLevel4 = regionLevel4;
    }

    public String getRegionAddress() {
        return regionAddress;
    }

    public void setRegionAddress(String regionAddress) {
        this.regionAddress = regionAddress;
    }

    public Object getExt() {
        return ext;
    }

    public void setExt(Object ext) {
        this.ext = ext;
    }

    @Override
    public String toString() {
        return "CacheOrganizationVo{" + "id='" + id + '\'' + ", pid='" + pid + '\'' + ", code='" + code + '\''
                + ", name='" + name + '\'' + ", treeNumber='" + treeNumber + '\'' + ", logo='" + logo + '\''
                + ", level='" + level + '\'' + ", type='" + type + '\'' + ", category='" + category + '\'' + ", tag='"
                + tag + '\'' + ", chargePerson='" + chargePerson + '\'' + ", regionLevel1=" + regionLevel1
                + ", regionLevel2=" + regionLevel2 + ", regionLevel3=" + regionLevel3 + ", regionLevel4=" + regionLevel4
                + ", regionAddress='" + regionAddress + '\'' + ", ext=" + ext + '}';
    }

}
