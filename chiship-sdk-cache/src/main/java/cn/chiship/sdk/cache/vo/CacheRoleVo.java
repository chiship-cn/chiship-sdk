package cn.chiship.sdk.cache.vo;

import java.io.Serializable;

/**
 * 登录用户所属角色视图
 *
 * @author LiJian
 */
public class CacheRoleVo implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 角色编号
	 */
	private String roleCode;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 角色类型
	 */
	private Byte roleType;

	/**
	 * 数据范围
	 */
	private Byte dataScope;

	public CacheRoleVo() {
	}

	public CacheRoleVo(String id, String roleCode, String roleName, Byte roleType) {
		this(id, roleCode, roleName, roleType, null);
	}

	public CacheRoleVo(String id, String roleCode, String roleName, Byte roleType, Byte dataScope) {
		this.id = id;
		this.roleCode = roleCode;
		this.roleName = roleName;
		this.roleType = roleType;
		this.dataScope = dataScope;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Byte getRoleType() {
		return roleType;
	}

	public void setRoleType(Byte roleType) {
		this.roleType = roleType;
	}

	public Byte getDataScope() {
		return dataScope;
	}

	public void setDataScope(Byte dataScope) {
		this.dataScope = dataScope;
	}

}
