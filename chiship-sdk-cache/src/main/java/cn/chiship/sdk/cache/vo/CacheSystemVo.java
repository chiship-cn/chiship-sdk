package cn.chiship.sdk.cache.vo;

import java.io.Serializable;

/**
 * @author lijian
 */
public class CacheSystemVo implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 系统编号
	 */
	private String systemCode;

	/**
	 * 系统名称
	 */
	private String systemName;

	/**
	 * 扩展信息
	 */
	private Object extInfo;

	public CacheSystemVo() {
	}

	public CacheSystemVo(String id, String systemCode, String systemName, Object extInfo) {
		this.id = id;
		this.systemCode = systemCode;
		this.systemName = systemName;
		this.extInfo = extInfo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public Object getExtInfo() {
		return extInfo;
	}

	public void setExtInfo(Object extInfo) {
		this.extInfo = extInfo;
	}

	@Override
	public String toString() {
		return "CacheSystemVo{" + "id=" + id + ", systemCode='" + systemCode + '\'' + ", systemName='" + systemName
				+ '\'' + ", extInfo=" + extInfo + '}';
	}

}
