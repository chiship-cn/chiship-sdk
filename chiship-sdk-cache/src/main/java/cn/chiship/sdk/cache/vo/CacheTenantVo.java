package cn.chiship.sdk.cache.vo;

import java.io.Serializable;

/**
 * @author lj 租户
 */
public class CacheTenantVo implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 租户统一社会信用代码
	 */
	private String socialCreditCode;

	/**
	 * 租户名称
	 */
	private String name;

	/**
	 * 租户法人
	 */
	private String realName;

	/**
	 * 租户法人手机号
	 */
	private String mobile;

	/**
	 * 租户级别
	 */
	private String level;

	/**
	 * 租户类型
	 */
	private Byte type;

	public CacheTenantVo() {
	}

	public CacheTenantVo(String id, String socialCreditCode, String name, String realName, String mobile,
			String level) {
		this.id = id;
		this.socialCreditCode = socialCreditCode;
		this.name = name;
		this.realName = realName;
		this.mobile = mobile;
		this.level = level;
	}

	public CacheTenantVo(String id, String socialCreditCode, String name, String realName, String mobile, String level,
			Byte type) {
		this.id = id;
		this.socialCreditCode = socialCreditCode;
		this.name = name;
		this.realName = realName;
		this.mobile = mobile;
		this.level = level;
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSocialCreditCode() {
		return socialCreditCode;
	}

	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

}
