
package cn.chiship.sdk.cache.vo;

import cn.chiship.sdk.core.enums.UserTypeEnum;

import java.io.Serializable;
import java.util.List;

/**
 * @author LiJian
 */
public class CacheUserVO implements Serializable {

	/**
	 * 会话ID
	 */
	private String sessionId;

	/**
	 * 用户类型
	 */
	private UserTypeEnum userTypeEnum;

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 头像
	 */
	private String avatar;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 昵称
	 */
	private String nickName;

	/**
	 * 真实姓名
	 */
	private String realName;

	/**
	 * 性别 1男 2女 0保密
	 */
	private Byte gender;

	/**
	 * 手机
	 */
	private String mobile;

	/**
	 * 手机2
	 */
	private String mobile2;

	/**
	 * 座机
	 */
	private String phone;

	/**
	 * 工作
	 */
	private String job;

	/**
	 * 电子邮箱
	 */
	private String email;

	/**
	 * 用户编号
	 */
	private String userCode;

	/**
	 * 状态
	 */
	private Byte status;

	/**
	 * 是否是超级管理员
	 */
	private Boolean isAdmin;

	/**
	 * 是否需要修改密码
	 */
	private Boolean isInitPassword;

	/**
	 * 二级用户类型 若没有则忽略
	 */
	private Byte subUserType;

	/**
	 * 姓名拼音
	 */
	private String userPinyin;

	/**
	 * 注册时间
	 */
	private Long registerTime;

	/**
	 * 会员ID，若类本省就是会员，此id可不用赋值
	 */
	private String memberId;

	/**
	 * 扩展信息
	 */
	private Object extInfo;

	/**
	 * 用户所属角色 当系统中设计用户只有一个角色时用
	 */
	private CacheRoleVo cacheRoleVo;

	/**
	 * 租户实体
	 */
	private CacheTenantVo cacheTenantVo;

	/**
	 * 用户所属角色集合 当系统中设计用户拥有多个角色时用
	 */
	private List<CacheRoleVo> cacheRoleVos;

	/**
	 * 用户所属组织 当系统中设计用户只有一个组织时用
	 */
	private CacheOrganizationVo cacheOrganizationVo;

	/**
	 * 用户所属组织集合 当系统中设计用户拥有多个组织时用
	 */
	private List<CacheOrganizationVo> cacheOrganizationVos;

	/**
	 * 用户所属组织 当系统中设计用户只有一个组织时用
	 */
	private CacheDepartmentVo cacheDepartmentVo;

	/**
	 * 用户所属组织集合 当系统中设计用户拥有多个组织时用
	 */
	private List<CacheDepartmentVo> cacheDepartmentVos;

	/**
	 * 用户所属系统集合
	 */
	private List<CacheSystemVo> cacheSystemVos;

	/**
	 * 用户权限集合
	 */
	private List<String> perms;

	/**
	 * 用户菜单集合
	 */
	private List<Object> menuPerms;

	/**
	 * 下级部门集合
	 */
	private List<Object> subordinateDept;

	public CacheUserVO() {
	}

	public CacheUserVO(UserTypeEnum userTypeEnum) {
		this.userTypeEnum = userTypeEnum;
	}

	public CacheUserVO(UserTypeEnum userTypeEnum, String id, String avatar, String username, String realName,
			String mobile, String email, String userCode, Byte status, Boolean isAdmin, Boolean isInitPassword,
			Byte userType) {
		this(null, userTypeEnum, id, avatar, username, realName, mobile, email, userCode, status, isAdmin,
				isInitPassword, userType);
	}

	public CacheUserVO(String sessionId, UserTypeEnum userTypeEnum, String id, String avatar, String username,
			String realName, String mobile, String email, String userCode, Byte status, Boolean isAdmin,
			Boolean isInitPassword, Byte subUserType) {
		this.sessionId = sessionId;
		this.userTypeEnum = userTypeEnum;
		this.id = id;
		this.avatar = avatar;
		this.username = username;
		this.realName = realName;
		this.mobile = mobile;
		this.email = email;
		this.userCode = userCode;
		this.status = status;
		this.isAdmin = isAdmin;
		this.isInitPassword = isInitPassword;
		this.subUserType = subUserType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public UserTypeEnum getUserTypeEnum() {
		return userTypeEnum;
	}

	public void setUserTypeEnum(UserTypeEnum userTypeEnum) {
		this.userTypeEnum = userTypeEnum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Byte getGender() {
		return gender;
	}

	public void setGender(Byte gender) {
		this.gender = gender;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Boolean getAdmin() {
		return isAdmin;
	}

	public void setAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public Boolean getInitPassword() {
		return isInitPassword;
	}

	public void setInitPassword(Boolean initPassword) {
		isInitPassword = initPassword;
	}

	public Byte getSubUserType() {
		return subUserType;
	}

	public void setSubUserType(Byte subUserType) {
		this.subUserType = subUserType;
	}

	public String getUserPinyin() {
		return userPinyin;
	}

	public void setUserPinyin(String userPinyin) {
		this.userPinyin = userPinyin;
	}

	public Long getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Long registerTime) {
		this.registerTime = registerTime;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Object getExtInfo() {
		return extInfo;
	}

	public void setExtInfo(Object extInfo) {
		this.extInfo = extInfo;
	}

	public CacheRoleVo getCacheRoleVo() {
		return cacheRoleVo;
	}

	public void setCacheRoleVo(CacheRoleVo cacheRoleVo) {
		this.cacheRoleVo = cacheRoleVo;
	}

	public CacheTenantVo getCacheTenantVo() {
		return cacheTenantVo;
	}

	public void setCacheTenantVo(CacheTenantVo cacheTenantVo) {
		this.cacheTenantVo = cacheTenantVo;
	}

	public List<CacheRoleVo> getCacheRoleVos() {
		return cacheRoleVos;
	}

	public void setCacheRoleVos(List<CacheRoleVo> cacheRoleVos) {
		this.cacheRoleVos = cacheRoleVos;
	}

	public CacheOrganizationVo getCacheOrganizationVo() {
		return cacheOrganizationVo;
	}

	public void setCacheOrganizationVo(CacheOrganizationVo cacheOrganizationVo) {
		this.cacheOrganizationVo = cacheOrganizationVo;
	}

	public List<CacheOrganizationVo> getCacheOrganizationVos() {
		return cacheOrganizationVos;
	}

	public void setCacheOrganizationVos(List<CacheOrganizationVo> cacheOrganizationVos) {
		this.cacheOrganizationVos = cacheOrganizationVos;
	}

	public CacheDepartmentVo getCacheDepartmentVo() {
		return cacheDepartmentVo;
	}

	public void setCacheDepartmentVo(CacheDepartmentVo cacheDepartmentVo) {
		this.cacheDepartmentVo = cacheDepartmentVo;
	}

	public List<CacheDepartmentVo> getCacheDepartmentVos() {
		return cacheDepartmentVos;
	}

	public void setCacheDepartmentVos(List<CacheDepartmentVo> cacheDepartmentVos) {
		this.cacheDepartmentVos = cacheDepartmentVos;
	}

	public List<CacheSystemVo> getCacheSystemVos() {
		return cacheSystemVos;
	}

	public void setCacheSystemVos(List<CacheSystemVo> cacheSystemVos) {
		this.cacheSystemVos = cacheSystemVos;
	}

	public List<String> getPerms() {
		return perms;
	}

	public void setPerms(List<String> perms) {
		this.perms = perms;
	}

	public List<Object> getMenuPerms() {
		return menuPerms;
	}

	public void setMenuPerms(List<Object> menuPerms) {
		this.menuPerms = menuPerms;
	}

	public List<Object> getSubordinateDept() {
		return subordinateDept;
	}

	public void setSubordinateDept(List<Object> subordinateDept) {
		this.subordinateDept = subordinateDept;
	}

	@Override
	public String toString() {
		return "CacheUserVO{" + "sessionId='" + sessionId + '\'' + ", userTypeEnum=" + userTypeEnum + ", id=" + id
				+ ", avatar='" + avatar + '\'' + ", username='" + username + '\'' + ", nickName='" + nickName + '\''
				+ ", realName='" + realName + '\'' + ", gender=" + gender + ", mobile='" + mobile + '\'' + ", mobile2='"
				+ mobile2 + '\'' + ", phone='" + phone + '\'' + ", job='" + job + '\'' + ", email='" + email + '\''
				+ ", userCode='" + userCode + '\'' + ", status=" + status + ", isAdmin=" + isAdmin + ", isInitPassword="
				+ isInitPassword + ", subUserType=" + subUserType + ", userPinyin='" + userPinyin + '\''
				+ ", registerTime=" + registerTime + ", memberId=" + memberId + ", extInfo=" + extInfo
				+ ", cacheRoleVo=" + cacheRoleVo + ", cacheTenantVo=" + cacheTenantVo + ", cacheRoleVos=" + cacheRoleVos
				+ ", cacheOrganizationVo=" + cacheOrganizationVo + ", cacheOrganizationVos=" + cacheOrganizationVos
				+ ", cacheDepartmentVo=" + cacheDepartmentVo + ", cacheDepartmentVos=" + cacheDepartmentVos
				+ ", cacheSystemVos=" + cacheSystemVos + ", perms=" + perms + ", menuPerms=" + menuPerms
				+ ", subordinateDept=" + subordinateDept + '}';
	}

}
