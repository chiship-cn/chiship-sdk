package cn.chiship.sdk.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 在Controller的方法上使用此注解，该方法在映射时会对请求不会进行密钥验证
 * </p>
 * <p>
 * 也可以直接在Controller上使用，代表该Controller的所有方法均忽略密钥验证
 * </p>
 * @author lj
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NoVerificationAppIdAndKey {

}
