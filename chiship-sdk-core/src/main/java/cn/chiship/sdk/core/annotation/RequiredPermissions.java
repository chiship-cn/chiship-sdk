package cn.chiship.sdk.core.annotation;

import cn.chiship.sdk.core.enums.PermissionsEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限集合
 * @author lijian
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredPermissions {

	String[] value();

	PermissionsEnum logical() default PermissionsEnum.AND;

}
