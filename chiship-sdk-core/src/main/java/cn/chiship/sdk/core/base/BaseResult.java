package cn.chiship.sdk.core.base;

import cn.chiship.sdk.core.enums.BaseResultEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 统一返回结果类
 *
 * @author 李剑
 */
@ApiModel(value = "统一返回结果实体")
public class BaseResult implements Serializable {

	/**
	 * 是否成功
	 */
	@ApiModelProperty(value = "是否成功")
	private boolean success;

	/**
	 * 状态码：200成功，其他为失败
	 */
	@ApiModelProperty(value = "状态码")
	private int code;

	/**
	 * 成功为success，其他为失败原因
	 */
	@ApiModelProperty(value = "状态描述")
	private String message;

	/**
	 * 数据结果集
	 */
	@ApiModelProperty(value = "数据结果集")
	private Object data;

	public BaseResult() {
	}

	public BaseResult(boolean success, int code, String message, Object data) {
		this.success = success;
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public BaseResult(Boolean success, BaseResultEnum baseResultEnum, Object data) {
		this.success = success;
		this.code = baseResultEnum.getCode();
		this.message = baseResultEnum.getMessage();
		this.data = data;
	}

	public static BaseResult ok() {
		return new BaseResult(Boolean.TRUE, BaseResultEnum.SUCCESS, null);
	}

	public static BaseResult ok(Object data) {
		return new BaseResult(Boolean.TRUE, BaseResultEnum.SUCCESS, data);
	}

	public static BaseResult error() {
		return new BaseResult(Boolean.FALSE, BaseResultEnum.FAILED, null);
	}

	public static BaseResult error(Object data) {
		return new BaseResult(Boolean.FALSE, BaseResultEnum.FAILED, data);
	}

	public static BaseResult error(BaseResultEnum baseResultEnum, Object data) {
		return new BaseResult(Boolean.FALSE, baseResultEnum, data);
	}

	public static BaseResult error(int code, String message, Object data) {
		return new BaseResult(Boolean.FALSE, code, message, data);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getCode() {

		return code;

	}

	public void setCode(int code) {

		this.code = code;

	}

	public String getMessage() {

		return message;

	}

	public void setMessage(String message) {

		this.message = message;

	}

	public Object getData() {

		return data;

	}

	public void setData(Object data) {

		this.data = data;

	}

	@Override
	public String toString() {
		return "{" + "\"success\":" + success + ", \"code\":" + code + ", \"message\":\"" + message + '\"'
				+ ", \"data\":" + JSON.toJSONString(data) + '}';
	}

}
