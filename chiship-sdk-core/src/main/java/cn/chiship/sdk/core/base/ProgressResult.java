package cn.chiship.sdk.core.base;

import cn.chiship.sdk.core.enums.ProgressResultEnum;
import cn.chiship.sdk.core.util.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author lijian 进度视图
 */
@ApiModel(value = "进度视图结果实体")
public class ProgressResult implements Serializable {

    @ApiModelProperty(value = "是否成功")
    private boolean success;

    @ApiModelProperty(value = "进度编码")
    private String progress;

    @ApiModelProperty(value = "进度信息")
    private String message;

    @ApiModelProperty(value = "进度详细描述")
    private String describe;

    /**
     * 进度数值
     */
    private Double progressValue;

    /**
     * 异步下载使用
     */
    private boolean isFile;

    private byte[] bytes;

    private String fileName;

    private String exportType;

    private ProgressResult(boolean success, String progress, String message, String describe) {
        this.success = success;
        this.progress = progress;
        this.message = message;
        this.describe = describe;
    }

    private ProgressResult(boolean success, String progress, String message, String describe, String exportType,
                           boolean isFile, String fileName, byte[] bytes) {
        this.success = success;
        this.progress = progress;
        this.message = message;
        this.describe = describe;
        this.exportType = exportType;
        this.isFile = isFile;
        this.fileName = fileName;
        this.bytes = bytes;
    }

    public static ProgressResult out(ProgressResultEnum resultEnum, String describe) {
        boolean success = true;
        if (resultEnum.equals(ProgressResultEnum.PROGRESS_ERROR_ENUM)) {
            success = false;
        }
        return new ProgressResult(success, resultEnum.getCode(), resultEnum.getMessage(), describe);
    }

    public static ProgressResult out(ProgressResultEnum resultEnum) {
        boolean success = true;
        String describe = "任务处理中...";
        if (resultEnum.equals(ProgressResultEnum.PROGRESS_ERROR_ENUM)) {
            success = false;
        }
        if (resultEnum.equals(ProgressResultEnum.PROGRESS_SEARCH_DATABASE_ENUM)) {
            describe = "数据库查询中...";
        }
        if (resultEnum.equals(ProgressResultEnum.PROGRESS_FILE_ASSEMBLY_ENUM)) {
            describe = "文件组装中,请耐心等待...";
        }
        return new ProgressResult(success, resultEnum.getCode(), resultEnum.getMessage(), describe);
    }

    public static ProgressResult out(String exportType, String fileName, byte[] bytes) {
        ProgressResultEnum resultEnum = ProgressResultEnum.PROGRESS_STREAM_ENUM;
        return new ProgressResult(true, resultEnum.getCode(), resultEnum.getMessage(), null, exportType, true, fileName,
                bytes);
    }

    /**
     * 输出进度
     *
     * @param value    进度值
     * @param describe 进度描述
     * @return ProgressResult
     */
    public static ProgressResult out(Double value, String describe) {
        if (StringUtil.isNull(value)) {
            value = 100D;
        }
        ProgressResultEnum resultEnum = ProgressResultEnum.PROGRESS_PROGRESS_ENUM;
        ProgressResult progressResult = new ProgressResult(
                true,
                resultEnum.getCode(),
                resultEnum.getMessage(),
                describe);
        progressResult.setProgressValue(value);
        return progressResult;
    }

    /**
     * ProgressResult.out(ProgressResultEnum.PROGRESS_PROGRESS_ENUM, "解析出" + dataResult.size()
     * + "条数据！已完成：" + (StringUtil.formatPercent(finishCount++, dataResult.size())) + "%！"))
     *
     * @return
     */

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Double getProgressValue() {
        return progressValue;
    }

    public void setProgressValue(Double progressValue) {
        this.progressValue = progressValue;
    }

    public String getExportType() {
        return exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

}
