package cn.chiship.sdk.core.base.constants;

/**
 * 全局缓存常量
 *
 * @author lijian
 */
public class BaseCacheConstants {

    /**
     * 存放AppKey的Request Key
     */
    public static final String REDIS_APP_KEY_PREFIX = "AUTHORIZATION_APP_KEY";

    /**
     * Redis中token的前缀
     */
    public static final String REDIS_TOKEN_PREFIX = "AUTHORIZATION_TOKEN_";

    /**
     * 安全
     */
    public static final String REDIS_SECURITY_PREFIX = "SECURITY";

    /**
     * 白名单
     */
    public static final String REDIS_WHITE_IP_PREFIX = "WHITE_IP";

    /**
     * 密钥
     */
    public static final String REDIS_PROOF_PREFIX = "PROOF";

    /**
     * 系统
     */
    public static final String REDIS_SYSTEM_PREFIX = "SYSTEM";

    /**
     * 站点信息
     */
    public static final String REDIS_WEB_SITE_PREFIX = "WEB_SITE";

    /**
     * 登录二维码信息
     */
    public static final String REDIS_QRCODE_LOGIN_PREFIX = "QRCODE_LOGIN";

    /**
     * 地区
     */
    public static final String REDIS_REGIONS_PREFIX = "REGIONS";

    /**
     * 缓存地区条目
     */
    public static final String REDIS_REGION_ITEM_PREFIX = "REGION_ITEM";

    /**
     * 数据字典组
     */
    public static final String REDIS_DATA_DICT_PREFIX = "DATA_DICT";

    /**
     * 数据字典项目
     */
    public static final String REDIS_DATA_DICT_ITEM_PREFIX = "DATA_DICT_ITEM";

    /**
     * 组织
     */
    public static final String REDIS_ORG_PREFIX = "ORG";

    /**
     * 图片验证码
     */
    public static final String REDIS_CAPTCHA_PREFIX = "Captcha";

    /**
     * 邮箱设置
     */
    public static final String REDIS_EMAIL_SETTING_PREFIX = "EMAIL_SETTING";

    /**
     * 在线用户
     */
    public static final String REDIS_ONLINE_USER_PREFIX = "ONLINE_USER";

    /**
     * 系统配置
     */
    public static final String REDIS_SYSTEM_CONFIG_PREFIX = "CONFIG";

    /**
     * 验证码
     */
    public static final String REDIS_SMS_CODE_PREFIX = "SMS_CODE";

    /**
     * 工作岗位
     */
    public static final String REDIS_POST_PREFIX = "POST";

    /**
     * 下载任务
     */
    public static final String REDIS_TASK_DOWNLOAD = "TASK:DOWNLOAD";

    /**
     * 上传任务
     */
    public static final String REDIS_TASK_UPLOAD = "TASK:UPLOAD";

    /**
     * 微信配置
     */
    public static final String REDIS_WECHAT_CONFIG_PREFIX = "WECHAT_CONFIG";

    /**
     * 三方应用集成配置
     */
    public static final String REDIS_THIRD_CONFIG_PREFIX = "THIRD_APPLICATION_CONFIG";

    /**
     * 文件目录
     */
    public static final String REDIS_FILE_CATALOG_CONFIG_PREFIX = "FILE_CATALOG_CONFIG";

    /**
     * 文件列表
     */
    public static final String REDIS_FILE_CONFIG_PREFIX = "FILE_LIST";

}
