package cn.chiship.sdk.core.base.constants;

/**
 * 全局常量
 *
 * @author lijian
 */
public class BaseConstants {

    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * 是否常量（0：否）
     */
    public static final byte NO = 0;

    /**
     * 是否常量（1：是）
     */
    public static final byte YES = 1;

    /**
     * 常量成功
     */
    public static final Integer SUCCESS = 200;

    /**
     * 常量失败
     */
    public static final Integer FAIL = 0;

    /**
     * http默认端口（默认值：80）
     */
    public static final int HTTP_PORT_DEFAULT = 80;

    /**
     * https默认端口（默认值：443）
     */
    public static final int HTTPS_PORT_DEFAULT = 443;

    /**
     * http请求类型：GET
     */
    public static final String HTTP_REQUEST_TYPE_GET = "GET";

    /**
     * http请求类型：POST
     */
    public static final String HTTP_REQUEST_TYPE_POST = "POST";

    /**
     * http网络协议
     */
    public static final String NETWORKING_PROTOCOL_HTTP = "http";

    /**
     * https网络协议
     */
    public static final String NETWORKING_PROTOCOL_HTTPS = "https";

    /**
     * 字符串unknown
     */
    public static final String STRING_UNKNOWN = "unknown";

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "page";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "limit";

    /**
     * 排序
     */
    public static final String PAGE_SORT = "sort";

    /**
     * 默认语言
     */
    public static final String DEFAULT_LANGUAGE = "zh";

    /**
     * 成功
     */
    public static final Integer STATUS_OK = 200;

    /**
     * 未授权
     */
    public static final Integer STATUS_UNAUTHORIZED = 401;

    /**
     * 找不到
     */
    public static final Integer STATUS_NOT_FOUND = 404;

    /**
     * 系统错误
     */
    public static final Integer STATUS_SYSTEM_ERROR = 500;

    /**
     * 地区等级省
     */
    public static final String REGION_LEVEL_PROVINCE = "province";

    /**
     * 地区等级市
     */
    public static final String REGION_LEVEL_CITY = "city";

    /**
     * 地区等级县区
     */
    public static final String REGION_LEVEL_DISTRICT = "district";

    /**
     * 地区等级街道
     */
    public static final String REGION_LEVEL_STREET = "street";

    /**
     * 字符串逗号
     */
    public static final String STRING_COMMA = ",";

    /**
     * 字符串点
     */
    public static final String STRING_DROP = ".";

    /**
     * 数据导出格式
     */
    public static final String EXPORT_FORMAT_CSV = "csv";

    public static final String EXPORT_FORMAT_XLS = "xls";

    public static final String EXPORT_FORMAT_XLSX = "xlsx";

    public static final String EXPORT_FORMAT_TXT = "txt";

    public static final String EXPORT_FORMAT_JSON = "json";
}
