package cn.chiship.sdk.core.base.constants;

/**
 * @author lijian 提示常量
 */
public class BaseTipConstants {

    public static final String NOT_EMPTY = "不能为空";

    public static final String MOBILE = "请输入正确的手机号";

    public static final String EMAIL = "请输入正确的邮箱";

    public static final String LENGTH_MIN_MAX = "长度必须在{min}和{max}之间";

    public static final String LENGTH_MIN = "至少{min}个长度";
}
