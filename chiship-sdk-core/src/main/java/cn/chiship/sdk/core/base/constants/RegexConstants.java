package cn.chiship.sdk.core.base.constants;

/**
 * 正则表达式常量类
 *
 * @author lijian
 */
public class RegexConstants {

    /**
     * 邮箱
     */
    public static final String EMAIL = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";

    /**
     * 手机号码
     */
    public static final String MOBILE = "^[1]([3-9])[0-9]{9}$";

    /**
     * 字母或数字
     */
    public static final String CHARSET_NUMBER = "^[a-z0-9A-Z]+$";

    /**
     * 身份证
     */
    public static final String ID_CARD = "^(\\d{17}|\\d{14})[\\dxX]$";

    public static boolean check(String value, String regex) {
        return value.matches(regex);
    }

}
