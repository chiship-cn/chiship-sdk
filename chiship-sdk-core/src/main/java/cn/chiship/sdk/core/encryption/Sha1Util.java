package cn.chiship.sdk.core.encryption;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * SHA1工具
 *
 * @author lj
 */
public class Sha1Util {

	private Sha1Util() {

	}

	public static String sha1Encode(String sourceString) throws NoSuchAlgorithmException {
		String resultString = sourceString;
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		resultString = byte2hexString(md.digest(resultString.getBytes()));
		return resultString;
	}

	public static final String byte2hexString(byte[] bytes) {
		StringBuilder buf = new StringBuilder(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			if ((bytes[i] & 0xff) < 0x10) {
				buf.append("0");
			}
			buf.append(Long.toString(bytes[i] & 0xff, 16));
		}
		return buf.toString().toLowerCase();
	}

}
