package cn.chiship.sdk.core.encryption.rsa;

import cn.chiship.sdk.core.exception.custom.EncryptionException;
import cn.chiship.sdk.core.util.Base64Util;
import cn.chiship.sdk.core.util.ObjectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author lijian RSA公钥私钥加解密
 */
public class RsaEncrypt {

	private static final Logger LOGGER = LoggerFactory.getLogger(RsaEncrypt.class);

	private static final String RSA = "RSA";

	private static final int KEY_SIZE = 1024;

	private RsaEncrypt() {
	}

	/**
	 * 随机生成密钥对
	 */
	public static void genKeyPairBySystem(String filePath) {
		/**
		 * KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
		 */
		KeyPairGenerator keyPairGen = null;
		try {
			keyPairGen = KeyPairGenerator.getInstance(RSA);
		}
		catch (NoSuchAlgorithmException e) {
			LOGGER.error("发生异常", e);
		}
		keyPairGen.initialize(KEY_SIZE, new SecureRandom());
		/**
		 * 生成一个密钥对，保存在keyPair中
		 */
		KeyPair keyPair = keyPairGen.generateKeyPair();
		/**
		 * 得到私钥
		 */
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
		/**
		 * 得到公钥
		 */
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		try (FileWriter pubfw = new FileWriter(filePath + "/publicKey.keystore");
				FileWriter prifw = new FileWriter(filePath + "/privateKey.keystore");
				BufferedWriter pubbw = new BufferedWriter(pubfw);
				BufferedWriter pribw = new BufferedWriter(prifw);) {
			/**
			 * 得到公钥字符串
			 */
			String publicKeyString = Base64Util.encode(publicKey.getEncoded());
			/**
			 * 得到私钥字符串
			 */
			String privateKeyString = Base64Util.encode(privateKey.getEncoded());
			/**
			 * 将密钥对写入到文件
			 */

			pubbw.write(publicKeyString);
			pribw.write(privateKeyString);
			pubbw.flush();
			pribw.flush();
		}
		catch (Exception e) {
			LOGGER.error("发生异常", e);
		}
	}

	/**
	 * 随机生成密钥对
	 */
	public static String[] genKeyPair() {
		/**
		 * KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
		 */
		KeyPairGenerator keyPairGen = null;
		try {
			keyPairGen = KeyPairGenerator.getInstance("RSA");
		}
		catch (NoSuchAlgorithmException e) {
			LOGGER.error("发生异常", e);
		}
		if (keyPairGen == null) {
			return new String[0];
		}
		keyPairGen.initialize(KEY_SIZE, new SecureRandom());
		/**
		 * 生成一个密钥对，保存在keyPair中
		 */
		KeyPair keyPair = keyPairGen.generateKeyPair();
		/**
		 * 得到私钥
		 */
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
		/**
		 * 得到公钥
		 */
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		try {
			/**
			 * 得到公钥字符串
			 */
			String publicKeyString = Base64Util.encode(publicKey.getEncoded());
			/**
			 * 得到私钥字符串
			 */
			String privateKeyString = Base64Util.encode(privateKey.getEncoded());
			return new String[] { publicKeyString, privateKeyString };
		}
		catch (Exception e) {
			LOGGER.error("发生异常", e);
		}
		return new String[] {};
	}

	/**
	 * 从文件中输入流中加载公钥
	 * @param path 公钥输入流
	 * @throws Exception 加载公钥时产生的异常
	 */
	public static String loadPublicKeyByFile(String path) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(path + "/publicKey.keystore"));
			String readLine = null;
			StringBuilder sb = new StringBuilder();
			while ((readLine = br.readLine()) != null) {
				sb.append(readLine);
			}
			return sb.toString();
		}
		catch (IOException e) {
			throw new EncryptionException("公钥数据流读取错误");
		}
		catch (NullPointerException e) {
			throw new EncryptionException("公钥输入流为空");
		}
		finally {
			if (ObjectUtil.isNotEmpty(br)) {
				try {
					br.close();
				}
				catch (IOException e) {
					LOGGER.error("发生异常", e);
				}
			}
		}
	}

	/**
	 * 从字符串中加载公钥
	 * @param publicKeyStr 公钥数据字符串
	 * @throws Exception 加载公钥时产生的异常
	 */
	public static RSAPublicKey loadPublicKeyByStr(String publicKeyStr) {
		try {
			byte[] buffer = Base64Util.decode(publicKeyStr);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
			return (RSAPublicKey) keyFactory.generatePublic(keySpec);
		}
		catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("无此算法");
		}
		catch (InvalidKeySpecException e) {
			throw new EncryptionException("公钥非法");
		}
		catch (NullPointerException e) {
			throw new EncryptionException("公钥数据为空");
		}
	}

	/**
	 * 从文件中加载私钥
	 * @param path 私钥文件名
	 * @return 是否成功
	 * @throws Exception 异常
	 */
	public static String loadPrivateKeyByFile(String path) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(path + "/privateKey.keystore"));
			String readLine = null;
			StringBuilder sb = new StringBuilder();
			while ((readLine = br.readLine()) != null) {
				sb.append(readLine);
			}
			return sb.toString();
		}
		catch (IOException e) {
			throw new EncryptionException("私钥数据读取错误");
		}
		catch (NullPointerException e) {
			throw new EncryptionException("私钥输入流为空");
		}
		finally {
			if (ObjectUtil.isNotEmpty(br)) {
				try {
					br.close();
				}
				catch (IOException e) {
					LOGGER.error("发生异常", e);
				}
			}
		}
	}

	public static RSAPrivateKey loadPrivateKeyByStr(String privateKeyStr) {
		try {
			byte[] buffer = Base64Util.decode(privateKeyStr);
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
		}
		catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("无此算法");
		}
		catch (InvalidKeySpecException e) {
			throw new EncryptionException("私钥非法");
		}
		catch (NullPointerException e) {
			throw new EncryptionException("私钥数据为空");
		}
	}

	/**
	 * 公钥加密过程
	 * @param publicKey 公钥
	 * @param plainTextData 明文数据
	 * @return 几个
	 * @throws Exception 加密过程中的异常信息
	 */
	public static byte[] encrypt(RSAPublicKey publicKey, byte[] plainTextData) {
		if (publicKey == null) {
			throw new EncryptionException("加密公钥为空, 请设置");
		}
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(RSA);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return cipher.doFinal(plainTextData);
		}
		catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("无此加密算法");
		}
		catch (NoSuchPaddingException e) {
			LOGGER.error("发生异常", e);
			return new byte[0];
		}
		catch (InvalidKeyException e) {
			throw new EncryptionException("加密公钥非法,请检查");
		}
		catch (IllegalBlockSizeException e) {
			throw new EncryptionException("明文长度非法");
		}
		catch (BadPaddingException e) {
			throw new EncryptionException("明文数据已损坏");
		}
	}

	/**
	 * 私钥加密过程
	 * @param privateKey 私钥
	 * @param plainTextData 明文数据
	 * @return 结果
	 * @throws Exception 加密过程中的异常信息
	 */
	public static byte[] encrypt(RSAPrivateKey privateKey, byte[] plainTextData) {
		if (privateKey == null) {
			throw new EncryptionException("加密私钥为空, 请设置");
		}
		Cipher cipher = null;
		try {
			/**
			 * 使用默认RSA
			 */
			cipher = Cipher.getInstance(RSA);
			cipher.init(Cipher.ENCRYPT_MODE, privateKey);
			return cipher.doFinal(plainTextData);
		}
		catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("无此加密算法");
		}
		catch (NoSuchPaddingException e) {
			LOGGER.error("发生异常", e);
			return new byte[0];
		}
		catch (InvalidKeyException e) {
			throw new EncryptionException("加密私钥非法,请检查");
		}
		catch (IllegalBlockSizeException e) {
			throw new EncryptionException("明文长度非法");
		}
		catch (BadPaddingException e) {
			throw new EncryptionException("明文数据已损坏");
		}
	}

	/**
	 * 私钥解密过程
	 * @param privateKey 私钥
	 * @param cipherData 密文数据
	 * @return 明文
	 * @throws Exception 解密过程中的异常信息
	 */
	public static byte[] decrypt(RSAPrivateKey privateKey, byte[] cipherData) {
		if (privateKey == null) {
			throw new EncryptionException("解密私钥为空, 请设置");
		}
		Cipher cipher = null;
		try {
			/**
			 * 使用默认RSA
			 */
			cipher = Cipher.getInstance(RSA);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return cipher.doFinal(cipherData);
		}
		catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("无此解密算法");
		}
		catch (NoSuchPaddingException e) {
			LOGGER.error("发生异常", e);
			return new byte[0];
		}
		catch (InvalidKeyException e) {
			throw new EncryptionException("解密私钥非法,请检查");
		}
		catch (IllegalBlockSizeException e) {
			throw new EncryptionException("密文长度非法");
		}
		catch (BadPaddingException e) {
			throw new EncryptionException("密文数据已损坏");
		}
	}

	/**
	 * 公钥解密过程
	 * @param publicKey 公钥
	 * @param cipherData 密文数据
	 * @return 明文
	 * @throws Exception 解密过程中的异常信息
	 */
	public static byte[] decrypt(RSAPublicKey publicKey, byte[] cipherData) {
		if (publicKey == null) {
			throw new EncryptionException("解密公钥为空, 请设置");
		}
		Cipher cipher = null;
		try {
			/**
			 * 使用默认RSA
			 */
			cipher = Cipher.getInstance(RSA);
			cipher.init(Cipher.DECRYPT_MODE, publicKey);
			return cipher.doFinal(cipherData);
		}
		catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("无此解密算法");
		}
		catch (NoSuchPaddingException e) {
			LOGGER.error("发生异常", e);
			return new byte[0];
		}
		catch (InvalidKeyException e) {
			throw new EncryptionException("解密公钥非法,请检查");
		}
		catch (IllegalBlockSizeException e) {
			throw new EncryptionException("密文长度非法");
		}
		catch (BadPaddingException e) {
			throw new EncryptionException("密文数据已损坏");
		}
	}

}
