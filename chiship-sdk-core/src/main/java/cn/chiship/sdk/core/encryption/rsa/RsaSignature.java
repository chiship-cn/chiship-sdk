package cn.chiship.sdk.core.encryption.rsa;

import cn.chiship.sdk.core.util.Base64Util;
import cn.chiship.sdk.core.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * RSA签名验签类
 *
 * @author lijian
 */
public class RsaSignature {

	private static final Logger LOGGER = LoggerFactory.getLogger(RsaSignature.class);

	private RsaSignature() {

	}

	/**
	 * 签名算法
	 */
	public static final String SIGN_ALGORITHMS = "SHA256withRSA";

	/**
	 * RSA签名
	 * @param content 待签名数据
	 * @param privateKey 商户私钥
	 * @param encode 字符集编码
	 * @return 签名值
	 */
	public static String sign(String content, String privateKey, String encode) {
		try {
			PKCS8EncodedKeySpec priPkcs8 = new PKCS8EncodedKeySpec(Base64Util.decode(privateKey));

			KeyFactory keyf = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyf.generatePrivate(priPkcs8);

			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);

			signature.initSign(priKey);
			signature.update(content.getBytes(encode));

			byte[] signed = signature.sign();

			return Base64Util.encode(signed);
		}
		catch (Exception e) {
			LOGGER.error("RSA签名发生异常", e);
		}

		return null;
	}

	public static String sign(String content, String privateKey) {
		try {
			PKCS8EncodedKeySpec priPkcs8 = new PKCS8EncodedKeySpec(Base64Util.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyf.generatePrivate(priPkcs8);
			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);
			signature.initSign(priKey);
			signature.update(content.getBytes());
			byte[] signed = signature.sign();
			return Base64Util.encode(signed);
		}
		catch (Exception e) {
			LOGGER.error("RSA签名发生异常", e);
		}
		return null;
	}

	/**
	 * RSA验签名检查
	 * @param content 待签名数据
	 * @param sign 签名值
	 * @param publicKey 分配给开发商公钥
	 * @param encode 字符集编码
	 * @return 布尔值
	 */
	public static boolean doCheck(String content, String sign, String publicKey, String encode) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			byte[] encodedKey = Base64Util.decode(publicKey);
			PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
			java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);

			signature.initVerify(pubKey);
			signature.update(content.getBytes(encode));

			if (StringUtil.isNull(Base64Util.decode(sign))) {
				return false;
			}
			else {
				return signature.verify(Base64Util.decode(sign));
			}

		}
		catch (Exception e) {
			return false;
		}
	}

	public static boolean doCheck(String content, String sign, String publicKey) {
		return doCheck(content, sign, publicKey, "UTF-8");
	}

}
