package cn.chiship.sdk.core.enums;

/**
 * restful接口结果常量枚举类
 *
 * @author lijian
 */
public enum BaseResultEnum {

	/**
	 * 失败
	 */
	FAILED(0, "操作失败"),

	/**
	 * 成功
	 */
	SUCCESS(200, "操作成功"),

	/**
	 * 用户名或密码错误
	 */
	ERROR_USERNAME_OR_PASSWORD(200001, "用户名或密码错误"),
	/**
	 * 密码错误
	 */
	ERROR_PASSWORD(200002, "密码错误"),
	/**
	 * 用户不存在
	 */
	USER_IS_EXIST_NO(200003, "用户不存在"),

	/**
	 * 账号被锁定
	 */
	ACCOUNT_LOCKED(200009, "账号被锁定"),

	/**
	 * 未找到
	 */
	UNAUTHORIZED(401, "未经授权"),

	/**
	 * 未找到
	 */
	NOT_FOUND(404, "(未找到)服务器找不到请求的网页"),

	/**
	 * 系统错误
	 */
	SYSTEM_ERROR(500, "系统错误,请联系管理员"),

	/**
	 * 业务错误
	 */
	BUSINESS_ERROR(5001, "业务错误,请仔细核对相关业务或联系管理员"),

	/**
	 * 权限错误
	 */
	PERMISSIONS_ERROR(5002, "权限错误,请仔细核对该接口权限与用户权限是否一致"),

	/**
	 * 三方API调用失败
	 */
	ERR_THIRD_API(5009, "三方API调用发生错误"),

	/**
	 * 参数检查失败
	 */
	PARAM_CHECK_FAILED(501, "参数检查失败"),

	/**
	 * 缺少消息头Access-Token或为空
	 */
	HEADER_NO_TOKEN(502, "缺少消息头Access-Token或为空"),
	/**
	 * 参数被篡改
	 */
	PARAMETER_MANIPULATION(503, "参数被篡改"),

	/**
	 * 缺少消息头MAC地址或为空
	 */
	HEADER_NO_MAC(504, "缺少消息头MAC地址或为空"),

	/**
	 * 缺少消息头User-Agent地址或为空
	 */
	HEADER_NO_USER_AGENT(505, "缺少消息头User-Agent地址或为空"),

	/**
	 * 缺少消息头Encrypt参数或为空
	 */
	HEADER_NO_ENCRYPT_PARAM(506, "缺少消息头Encrypt参数或为空"),

	/**
	 * 缺少消息头App-Key和App-Secret参数或为空
	 */
	HEADER_NO_APP_KEY_AND_SECRET_PARAM(507, "缺少消息头App-Key和App-Secret参数或为空"),

	/**
	 * 缺少消息头App-Key和App-Id参数或为空
	 */
	HEADER_NO_APP_KEY_AND_APP_ID(508, "缺少消息头App-Key和App-Id参数或为空"),

	/**
	 * 验签失败
	 */
	SIGN_ERR(509, "验签失败"),

	/**
	 * 缺少消息头Sign
	 */
	HEADER_NO_SIGN_PARAM(510, "缺少消息头Sign"),

	/**
	 * 缺少消息头ProjectsId
	 */
	HEADER_NO_PROJECTS_ID_PARAM(511, "缺少消息头ProjectsId"),

	/**
	 * 无效的App-ID
	 */
	ERROR_APP_ID(506000, "无效的App-Id"),

	/**
	 * 无效的App-Key
	 */
	ERROR_APP_KEY(506001, "无效的App-Key"),

	/**
	 * 无效的ProjectsId
	 */
	ERROR_PROJECTS_ID(506002, "无效的ProjectsId"),

	/**
	 * 令牌失效
	 */
	ERROR_TOKEN(506003, "令牌失效，请重新登录"),

	/**
	 * 暂无数据
	 */
	EXCEPTION_DATA_BASE_NO(50020, "暂无数据"),

	/**
	 * 数据保存异常
	 */
	EXCEPTION_DATA_BASE_INSERT(50021, "数据保存异常"),

	/**
	 * 数据更新异常
	 */
	EXCEPTION_DATA_BASE_UPDATE(50022, "数据更新异常"),

	/**
	 * 数据删除异常
	 */
	EXCEPTION_DATA_BASE_DELETE(50023, "数据删除异常"),

	/**
	 * 数据查询异常
	 */
	EXCEPTION_DATA_BASE_SELECT(50024, "数据查询异常"),

	/**
	 * 数据还原异常
	 */
	EXCEPTION_DATA_BASE_RESTORE(50025, "数据还原异常"),

	/**
	 * 数据已存在
	 */
	EXCEPTION_DATA_BASE_REPEAT(50026, "数据已存在");

	/**
	 * 自定义状态码
	 */
	private int code;

	/**
	 * 状态信息
	 */
	private String message;

	BaseResultEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
