package cn.chiship.sdk.core.enums;

/**
 * @author lijian 内容类型
 */

public enum ContentTypeEnum {

	/**
	 * JSON
	 */
	APPLICATION_JSON("application/json"),
	/**
	 * xls xlsx
	 */
	APPLICATION_EXCEL("application/vnd.ms-excel"),
	/**
	 * csv
	 */
	APPLICATION_CSV("application/ms-txt.numberformat:@"),
	/**
	 * txt
	 */
	APPLICATION_TEXT("text/plain"),
	/**
	 * pdf
	 */
	APPLICATION_PDF("application/pdf"),
	/**
	 * 二进制数组
	 */
	APPLICATION_STREAM("application/octet-stream"),;

	private String name;

	private ContentTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
