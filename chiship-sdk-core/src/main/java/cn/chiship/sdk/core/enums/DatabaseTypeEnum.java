package cn.chiship.sdk.core.enums;

/**
 * 数据库类型
 *
 * @author lj
 */
public enum DatabaseTypeEnum {

	/**
	 * mysql
	 */
	DATABASE_TYPE_MYSQL(Byte.valueOf("0"), "MYSQL"),

	/**
	 * SQL_SERVER
	 */
	DATABASE_TYPE_SQL_SERVER(Byte.valueOf("1"), "SQL_SERVER"),

	/**
	 * ORACLE
	 */
	DATABASE_TYPE_ORACLE(Byte.valueOf("2"), "ORACLE"),

	;

	/**
	 * 类型
	 */
	private Byte type;

	/**
	 * 描述
	 */
	private String desc;

	private DatabaseTypeEnum(Byte type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public Byte getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}
