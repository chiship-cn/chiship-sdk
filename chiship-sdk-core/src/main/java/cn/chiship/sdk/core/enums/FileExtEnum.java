package cn.chiship.sdk.core.enums;

import cn.chiship.sdk.core.base.constants.BaseConstants;

/**
 * @author lijian 文件扩展名
 */
public enum FileExtEnum {

	FILE_EXT_PNG(".png"), FILE_EXT_JPG(".jpg"), FILE_EXT_JSON(".json"), FILE_EXT_XML(".xml"), FILE_EXT_TXT(
			".txt"), FILE_EXT_PDF(".pdf"), FILE_EXT_DOC(".doc"), FILE_EXT_DOCX(".docx"), FILE_EXT_XLS(
					".xls"), FILE_EXT_XLSX(".xlsx"), FILE_EXT_CSV(".csv"), FILE_EXT_PPT(".ppt"), FILE_EXT_PPTX(
							".pptx"), FILE_EXT_ZIP(".zip"), FILE_EXT_RAR(
									".rar"), FILE_EXT_EXE(".exe"), FILE_EXT_MP3(".mp3"), FILE_EXT_MP4(".mp4"),;

	private String name;

	FileExtEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * 根据名称获得枚举
	 * @param name 不携带.
	 * @return FileExtEnum
	 */
	public static FileExtEnum getByName(String name) {
		FileExtEnum[] fileExtEnums = values();
		for (FileExtEnum fileExtEnum : fileExtEnums) {
			if (name.indexOf(BaseConstants.STRING_DROP) == -1) {
				name = BaseConstants.STRING_DROP + name;
			}
			if (fileExtEnum.getName().equals(name)) {
				return fileExtEnum;
			}
		}
		return null;
	}

}
