package cn.chiship.sdk.core.enums;

/**
 * 性别枚举
 *
 * @author lijian
 */
public enum GenderEnum {

	/**
	 * 性别 未知
	 */
	UNKNOW(Byte.valueOf("0"), "未知"),

	/**
	 * 性别 男
	 */
	BOY(Byte.valueOf("1"), "男"),

	/**
	 * 性别 女
	 */
	GIRL(Byte.valueOf("2"), "女"),

	;

	/**
	 * 类型
	 */
	private Byte value;

	/**
	 * 描述
	 */
	private String desc;

	private GenderEnum(Byte value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public Byte getType() {
		return value;
	}

	public String getDesc() {
		return desc;
	}

}
