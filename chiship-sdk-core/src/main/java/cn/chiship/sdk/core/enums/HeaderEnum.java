package cn.chiship.sdk.core.enums;

/**
 * @author lijian 消息头枚举
 */

public enum HeaderEnum {

	/**
	 * 登陆令牌
	 */
	HEADER_ACCESS_TOKEN("Access-Token", "登陆令牌"),

	/**
	 * App Id
	 */
	HEADER_APP_ID("App-Id", "App Id"),
	/**
	 * AccessKey ID
	 */
	HEADER_APP_KEY("App-Key", "AccessKey ID"),

	/**
	 * 项目唯一标识
	 */
	HEADER_PROJECTS_ID("ProjectsId", "项目唯一标识"),

	/**
	 * 参数加密字符串
	 */
	HEADER_ENCRYPT("Encrypt", "参数加密字符串"),

	/**
	 * 参数签名串
	 */
	HEADER_SIGN("Sign", "参数签名串"),

	/**
	 * 操作环境
	 */
	HEADER_USER_AGENT("User-Agent", "操作环境"),

	/**
	 * 请求语言
	 */
	HEADER_ACCEPT_LANGUAGE("Accept-Language", "请求语言"),

	/**
	 * 内容处理
	 */
	HEADER_CONTENT_DISPOSITION("Content-Disposition", "内容处理"),

	/**
	 * 下载文件名称
	 */
	HEADER_DOWNLOAD_FILENAME("Download-FileName", "下载文件名称"),

	;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 描述
	 */
	private String desc;

	private HeaderEnum(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

}