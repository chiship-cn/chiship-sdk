package cn.chiship.sdk.core.enums;

/**
 * @author lijian 权限枚举
 */

public enum PermissionsEnum {

	/**
	 * 并且
	 */
	AND,
	/**
	 * 或
	 */
	OR;

	private PermissionsEnum() {
	}

}