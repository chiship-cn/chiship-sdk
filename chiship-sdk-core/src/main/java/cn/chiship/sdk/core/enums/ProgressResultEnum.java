package cn.chiship.sdk.core.enums;

/**
 * @author lijian 流程结果枚举
 */

public enum ProgressResultEnum {

	/**
	 * 正在处理
	 */
	PROGRESS_DOING_ENUM("DO", "正在处理"),

	/**
	 * 完成
	 */
	PROGRESS_FINISH_ENUM("FINISH", "完成"),

	/**
	 * 下载
	 */
	PROGRESS_DOWN_ENUM("DOWN", "下载"),

	/**
	 * 错误
	 */
	PROGRESS_ERROR_ENUM("ERROR", "错误"),

	/**
	 * 进度
	 */
	PROGRESS_PROGRESS_ENUM("PROGRESS", "进度"),

	/**
	 * 流
	 */
	PROGRESS_STREAM_ENUM("STREAM", "文件输出流"),
	/**
	 * 数据库查询中
	 */
	PROGRESS_SEARCH_DATABASE_ENUM("SEARCH_DATABASE", "数据库查询中"),

	/**
	 * 数据库查询中
	 */
	PROGRESS_FILE_ASSEMBLY_ENUM("FILE_ASSEMBLY", "文件组装"),;

	/**
	 * 业务编号
	 */
	private String code;

	/**
	 * 业务描述
	 */
	private String message;

	ProgressResultEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
