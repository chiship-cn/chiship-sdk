package cn.chiship.sdk.core.enums;

/**
 * @author lijian 用户类型枚举
 */

public enum UserTypeEnum {

	/**
	 * 后台用户
	 */
	ADMIN("admin", "后台用户"),

	/**
	 * 前台用户
	 */
	MEMBER("member", "前台用户"),
	/**
	 * 其他
	 */
	OTHER("other", "其他"),;

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 描述
	 */
	private String desc;

	private UserTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}