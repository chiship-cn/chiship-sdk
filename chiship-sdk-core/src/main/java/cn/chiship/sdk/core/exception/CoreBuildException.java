package cn.chiship.sdk.core.exception;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.exception.custom.*;

/**
 * @author lj
 */
public class CoreBuildException {

	/**
	 * @param e 异常
	 * @return 结果
	 */
	public BaseResult formatException(Exception e) {
		return buildException(e);
	}

	/**
	 * @param requestId 请求序号
	 * @param e 异常
	 * @return 结果
	 */
	public BaseResult formatException(String requestId, Exception e) {
		return buildException(requestId, e);
	}

	public BaseResult buildException(Exception e) {
		return buildException(null, e);
	}

	public BaseResult buildException(String requestId, Exception e) {
		/**
		 * 系统类异常
		 */
		if (e instanceof SystemErrorException) {
			return ((SystemErrorException) e).formatException(requestId);
		}

		/**
		 * 业务相关
		 */
		if (e instanceof BusinessException) {
			return ((BusinessException) e).formatException();
		}

		/**
		 * 权限相关
		 */
		if (e instanceof PermissionsException) {
			return ((PermissionsException) e).formatException();
		}

		/**
		 * 令牌相关
		 */
		if (e instanceof AccessTokenException) {
			return ((AccessTokenException) e).formatException();
		}

		/**
		 * 加解密相关异常
		 */
		if (e instanceof EncryptionException) {
			return ((EncryptionException) e).formatException();
		}

		/**
		 * 系统错误
		 */
		return new SystemErrorException(e).formatException(requestId);
	}

}
