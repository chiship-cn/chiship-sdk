package cn.chiship.sdk.core.exception;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.exception.model.CommonExceptionVo;

/**
 * @author lj
 */
public class ExceptionUtil {

	private ExceptionUtil() {

	}

	/**
	 * @param e 异常
	 * @return 结果
	 */
	public static CommonExceptionVo formatExceptionVo(Exception e) {
		BaseResult baseResult = new CoreBuildException().buildException(e);
		if (baseResult.getData() instanceof CommonExceptionVo) {
			return (CommonExceptionVo) baseResult.getData();
		}
		else {
			return null;
		}
	}

	/**
	 * @param e 异常
	 * @return 结果
	 */
	public static CommonExceptionVo formatExceptionVo(String requestId, Exception e) {
		BaseResult baseResult = new CoreBuildException().buildException(requestId, e);
		if (baseResult.getData() instanceof CommonExceptionVo) {
			return (CommonExceptionVo) baseResult.getData();
		}
		else {
			return null;
		}
	}

	/**
	 * @param e 异常
	 * @return 结果
	 */
	public static BaseResult formatException(Exception e) {
		return new CoreBuildException().buildException(e);
	}

	public static String getErrorLocalizedMessage(Exception e) {
		return e.getClass().getName() + " " + (e.getLocalizedMessage() == null ? "" : e.getLocalizedMessage());
	}

	public static String getErrorLocalizedMessage(Throwable cause) {
		return cause.getClass().getName() + " "
				+ (cause.getLocalizedMessage() == null ? "" : cause.getLocalizedMessage());
	}

}
