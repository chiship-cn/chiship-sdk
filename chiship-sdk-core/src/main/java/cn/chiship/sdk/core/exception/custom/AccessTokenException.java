package cn.chiship.sdk.core.exception.custom;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;

/**
 * AccessToken异常
 *
 * @author lijian
 */
public class AccessTokenException extends RuntimeException {

	private final int code;

	private final String message;

	public AccessTokenException(BaseResultEnum baseResultEnum) {
		this(baseResultEnum.getCode(), baseResultEnum.getMessage());
	}

	public AccessTokenException(Integer code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public BaseResult formatException() {
		/**
		 * 注意 BaseResult.error()第一个参数根据不同的分类异常返回一级错误码 一级错误码在 @see
		 * com.dianll.common.lib.core.base.BaseResultEnum 中定义
		 *
		 */
		if (this.code == BaseResultEnum.HEADER_NO_TOKEN.getCode()) {
			return BaseResult.error(BaseResultEnum.HEADER_NO_TOKEN, null);
		}
		if (this.code == BaseResultEnum.ERROR_TOKEN.getCode()) {
			return BaseResult.error(BaseResultEnum.ERROR_TOKEN, null);
		}
		return new BaseResult(false, this.code, this.message, null);
	}

	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
