package cn.chiship.sdk.core.exception.custom;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;

/**
 * 业务异常
 *
 * @author lj
 */
public class BusinessException extends RuntimeException {

	public BusinessException(String message) {
		super(message);
	}

	public BaseResult formatException() {
		/**
		 * 注意 BaseResult.error()第一个参数根据不同的分类异常返回一级错误码 一级错误码在 @see
		 * com.dianll.common.lib.core.base.BaseResultEnum 中定义
		 *
		 */
		return BaseResult.error(BaseResultEnum.BUSINESS_ERROR, this.getMessage());
	}

}
