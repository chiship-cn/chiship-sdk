package cn.chiship.sdk.core.exception.custom;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.exception.ExceptionUtil;
import cn.chiship.sdk.core.exception.model.CommonExceptionVo;
import cn.chiship.sdk.core.exception.model.ExceptionEnum;
import cn.chiship.sdk.core.util.StringUtil;

/**
 * 加密数据异常
 *
 * @author lj
 */
public class EncryptionException extends RuntimeException {

	private final int errorCode;

	private final String errorMessage;

	private final String errorLocalizedMessage;

	public EncryptionException(String errorMessage) {
		this(null, errorMessage, null);
	}

	public EncryptionException(Integer errorCode, String errorMessage) {
		this(errorCode, errorMessage, null);
	}

	public EncryptionException(Throwable cause) {
		this((String) null, cause);
	}

	public EncryptionException(String errorMessage, Throwable cause) {
		this(null, errorMessage, cause);
	}

	public EncryptionException(ExceptionEnum exceptionEnum, Throwable cause) {
		this(exceptionEnum.getErrorCode(), exceptionEnum.getErrorMessage(), cause);
	}

	public EncryptionException(Integer errorCode, String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		ExceptionEnum exceptionEnum = ExceptionEnum.ENCRYPTION_ERROR;
		/**
		 * 判断传入的异常是否为空 若为空，errorLocalizedMessage默认为'自定义的异常信息'
		 * 不为空，errorLocalizedMessage为异常详情信息
		 */
		if (StringUtil.isNull(cause)) {
			this.errorLocalizedMessage = exceptionEnum.getErrorMessage();
		}
		else {
			this.errorLocalizedMessage = ExceptionUtil.getErrorLocalizedMessage(cause);
		}

		/**
		 * 判断错误码是否为空 若为空，errorCode默认为'5003001' 不为空，errorCode显示参数传递
		 */
		if (StringUtil.isNull(errorCode)) {
			this.errorCode = exceptionEnum.getErrorCode();
		}
		else {
			this.errorCode = errorCode;
		}

		/**
		 * 判断错误信息是否为空 若为空，errorMessage根据异常显示对应的错误信息 不为空，errorMessage显示参数传递的信息
		 */
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			this.errorMessage = errorMessage;
		}
		else {
			this.errorMessage = exceptionEnum.getErrorMessage();
		}
	}

	public BaseResult formatException() {
		/**
		 * 注意 BaseResult.error()第一个参数根据不同的分类异常返回一级错误码 一级错误码在 @see
		 * com.dianll.common.lib.core.base.BaseResultEnum 中定义
		 *
		 */
		return BaseResult.error(BaseResultEnum.SYSTEM_ERROR,
				new CommonExceptionVo(this.getErrorCode(), this.getErrorMessage(), this.getErrorLocalizedMessage()));
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorLocalizedMessage() {
		return errorLocalizedMessage;
	}

}
