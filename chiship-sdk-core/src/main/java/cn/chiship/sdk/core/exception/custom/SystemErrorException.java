package cn.chiship.sdk.core.exception.custom;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.exception.ExceptionUtil;
import cn.chiship.sdk.core.exception.model.CommonExceptionVo;
import cn.chiship.sdk.core.exception.model.ExceptionEnum;
import cn.chiship.sdk.core.util.StringUtil;

import java.net.UnknownHostException;

/**
 * @author lijian 系统异常
 */
public class SystemErrorException extends RuntimeException {

	private Integer errorCode;

	private String errorMessage;

	private String errorLocalizedMessage;

	public SystemErrorException() {
	}

	public SystemErrorException(String errorMessage) {
		this(null, errorMessage, null);
	}

	public SystemErrorException(Integer errorCode, String errorMessage) {
		this(errorCode, errorMessage, null);
	}

	public SystemErrorException(Throwable cause) {
		this((String) null, cause);
	}

	public SystemErrorException(String errorMessage, Throwable cause) {
		this(null, errorMessage, cause);
	}

	public SystemErrorException(ExceptionEnum exceptionEnum, Throwable cause) {
		this(exceptionEnum.getErrorCode(), exceptionEnum.getErrorMessage(), cause);
	}

	public SystemErrorException(Integer errorCode, String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		ExceptionEnum exceptionEnum = ExceptionEnum.CUSTOM;
		/**
		 * 判断传入的异常是否为空 若为空，errorLocalizedMessage默认为'自定义的异常信息'
		 * 不为空，errorLocalizedMessage为异常详情信息
		 */
		if (StringUtil.isNull(cause)) {
			this.errorLocalizedMessage = exceptionEnum.getErrorMessage();
		}
		else {
			this.errorLocalizedMessage = ExceptionUtil.getErrorLocalizedMessage(cause);
		}

		/**
		 * 判断错误码是否为空 若为空，errorCode默认为'0' 不为空，errorCode显示参数传递
		 */
		if (StringUtil.isNull(errorCode)) {
			this.errorCode = exceptionEnum.getErrorCode();
		}
		else {
			this.errorCode = errorCode;
		}

		/**
		 * 判断错误信息是否为空 若为空，errorMessage根据异常显示对应的错误信息 不为空，errorMessage显示参数传递的信息
		 */
		if (!StringUtil.isNullOrEmpty(errorMessage)) {
			this.errorMessage = errorMessage;
		}
		else {
			/**
			 * JDK自带异常
			 */
			if (cause instanceof RuntimeException) {
				exceptionEnum = ExceptionEnum.RUNTIME;
			}
			if (cause instanceof NullPointerException) {
				exceptionEnum = ExceptionEnum.NULL_POINTER;
			}
			if (cause instanceof ArithmeticException) {
				exceptionEnum = ExceptionEnum.ARITHMETIC;
			}
			if (cause instanceof ArrayIndexOutOfBoundsException) {
				exceptionEnum = ExceptionEnum.ARRAY_INDEX_OUT_OF_BOUNDS;
			}
			if (cause instanceof NumberFormatException) {
				exceptionEnum = ExceptionEnum.NUMBER_FORMAT;
			}
			if (cause instanceof UnknownHostException) {
				exceptionEnum = ExceptionEnum.UNKNOWN_HOST;
			}
			this.errorMessage = exceptionEnum.getErrorMessage();
			this.errorCode = exceptionEnum.getErrorCode();
		}
	}

	public BaseResult formatException() {
		return formatException(null);
	}

	public BaseResult formatException(String requestId) {
		/**
		 * 注意 BaseResult.error()第一个参数根据不同的分类异常返回一级错误码 一级错误码在 @see
		 * com.dianll.common.lib.core.base.BaseResultEnum 中定义
		 *
		 */
		return BaseResult.error(BaseResultEnum.SYSTEM_ERROR, new CommonExceptionVo(this.getErrorCode(),
				this.getErrorMessage(), requestId, null, this.getErrorLocalizedMessage()));
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorLocalizedMessage() {
		return errorLocalizedMessage;
	}

}
