package cn.chiship.sdk.core.exception.custom;

/**
 * Token验证失效异常
 *
 * @author lj
 */
public class TokenInvalidException extends RuntimeException {

	private TokenInvalidException() {
	}

}
