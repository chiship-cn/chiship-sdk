package cn.chiship.sdk.core.exception.model;

/**
 * 异常信息统一返回的实体
 *
 * @author lj
 */
public class CommonExceptionVo {

	private Integer errorCode;

	private String errorMessage;

	private String requestId;

	private String hostId;

	private String errorLocalizedMessage;

	public CommonExceptionVo() {
	}

	public CommonExceptionVo(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public CommonExceptionVo(ExceptionEnum exceptionEnum, String errorLocalizedMessage) {
		this(exceptionEnum.getErrorCode(), exceptionEnum.getErrorMessage(), null, null, errorLocalizedMessage);
	}

	public CommonExceptionVo(Integer errorCode, String errorMessage, String errorLocalizedMessage) {
		this(errorCode, errorMessage, null, null, errorLocalizedMessage);
	}

	public CommonExceptionVo(Integer errorCode, String errorMessage, String requestId, String hostId) {
		this(errorCode, errorMessage, requestId, hostId, null);
	}

	public CommonExceptionVo(Integer errorCode, String errorMessage, String requestId, String hostId,
			String errorLocalizedMessage) {
		if (null == errorCode) {
			errorCode = 0;
		}
		if (null == errorMessage) {
			errorMessage = "未定义的异常信息";
		}
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.requestId = requestId;
		this.hostId = hostId;
		this.errorLocalizedMessage = errorLocalizedMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getRequestId() {
		return requestId;
	}

	public String getHostId() {
		return hostId;
	}

	public String getErrorLocalizedMessage() {
		return errorLocalizedMessage;
	}

	@Override
	public String toString() {
		return "{" + "\"errorCode\":" + errorCode + ", \"errorMessage\":\"" + errorMessage + '\"'
				+ (requestId != null ? ", \"requestId':\"" + requestId + '\"' : "")
				+ (hostId != null ? ", \"hostId':\"" + hostId + '\"' : "") + ", \"errorLocalizedMessage\":\""
				+ errorLocalizedMessage + '\"' + '}';
	}

}
