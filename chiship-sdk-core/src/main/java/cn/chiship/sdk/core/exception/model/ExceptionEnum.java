package cn.chiship.sdk.core.exception.model;

/**
 * restful接口结果常量枚举类
 *
 * @author lijian
 */
public enum ExceptionEnum {

	/**
	 * 自定义的异常信息
	 */
	CUSTOM(0, "自定义的异常信息!"),
	/**
	 * 加密解密发生异常
	 */
	ENCRYPTION_ERROR(10, "加密解密发生异常!"),

	/**
	 * jdk自带 5001开头
	 */
	/**
	 * 运行异常
	 */
	RUNTIME(5001001, "运行异常!"),
	/**
	 * 运算条件抛出异常
	 */
	ARITHMETIC(5001002, "运算条件抛出异常!"),
	/**
	 * 集合下标越界
	 */
	ARRAY_INDEX_OUT_OF_BOUNDS(5001003, "集合下标越界!"),
	/**
	 * 数字格式化异常
	 */
	NUMBER_FORMAT(5001004, "数字格式化异常!"),
	/**
	 * 空指针错误
	 */
	NULL_POINTER(5001005, "空指针错误!"),
	/**
	 * 网络错误:未知主机!
	 */
	UNKNOWN_HOST(5001006, "网络错误:未知主机!"),;

	/**
	 * 自定义状态码
	 */
	private int errorCode;

	/**
	 * 状态信息
	 */
	private String errorMessage;

	ExceptionEnum(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
