package cn.chiship.sdk.core.monitor;

import cn.chiship.sdk.core.monitor.server.*;
import cn.chiship.sdk.core.util.ArithUtil;
import cn.chiship.sdk.core.util.ip.IpUtils;
import com.alibaba.fastjson.JSON;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;
import oshi.util.Util;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * 服务器相关信息
 *
 * @author lijian
 */
public class ServerMonitorSingleton {

	private static final Logger log = LoggerFactory.getLogger(ServerMonitorSingleton.class);

	private static final int OSHI_WAIT_SECOND = 1000;

	private static ServerMonitorSingleton instance;

	private ServerMonitorSingleton() {

	}

	public static synchronized ServerMonitorSingleton getInstance() {
		if (null == instance) {
			instance = new ServerMonitorSingleton();
		}
		return instance;
	}

	/**
	 * CPU相关信息
	 */
	private CpuVo cpuVo = new CpuVo();

	/**
	 * 內存相关信息
	 */
	private MemoryVo memoryVo = new MemoryVo();

	/**
	 * JVM相关信息
	 */
	private JvmVo jvmVo = new JvmVo();

	/**
	 * 服务器相关信息
	 */
	private SystemVo sys = new SystemVo();

	/**
	 * 磁盘相关信息
	 */
	private List<SystemFileVo> systemFileVos = new LinkedList<>();

	public CpuVo getCpu() {
		return cpuVo;
	}

	public void setCpu(CpuVo cpuVo) {
		this.cpuVo = cpuVo;
	}

	public MemoryVo getMem() {
		return memoryVo;
	}

	public void setMem(MemoryVo memoryVo) {
		this.memoryVo = memoryVo;
	}

	public JvmVo getJvm() {
		return jvmVo;
	}

	public void setJvm(JvmVo jvmVo) {
		this.jvmVo = jvmVo;
	}

	public SystemVo getSys() {
		return sys;
	}

	public void setSys(SystemVo sys) {
		this.sys = sys;
	}

	public List<SystemFileVo> getSysFiles() {
		return systemFileVos;
	}

	public void setSysFiles(List<SystemFileVo> systemFileVos) {
		this.systemFileVos = systemFileVos;
	}

	public void copyTo() {
		SystemInfo si = new SystemInfo();
		HardwareAbstractionLayer hal = si.getHardware();

		setCpuInfo(hal.getProcessor());

		setMemInfo(hal.getMemory());

		setSysInfo();

		setJvmInfo();

		setSysFiles(si.getOperatingSystem());
	}

	/**
	 * 设置CPU信息
	 */
	private void setCpuInfo(CentralProcessor processor) {
		// CPU信息
		long[] prevTicks = processor.getSystemCpuLoadTicks();
		Util.sleep(OSHI_WAIT_SECOND);
		long[] ticks = processor.getSystemCpuLoadTicks();
		long nice = ticks[TickType.NICE.getIndex()] - prevTicks[TickType.NICE.getIndex()];
		long irq = ticks[TickType.IRQ.getIndex()] - prevTicks[TickType.IRQ.getIndex()];
		long softirq = ticks[TickType.SOFTIRQ.getIndex()] - prevTicks[TickType.SOFTIRQ.getIndex()];
		long steal = ticks[TickType.STEAL.getIndex()] - prevTicks[TickType.STEAL.getIndex()];
		long cSys = ticks[TickType.SYSTEM.getIndex()] - prevTicks[TickType.SYSTEM.getIndex()];
		long user = ticks[TickType.USER.getIndex()] - prevTicks[TickType.USER.getIndex()];
		long iowait = ticks[TickType.IOWAIT.getIndex()] - prevTicks[TickType.IOWAIT.getIndex()];
		long idle = ticks[TickType.IDLE.getIndex()] - prevTicks[TickType.IDLE.getIndex()];
		long totalCpu = user + nice + cSys + idle + iowait + irq + softirq + steal;
		cpuVo.setCpuNum(processor.getLogicalProcessorCount());
		cpuVo.setTotal(totalCpu);
		cpuVo.setSys(cSys);
		cpuVo.setUsed(user);
		cpuVo.setWait(iowait);
		cpuVo.setFree(idle);
	}

	/**
	 * 设置内存信息
	 */
	private void setMemInfo(GlobalMemory memory) {
		memoryVo.setTotal(memory.getTotal());
		memoryVo.setUsed(memory.getTotal() - memory.getAvailable());
		memoryVo.setFree(memory.getAvailable());
	}

	/**
	 * 设置服务器信息
	 */
	private void setSysInfo() {
		Properties props = System.getProperties();
		sys.setComputerName(IpUtils.getHostName());
		sys.setComputerIp(IpUtils.getHostIp());
		sys.setOsName(props.getProperty("os.name"));
		sys.setOsArch(props.getProperty("os.arch"));
		sys.setUserDir(props.getProperty("user.dir"));
	}

	/**
	 * 设置Java虚拟机
	 */
	private void setJvmInfo() {
		Properties props = System.getProperties();
		jvmVo.setTotal(Runtime.getRuntime().totalMemory());
		jvmVo.setMax(Runtime.getRuntime().maxMemory());
		jvmVo.setFree(Runtime.getRuntime().freeMemory());
		jvmVo.setVersion(props.getProperty("java.version"));
		jvmVo.setHome(props.getProperty("java.home"));
	}

	/**
	 * 设置磁盘信息
	 */
	private void setSysFiles(OperatingSystem os) {
		FileSystem fileSystem = os.getFileSystem();
		List<OSFileStore> fsArray = fileSystem.getFileStores();
		systemFileVos.clear();
		for (OSFileStore fs : fsArray) {
			long free = fs.getUsableSpace();
			long total = fs.getTotalSpace();
			long used = total - free;
			SystemFileVo systemFileVo = new SystemFileVo();
			systemFileVo.setDirName(fs.getMount());
			systemFileVo.setSysTypeName(fs.getType());
			systemFileVo.setTypeName(fs.getName());
			systemFileVo.setTotal(convertFileSize(total));
			systemFileVo.setFree(convertFileSize(free));
			systemFileVo.setUsed(convertFileSize(used));
			systemFileVo.setUsage(ArithUtil.mul(ArithUtil.div(used, total, 4), 100));
			systemFileVos.add(systemFileVo);
		}
	}

	/**
	 * 字节转换
	 * @param size 字节大小
	 * @return 转换后值
	 */
	public String convertFileSize(long size) {
		long kb = 1024;
		long mb = kb * 1024;
		long gb = mb * 1024;
		if (size >= gb) {
			return String.format("%.1f GB", (float) size / gb);
		}
		else if (size >= mb) {
			float f = (float) size / mb;
			return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
		}
		else if (size >= kb) {
			float f = (float) size / kb;
			return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
		}
		else {
			return String.format("%d B", size);
		}
	}

	public static void main(String[] args) {
		ServerMonitorSingleton server = getInstance();
		server.copyTo();
		log.info(JSON.toJSONString(server));
	}

}
