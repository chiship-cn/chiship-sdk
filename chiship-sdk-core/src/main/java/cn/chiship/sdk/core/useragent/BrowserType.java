package cn.chiship.sdk.core.useragent;

/**
 * 浏览器类型
 * @author lj
 */
public enum BrowserType {

	WEB_BROWSER("Browser"), MOBILE_BROWSER("Browser (mobile)"), TEXT_BROWSER("Browser (text only)"), EMAIL_CLIENT(
			"Email Client"), ROBOT("Robot"), TOOL("Downloading tool"), APP("Application"), UNKNOWN("unknown");

	private String name;

	BrowserType(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
