package cn.chiship.sdk.core.useragent;

/**
 * 设备类型
 * @author lj
 */
public enum DeviceType {

	COMPUTER("Computer"), MOBILE("Mobile"), TABLET("Tablet"), GAME_CONSOLE("Game console"), DMR(
			"Digital media receiver"), WEARABLE("Wearable computer"), UNKNOWN("Unknown");

	String name;

	DeviceType(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
