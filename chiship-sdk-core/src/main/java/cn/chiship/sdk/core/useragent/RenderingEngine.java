package cn.chiship.sdk.core.useragent;

/**
 * 引擎
 * @author lijian
 */
public enum RenderingEngine {

	EDGE_HTML("EdgeHTML"), TRIDENT("Trident"), WORD("Microsoft Office Word"), GECKO("Gecko"), WEBKIT("WebKit"), PRESTO(
			"Presto"), MOZILLA("Mozilla"), KHTML("KHTML"), BLINK("Blink"), OTHER("Other");

	String name;

	RenderingEngine(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
