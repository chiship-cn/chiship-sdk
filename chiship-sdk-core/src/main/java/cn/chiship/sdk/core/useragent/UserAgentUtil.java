package cn.chiship.sdk.core.useragent;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * @author lijian
 */
public class UserAgentUtil implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(UserAgentUtil.class);

	private static final long serialVersionUID = 7025462762784240212L;

	private final OperatingSystem operatingSystem;

	private final Browser browser;

	private final int id;

	private String userAgentString;

	private UserAgentUtil(OperatingSystem operatingSystem, Browser browser) {
		this.operatingSystem = operatingSystem;
		this.browser = browser;
		this.id = (operatingSystem.getId() << 16) + browser.getId();
	}

	private UserAgentUtil(String userAgentString) {
		String userAgentLowercaseString = (userAgentString == null) ? null : userAgentString.toLowerCase();
		Browser browser = Browser.parseUserAgentLowercaseString(userAgentLowercaseString);
		OperatingSystem operatingSystem = OperatingSystem.UNKNOWN;
		if (browser != Browser.BOT) {
			operatingSystem = OperatingSystem.parseUserAgentLowercaseString(userAgentLowercaseString);
		}
		this.operatingSystem = operatingSystem;
		this.browser = browser;
		this.id = (operatingSystem.getId() << 16) + browser.getId();
		this.userAgentString = userAgentString;
	}

	/**
	 * 根据传递的字符串解析
	 * @param userAgentString
	 * @return 描述
	 */
	public static UserAgentUtil parseUserAgentString(String userAgentString) {
		return new UserAgentUtil(userAgentString);
	}

	public Version getBrowserVersion() {
		return this.browser.getVersion(this.userAgentString);
	}

	public OperatingSystem getOperatingSystem() {
		return this.operatingSystem;
	}

	public Browser getBrowser() {
		return this.browser;
	}

	public int getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return this.operatingSystem.toString() + "-" + this.browser.toString();
	}

	public static UserAgentUtil valueOf(int id) {
		OperatingSystem operatingSystem = OperatingSystem.valueOf(String.valueOf((short) (id >> 16)));
		Browser browser = Browser.valueOf(String.valueOf((short) (id & 0xFFFF)));
		return new UserAgentUtil(operatingSystem, browser);
	}

	public static UserAgentUtil valueOf(String name) {
		if (name == null) {
			throw new NullPointerException("Name is null");
		}
		String[] elements = name.split("-");
		int len = 2;
		if (elements.length == len) {
			OperatingSystem operatingSystem = OperatingSystem.valueOf(elements[0]);
			Browser browser = Browser.valueOf(elements[1]);
			return new UserAgentUtil(operatingSystem, browser);
		}
		throw new IllegalArgumentException("Invalid string for userAgent " + name);
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = 31 * result + ((this.browser == null) ? 0 : this.browser.hashCode());
		result = 31 * result + this.id;
		result = 31 * result + ((this.operatingSystem == null) ? 0 : this.operatingSystem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserAgentUtil other = (UserAgentUtil) obj;
		if (this.browser == null) {
			if (other.browser != null) {
				return false;
			}
		}
		else if (!this.browser.equals(other.browser)) {
			return false;
		}
		if (this.id != other.id) {
			return false;
		}
		if (this.operatingSystem == null) {
			if (other.operatingSystem != null) {
				return false;
			}
		}
		else if (!this.operatingSystem.equals(other.operatingSystem)) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		// ServletUtils.getRequest().getHeader("User-Agent")
		UserAgentUtil userAgent = UserAgentUtil.parseUserAgentString(
				"Mozilla/5.0 (Linux; Android 11; PEGM00 Build/RKQ1.200903.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/89.0.4389.72 MQQBrowser/6.2 TBS/045811 Mobile Safari/537.36 MMWEBID/6063 MicroMessenger/8.0.16.2040(0x2800103F) Process/tools WeChat/arm64 Weixin NetType/WIFI Language/zh_CN ABI/arm64");
		log.info(JSON.toJSON(userAgent).toString());
		log.info(userAgent.getBrowser().getName());
		log.info(userAgent.getOperatingSystem().getDeviceType().getName());

		userAgent = UserAgentUtil.parseUserAgentString(
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36");
		log.info(JSON.toJSON(userAgent).toString());
		log.info(userAgent.getBrowserVersion().getVersion());
		log.info(userAgent.getOperatingSystem().getDeviceType().getName());
		log.info(userAgent.getOperatingSystem().isMobileDevice() + "");
	}

}
