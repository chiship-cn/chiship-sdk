package cn.chiship.sdk.core.useragent;

interface VersionFetcher {

	/**
	 * 版本
	 * @param paramString
	 * @return 结果
	 */
	Version version(String paramString);

}
