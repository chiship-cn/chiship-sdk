package cn.chiship.sdk.core.useragent;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class VersionFetcherFromMap extends PatternBasedVersionFetcher {

	private final Map<String, Version> versions;

	VersionFetcherFromMap(Pattern pattern, Map<String, Version> versions) {
		super(pattern);
		this.versions = Collections.unmodifiableMap(versions);
	}

	@Override
	protected Version createVersion(Matcher matcher) {
		return this.versions.get(matcher.group(1));
	}

}
