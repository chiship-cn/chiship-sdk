package cn.chiship.sdk.core.util;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.RegexConstants;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lj 分析工具
 */
public class AnalysisUtil {

    private static final String BIRTHDAY_STR = "birthday";

    private AnalysisUtil() {
    }


    /**
     * 通过身份证号码获取出生日期(birthday)、出生日期时间戳(birthdayTimestamp)、年龄(age)、性别(sex)
     *
     * @param idCardNo 身份证号码
     * @return 返回的出生日期格式：1993-05-07 性别格式：1:男，2:女
     */
    public static BaseResult analysisIdCard(String idCardNo) {
        if (StringUtil.isNullOrEmpty(idCardNo)) {
            return BaseResult.error("身份证号不能为空");
        }
        if (!RegexConstants.check(idCardNo, RegexConstants.ID_CARD)) {
            return BaseResult.error("身份证号码不正确");
        }
        Map<String, Object> idCardMap = new HashMap<>(7);
        idCardMap.put("ID", idCardNo);

        int[] province = {11, 12, 13, 14, 15, 21, 22, 23, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 50, 51, 52, 53, 54, 61, 62, 63, 64, 65, 71, 81, 82, 91};
        if (Arrays.binarySearch(province, Integer.valueOf(idCardNo.substring(0, 2))) == -1) {
            return BaseResult.error("身份证地区非法");
        }
        char[] number = idCardNo.toCharArray();
        if (number.length == 15) {
            firstIdCard(idCardMap);
        }
        if (number.length == 18) {
            //校验因子
            char[] cardArray = idCardNo.toCharArray();
            int[] power = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
            char[] validateCode = {'1', '0', 'x', '9', '8', '7', '6', '5', '4', '3', '2'};
            int sum = 0;
            for (int i = 0; i < 17; i++) {
                sum += (cardArray[i] - '0') * power[i];
            }
            char code = validateCode[sum % 11];
            if (Boolean.FALSE.equals(code == cardArray[17] || code == Character.toLowerCase(cardArray[17]))) {
                return BaseResult.error("输入的身份证号非法");
            }
            secondIdCard(idCardMap);
        }
        if (!idCardMap.containsKey(BIRTHDAY_STR)) {
            return BaseResult.error("生日解析失败");
        }
        String birthday = idCardMap.get(BIRTHDAY_STR).toString();
        if (!StringUtil.isNullOrEmpty(birthday)) {
            idCardMap.put("birthdayTime", DateUtils.dateTime(DateUtils.YYYY_MM_DD, birthday).getTime());
        } else {
            return BaseResult.error("生日解析失败");
        }
        idCardMap.put("province", idCardNo.substring(0, 2) + "0000");
        idCardMap.put("city", idCardNo.substring(0, 4) + "00");
        idCardMap.put("district", idCardNo.substring(0, 6));
        return BaseResult.ok(idCardMap);
    }

    /**
     * 一代身份证
     *
     * @param idCardMap
     * @return
     */
    static void firstIdCard(Map<String, Object> idCardMap) {
        String idCardNo = idCardMap.get("ID").toString();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String birthday = "19" + idCardNo.substring(6, 8) + "-" + idCardNo.substring(8, 10) + "-"
                + idCardNo.substring(10, 12);
        Byte gender = Integer.parseInt(idCardNo.substring(idCardNo.length() - 3, idCardNo.length())) % 2 == 0 ? Byte.valueOf("2")
                : Byte.valueOf("1");
        Integer age = (year - Integer.parseInt("19" + idCardNo.substring(6, 8)));
        idCardMap.put(BIRTHDAY_STR, birthday);
        idCardMap.put("age", age);
        idCardMap.put("gender", gender);
        idCardMap.put("type", "一代身份证");
    }

    /**
     * 二代身份证
     *
     * @param idCardMap
     * @return
     */
    static void  secondIdCard(Map<String, Object> idCardMap) {
        String idCardNo = idCardMap.get("ID").toString();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String birthday = idCardNo.substring(6, 10) + "-" + idCardNo.substring(10, 12) + "-" + idCardNo.substring(12, 14);
        Byte gender = Integer.parseInt(idCardNo.substring(idCardNo.length() - 4, idCardNo.length() - 1)) % 2 == 0 ? Byte.valueOf("2")
                : Byte.valueOf("1");
        Integer age = (year - Integer.parseInt(idCardNo.substring(6, 10)));
        idCardMap.put(BIRTHDAY_STR, birthday);
        idCardMap.put("age", age);
        idCardMap.put("gender", gender);
        idCardMap.put("type", "二代身份证");
    }

    public static void main(String[] args) {
        PrintUtil.console(AnalysisUtil.analysisIdCard("371425199111183212"));

    }

}
