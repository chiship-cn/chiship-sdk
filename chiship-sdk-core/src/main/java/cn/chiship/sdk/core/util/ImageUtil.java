package cn.chiship.sdk.core.util;

import cn.chiship.sdk.core.base.BaseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Base64;

/**
 * @author 李剑 将图片转换为Base64<br>
 * 将base64编码字符串解码成img图片
 */
public class ImageUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImageUtil.class);

	private ImageUtil() {
	}

	/**
	 * 将图片转换成Base64编码
	 * @param imgFile 待处理图片
	 * @return 结果
	 */
	public static String getImgStrByFile(String imgFile) throws IOException {
		byte[] data = null;
		try (InputStream in = new FileInputStream(imgFile);) {
			data = new byte[in.available()];
			in.read(data);
		}
		catch (IOException e) {
			LOGGER.error("发生异常", e);
		}
		Base64.Encoder encoder = Base64.getEncoder();
		return new String(encoder.encode(data));
	}

	/**
	 * 将图片转换成字节数组
	 * @param imgFile 待处理图片
	 * @return 结果
	 */
	public static byte[] getImgByteByFile(String imgFile) {
		byte[] data = null;
		try (InputStream in = new FileInputStream(imgFile);) {
			data = new byte[in.available()];
			in.read(data);
		}
		catch (IOException e) {
			LOGGER.error("发生异常", e);
		}
		return data;
	}

	/**
	 * 网络图片转换base64
	 * @param url
	 * @return 结果
	 */
	public static String getImgBase64ByUrl(String url) {
		return FileUtil.downLoadToBase64(url);
	}

	/**
	 * 网络图片转换Byte
	 * @param url
	 * @return 结果
	 */
	public static byte[] getImgByteByUrl(String url) {
		return FileUtil.downLoadToByte(url);

	}

	/**
	 * 下载网络图片到本地
	 * @param url 网络地址
	 * @param savePath 保存文件夹
	 * @param saveName 保存文件名
	 * @return 结果
	 */
	public static BaseResult downloadPicture(String url, String savePath, String saveName) {
		return FileUtil.downloadFile(url, savePath, saveName);
	}

	/**
	 * 对字节数组字符串进行Base64解码并生成图片
	 * @param imgBase64 Base64图片数据
	 * @param imgFilePath 保存图片全路径地址
	 * @return 结果
	 */
	public static boolean generateImage(String imgBase64, String imgFilePath) {
		if (StringUtil.isNullOrEmpty(imgBase64)) {
			return false;
		}
		try (OutputStream out = new FileOutputStream(imgFilePath);) {
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] b = decoder.decode(imgBase64);
			for (int i = 0; i < b.length; ++i) {
				if (b[i] < 0) {
					b[i] += 256;
				}
			}

			out.write(b);
			out.flush();
			out.close();
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

}
