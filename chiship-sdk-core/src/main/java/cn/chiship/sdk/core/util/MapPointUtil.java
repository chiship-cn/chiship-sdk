package cn.chiship.sdk.core.util;

import java.math.BigDecimal;

/**
 * 地图坐标转换 WGS84坐标系 地球坐标系，国际通用坐标系 GCJ02坐标系 火星坐标系，WGS84坐标系加密后的坐标系；Google国内地图、高德、QQ地图 使用
 * BD09坐标系 百度坐标系，GCJ02坐标系加密后的坐标系
 *
 * @author lijian
 */
public class MapPointUtil {

	private MapPointUtil() {
	}

	/**
	 * Google国内地图、高德、QQ地图转百度
	 * @param lng
	 * @param lat
	 * @return 结果
	 */
	public static double[] gcj02ToBd09(double lng, double lat, int digit) {
		double[] latLng = new double[2];
		double pi = 3.14159265358979324 * 3000.0 / 180.0;
		double x = lng;
		double y = lat;
		double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * pi);
		double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * pi);
		latLng[0] = z * Math.cos(theta) + 0.0065;
		latLng[1] = z * Math.sin(theta) + 0.006;
		latLng[0] = (BigDecimal.valueOf(latLng[0]).setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue());
		latLng[1] = (BigDecimal.valueOf(latLng[1]).setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue());
		return latLng;
	}

	/**
	 * 百度转Google国内地图、高德、QQ地图
	 * @param lng
	 * @param lat
	 * @return 结果
	 */
	public static double[] bd09ToGcj02(double lng, double lat, int digit) {
		double[] latLng = new double[2];
		double pi = 3.14159265358979324 * 3000.0 / 180.0;
		double x = lng - 0.0065;
		double y = lat - 0.006;
		double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * pi);
		double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * pi);
		latLng[0] = z * Math.cos(theta);
		latLng[1] = z * Math.sin(theta);
		latLng[0] = (BigDecimal.valueOf(latLng[0]).setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue());
		latLng[1] = (BigDecimal.valueOf(latLng[1]).setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue());

		return latLng;
	}

}
