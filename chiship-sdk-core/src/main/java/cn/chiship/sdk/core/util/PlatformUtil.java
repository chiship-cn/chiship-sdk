package cn.chiship.sdk.core.util;

/**
 * @author lj
 */
public class PlatformUtil {

	private PlatformUtil() {

	}

	/**
	 * 是否Linux
	 * @return 结果
	 */
	public static boolean isLinux() {
		String os = System.getProperty("os.name");
		String systemName = "win";
		return !os.toLowerCase().startsWith(systemName);
	}

	/**
	 * 判断win系统是否64位
	 * @return 结果
	 */
	public static boolean isWin64Bit() {
		boolean is64bit = false;
		if (System.getProperty("os.name").contains("Windows")) {
			is64bit = (System.getenv("ProgramFiles(x86)") != null);
		}
		else {
			is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
		}
		return is64bit;
	}

}
