package cn.chiship.sdk.core.util;

import cn.chiship.sdk.core.encryption.AesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

/**
 * Redis 工具类
 *
 * @author lijian
 */
public class RedisUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisUtil.class);

	private static final String REDIS = "redis";

	/**
	 * Redis服务器IP
	 */
	private static String IP = PropertiesFileUtil.getInstance(REDIS).get("master.redis.ip");

	/**
	 * Redis的端口号
	 */
	private static int PORT = PropertiesFileUtil.getInstance(REDIS).getInt("master.redis.port");

	/**
	 * 访问密码
	 */
	private static String PASSWORD = AesUtil
			.aesDecode(PropertiesFileUtil.getInstance(REDIS).get("master.redis.password"));

	/**
	 * 可用连接实例的最大数目，默认值为8；
	 * 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
	 */
	private static int MAX_ACTIVE = PropertiesFileUtil.getInstance(REDIS).getInt("master.redis.max_active");

	/**
	 * 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
	 */
	private static int MAX_IDLE = PropertiesFileUtil.getInstance(REDIS).getInt("master.redis.max_idle");

	/**
	 * 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
	 */
	private static int MAX_WAIT = PropertiesFileUtil.getInstance(REDIS).getInt("master.redis.max_wait");

	/**
	 * 超时时间
	 */
	private static int TIMEOUT = PropertiesFileUtil.getInstance(REDIS).getInt("master.redis.timeout");

	/**
	 * 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
	 */
	private static boolean TEST_ON_BORROW = false;

	private static JedisPool jedisPool = null;

	/**
	 * redis过期时间,以秒为单位 一小时
	 */
	public static final int EXRP_HOUR = 60 * 60;

	/**
	 * redis过期时间,以秒为单位 一天
	 */
	public static final int EXRP_DAY = 60 * 60 * 24;

	/**
	 * redis过期时间,以秒为单位 一个月
	 */
	public static final int EXRP_MONTH = 60 * 60 * 24 * 30;

	private RedisUtil() {
	}

	/**
	 * 初始化Redis连接池
	 */
	private static void initialPool() {
		try {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxTotal(MAX_ACTIVE);
			config.setMaxIdle(MAX_IDLE);
			config.setMaxWaitMillis(MAX_WAIT);
			config.setTestOnBorrow(TEST_ON_BORROW);
			jedisPool = new JedisPool(config, IP, PORT, TIMEOUT);
		}
		catch (Exception e) {
			LOGGER.error("First create JedisPool error : {}", e);
		}
	}

	/**
	 * 在多线程环境同步初始化
	 */
	private static synchronized void poolInit() {
		if (null == jedisPool) {
			initialPool();
		}
	}

	public static synchronized JedisPool getJedisPool() {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(MAX_ACTIVE);
		config.setMaxIdle(MAX_IDLE);
		config.setMaxWaitMillis(MAX_WAIT);
		config.setTestOnBorrow(TEST_ON_BORROW);
		return new JedisPool(config, IP, PORT);
	}

	/**
	 * 同步获取Jedis实例
	 * @return 结果 Jedis
	 */
	public static synchronized Jedis getJedis() {
		poolInit();
		Jedis jedis = null;
		try {
			PrintUtil.console(String.format("host:%s,port:%s,password:%s", IP, PORT, PASSWORD));
			if (null != jedisPool) {
				jedis = jedisPool.getResource();
				if (!StringUtil.isNullOrEmpty(PASSWORD)) {
					jedis.auth(PASSWORD);
				}

			}
		}
		catch (Exception e) {
			LOGGER.error("Get jedis error : {}", e);
		}
		return jedis;
	}

	/**
	 * 设置 String
	 * @param key 键
	 * @param value 值
	 */
	public static synchronized void set(String key, String value) {
		value = StringUtil.isNullOrEmpty(value) ? "" : value;
		Jedis jedis = getJedis();
		if (jedis != null) {
			jedis.set(key, value);
			jedis.close();
		}

	}

	/**
	 * 设置 byte[]
	 * @param key 键
	 * @param value 值
	 */
	public static synchronized void set(byte[] key, byte[] value) {
		try {
			Jedis jedis = getJedis();
			if (jedis != null) {
				jedis.set(key, value);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("Set key error : {}", e);
		}
	}

	/**
	 * 设置 String 过期时间
	 * @param key 键
	 * @param value 值
	 * @param seconds 以秒为单位
	 */
	public static synchronized void set(String key, String value, int seconds) {
		try {
			value = StringUtil.isNullOrEmpty(value) ? "" : value;
			Jedis jedis = getJedis();
			if (jedis != null) {
				jedis.setex(key, seconds, value);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("Set keyex error : {}", e);
		}
	}

	/**
	 * 设置 byte[] 过期时间
	 * @param key 键
	 * @param value 值
	 * @param seconds 以秒为单位
	 */
	public static synchronized void set(byte[] key, byte[] value, int seconds) {
		try {
			Jedis jedis = getJedis();
			if (jedis != null) {
				jedis.set(key, value);
				jedis.expire(key, seconds);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("Set key error : {}", e);
		}
	}

	/**
	 * 获取String值
	 * @param key 键值
	 * @return 结果 value
	 */
	public static synchronized String get(String key) {
		Jedis jedis = getJedis();
		if (null == jedis) {
			return null;
		}
		String value = jedis.get(key);
		jedis.close();
		return value;
	}

	/**
	 * 获取byte[]值
	 * @param key 键
	 * @return 结果 value
	 */
	public static synchronized byte[] get(byte[] key) {
		Jedis jedis = getJedis();
		if (null == jedis) {
			return null;
		}
		byte[] value = jedis.get(key);
		jedis.close();
		return value;
	}

	/**
	 * 删除值
	 * @param key 键
	 */
	public static synchronized void remove(String key) {
		try {
			Jedis jedis = getJedis();
			if (jedis != null) {
				jedis.del(key);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("Remove keyex error : {}", e);
		}
	}

	/**
	 * 删除值
	 * @param key 键
	 */
	public static synchronized void remove(byte[] key) {
		try {
			Jedis jedis = getJedis();
			if (jedis != null) {
				jedis.del(key);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("Remove keyex error : {}", e);
		}
	}

	/**
	 * lpush
	 * @param key 键
	 * @param strings 值
	 */
	public static synchronized void lpush(String key, String... strings) {
		try {
			Jedis jedis = RedisUtil.getJedis();
			if (jedis != null) {
				jedis.lpush(key, strings);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("lpush error : {}", e);
		}
	}

	/**
	 * lrem
	 * @param key 键
	 * @param count 长度
	 * @param value 值
	 */
	public static synchronized void lrem(String key, long count, String value) {
		try {
			Jedis jedis = RedisUtil.getJedis();
			if (jedis != null) {
				jedis.lrem(key, count, value);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("lpush error : {}", e);
		}
	}

	/**
	 * sadd
	 * @param key 键
	 * @param value 值
	 * @param seconds 时间
	 */
	public static synchronized void sadd(String key, String value, int seconds) {
		try {
			Jedis jedis = RedisUtil.getJedis();
			if (jedis != null) {
				jedis.sadd(key, value);
				jedis.expire(key, seconds);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("sadd error : {}", e);
		}
	}

	/**
	 * incr
	 * @param key 键
	 * @return 结果 value
	 */
	public static synchronized Long incr(String key) {
		Jedis jedis = getJedis();
		if (null == jedis) {
			return null;
		}
		long value = jedis.incr(key);
		jedis.close();
		return value;
	}

	/**
	 * decr
	 * @param key 键
	 * @return 结果 value
	 */
	public static synchronized Long decr(String key) {
		Jedis jedis = getJedis();
		if (null == jedis) {
			return null;
		}
		long value = jedis.decr(key);
		jedis.close();
		return value;
	}

	/**
	 * expire
	 * @param key 键
	 * @param seconds 秒
	 */
	public static synchronized void expire(String key, int seconds) {
		try {
			Jedis jedis = RedisUtil.getJedis();
			if (jedis != null) {
				jedis.expire(key, seconds);
				jedis.close();
			}
		}
		catch (Exception e) {
			LOGGER.error("expire error : {}", e);
		}
	}

	public static synchronized void publishMsg(String channel, String message) {
		Jedis jedis = null;
		try {
			jedis = RedisUtil.getJedis();
			if (jedis != null) {
				jedis.publish(channel, message);
			}
		}
		catch (Exception e) {
			LOGGER.error("publishMsg error : {}", e);
		}
		finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

	/**
	 * 接收消息。在main方法调用后，会一直执行下去。当有发布对应消息时，就会在jedisPubSub中接收到！
	 * @param jedisPubSub 对象
	 * @param channels 通道
	 */
	public static synchronized void subscribeMsg(JedisPubSub jedisPubSub, String channels) {
		Jedis jedis = null;
		try {
			jedis = RedisUtil.getJedis();
			if (jedis != null) {
				jedis.subscribe(jedisPubSub, channels);
			}
		}
		catch (Exception e) {
			LOGGER.error("消息订阅", e);
		}
		finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

}
