package cn.chiship.sdk.core.util;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * String 工具类
 *
 * @author LiJian
 */
public class StringUtil extends StringUtils {

	private static Pattern linePattern = Pattern.compile("_(\\w)");

	private static Pattern humpPattern = Pattern.compile("[A-Z]");

	/**
	 * 下划线转驼峰 大驼峰
	 * @param str
	 * @return 结果
	 */
	public static String lineToPascal(String str) {
		if (null == str || "".equals(str)) {
			return str;
		}
		// str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);

		str = sb.toString();
		str = str.substring(0, 1).toUpperCase() + str.substring(1);

		return str;
	}

	/**
	 * 下划线转驼峰 小驼峰
	 * @param str 字符串
	 * @return 结果
	 */
	public static String lineToCamel(String str) {
		return toLowerCaseFirstOne(lineToPascal(str));
	}

	/**
	 * 驼峰转下划线
	 * @param str
	 * @return 结果
	 */
	public static String humpToLine(String str) {
		Matcher matcher = humpPattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 驼峰转下划线(简单写法，效率低于{@link #humpToLine(String)})
	 * @param str
	 * @return 结果
	 */
	public static String humpToLine2(String str) {
		return str.replaceAll("[A-Z]", "_$0").toLowerCase();
	}

	/**
	 * 首字母转小写
	 * @param s
	 * @return 结果
	 */
	public static String toLowerCaseFirstOne(String s) {
		if (StringUtil.isBlank(s)) {
			return s;
		}
		if (Character.isLowerCase(s.charAt(0))) {
			return s;
		}
		else {
			return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
		}
	}

	/**
	 * 首字母转大写
	 * @param s
	 * @return 结果
	 */
	public static String toUpperCaseFirstOne(String s) {
		if (StringUtil.isBlank(s)) {
			return s;
		}
		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}
		else {
			return (new StringBuffer()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
		}
	}

	/**
	 * object转String
	 * @param object
	 * @return 结果
	 */
	public static String getString(Object object) {
		return getString(object, "");
	}

	public static String getString(Object object, String defaultValue) {
		if (null == object) {
			return defaultValue;
		}
		try {
			return object.toString();
		}
		catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * object转Integer
	 * @param object
	 * @return 结果
	 */
	public static int getInt(Object object) {
		return getInt(object, -1);
	}

	public static int getInt(Object object, Integer defaultValue) {
		if (null == object) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(object.toString());
		}
		catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * object转Boolean
	 * @param object
	 * @return 结果
	 */
	public static boolean getBoolean(Object object) {
		return getBoolean(object, false);
	}

	public static boolean getBoolean(Object object, Boolean defaultValue) {
		if (null == object) {
			return defaultValue;
		}
		try {
			return Boolean.parseBoolean(object.toString());
		}
		catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * null值与空字符串判断，为空返回真，否则返回假;
	 * @param str
	 * @return 结果
	 */
	public static boolean isNullOrEmpty(String str) {
		return (str == null || str.isEmpty() || "".equals(str.trim()));

	}

	/**
	 * * 判断一个Collection是否为空， 包含List，Set，Queue
	 * @param coll 要判断的Collection
	 * @return 结果 true：为空 false：非空
	 */
	public static boolean isEmpty(Collection<?> coll) {
		return isNull(coll) || coll.isEmpty();
	}

	/**
	 * * 判断一个对象数组是否为空
	 * @param objects 要判断的对象数组 * @return 结果 true：为空 false：非空
	 */
	public static boolean isEmpty(Object[] objects) {
		return isNull(objects) || (objects.length == 0);
	}

	/**
	 * * 判断一个对象是否为空
	 * @param object Object
	 * @return 结果 true：为空 false：非空
	 */
	public static boolean isNull(Object object) {
		return object == null;
	}

	/**
	 * 判断字符不为空
	 * @param str 字符串
	 * @return boolean
	 */
	public static boolean isNotEmpty(String str) {
		return !isNullOrEmpty(str);
	}

	/**
	 * 去空格
	 */
	public static String trim(String str) {
		return (str == null ? "" : str.trim());
	}

	/**
	 * 是否包含字符串
	 * @param str 验证字符串
	 * @param strs 字符串组
	 * @return 结果 包含返回true
	 */
	public static boolean inStringIgnoreCase(String str, String... strs) {
		if (str != null && strs != null) {
			for (String s : strs) {
				if (str.equalsIgnoreCase(trim(s))) {
					return true;
				}
			}
		}
		return false;
	}

	public static String[] toLowerCase(String[] strArr) {
		if (strArr == null) {
			return new String[0];
		}
		String[] res = new String[strArr.length];
		for (int i = 0; i < strArr.length; i++) {
			res[i] = strArr[i].toLowerCase();
		}
		return res;
	}

	public static boolean contains(String str, String[] strArr) {
		if (strArr == null) {
			return false;
		}
		for (String arrStr : strArr) {
			if (str.contains(arrStr)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 字符串已指定格式转字节类型型集合
	 * @param val
	 * @param split
	 * @return 结果
	 */
	public static List<Byte> strToListByte(String val, String split) {
		List<Byte> result = new ArrayList<>();
		try {
			String[] v = val.split(split);
			for (String s : v) {
				result.add(Byte.valueOf(s));
			}
		}
		catch (Exception e) {
			throw new BusinessException("分割后的参数不是数字");
		}
		return result;
	}

	/**
	 * 字符串已指定格式转短整型集合
	 * @param val
	 * @param split
	 * @return 结果
	 */
	public static List<Integer> strToListInteger(String val, String split) {
		List<Integer> result = new ArrayList<>();
		try {
			String[] v = val.split(split);
			for (String s : v) {
				result.add(Integer.valueOf(s));
			}
		}
		catch (Exception e) {
			throw new BusinessException("分割后的参数不是数字");
		}
		return result;
	}

	/**
	 * 字符串已指定格式转长整型集合
	 * @param val
	 * @param split
	 * @return 结果
	 */
	public static List<Long> strToListLong(String val, String split) {
		List<Long> result = new ArrayList<>();
		try {
			String[] v = val.split(split);
			for (String s : v) {
				result.add(Long.valueOf(s));
			}
		}
		catch (Exception e) {
			throw new BusinessException("分割后的参数不是数字");
		}
		return result;
	}

	/**
	 * 字符串已指定格式转字符串集合
	 * @param val
	 * @param split
	 * @return 结果
	 */
	public static List<String> strToListString(String val, String split) {
		return Arrays.asList(val.split(split));
	}

	/**
	 * 格式化百分比
	 * @param count 计算百分比第一个数
	 * @param total 计算百分比第二个数
	 * @return 返回百分比
	 */
	public static String formatPercent(Integer count, Integer total) {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(count * 100.00 / total);
	}

	/**
	 * 获得排序字符串
	 * @param sort 排序
	 * @return String
	 */
	public static String getOrderByValue(String sort) {
		if (isNullOrEmpty(sort)) {
			sort = "-gmt_modified";
		}
		StringBuilder buffer = new StringBuilder();
		String a = ",";
		for (String s : sort.split(a)) {
			if ("+".equals(sort.substring(0, 1))) {
				buffer.append(CamelCaseUtils.toUnderlineName(s.substring(1)) + " ASC");
			}
			else {
				buffer.append(CamelCaseUtils.toUnderlineName(s.substring(1)) + " DESC");
			}
			buffer.append(",");
		}
		if (buffer.length() > 0) {
			sort = buffer.substring(0, buffer.length() - 1);
		}
		return sort;
	}

}
