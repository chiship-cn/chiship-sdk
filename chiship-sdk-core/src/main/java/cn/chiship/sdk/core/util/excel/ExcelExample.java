package cn.chiship.sdk.core.util.excel;

import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.excel.core.ExcelEnum;
import cn.chiship.sdk.core.util.excel.core.ExcelService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Excel使用案例
 *
 * @author lj
 */
public class ExcelExample {

	/**
	 * 读取文件
	 */
	public static void test() {
		Map<String, String> headerTitleMap = new LinkedHashMap<>();
		headerTitleMap.put("index", "所属社保机构（必填）");
		headerTitleMap.put("name", "医保编码（必填）");
		headerTitleMap.put("name3", "国家医保平台编码（必填）");
		headerTitleMap.put("name1", "医疗机构名称");
		headerTitleMap.put("name2", "医院医保结算级别（必填）");

		ExcelService excelService = ExcelFactory.getExcelService(ExcelEnum.XLS);
		excelService.load("D:\\医院代码对照.xls");
		PrintUtil.console(excelService.readExcel("Sheet1", 1, 2, headerTitleMap));
		PrintUtil.console(excelService.getSheetNames());

		excelService = ExcelFactory.getExcelService(ExcelEnum.XLSX);
		excelService.load("D:\\医院代码对照.xlsx");
		PrintUtil.console(excelService.readExcel(1, 1, 2, headerTitleMap));
		PrintUtil.console(excelService.getSheetNames());
	}

	public static void test1() throws FileNotFoundException {
		List<String> headerTitles = new ArrayList<>();
		headerTitles.add("序号");
		headerTitles.add("专职律师身份");
		headerTitles.add("性别");
		headerTitles.add("执业类别");
		List<List<String>> values = new ArrayList<>();
		int len = 2;
		for (Integer i = 0; i < len; i++) {
			List<String> s = new ArrayList<>();
			s.add(String.valueOf(i));
			s.add("371428199111183212");
			s.add("633180341827878912");
			s.add("3123");
			values.add(s);
		}
		ExcelService excelService = ExcelFactory.getExcelService(ExcelEnum.XLSX);
		FileOutputStream os = new FileOutputStream("D:/2.xlsx");
		excelService.writeExcel(os, "demo", "demo", headerTitles, values);

		excelService = ExcelFactory.getExcelService(ExcelEnum.XLS);
		os = new FileOutputStream("D:/2.xls");
		excelService.writeExcel(os, "demo", "demo", headerTitles, values);
	}

	public static void test2() {
		List<String> headerTitles = new ArrayList<>();
		headerTitles.add("序号");
		headerTitles.add("专职律师身份");
		headerTitles.add("性别");
		headerTitles.add("执业类别");
		List<List<String>> values = new ArrayList<>();
		int len = 2;
		for (Integer i = 0; i < len; i++) {
			List<String> s = new ArrayList<>();
			s.add(String.valueOf(i));
			s.add("371428199111183212");
			s.add("633180341827878912");
			s.add("3123");
			values.add(s);
		}
		ExcelService excelService = ExcelFactory.getExcelService(ExcelEnum.XLSX);
		excelService.writeExcel("D:/111/", "demo", "demo", "测试标题", headerTitles, values);

		excelService = ExcelFactory.getExcelService(ExcelEnum.XLS);
		excelService.writeExcel("D:/111/", "demo", "demo", "测试标题", headerTitles, values);
	}

	public static void test3() throws FileNotFoundException {
		List<String> headerTitles = new ArrayList<>();
		headerTitles.add("序号");
		headerTitles.add("专职律师身份");
		headerTitles.add("性别");
		headerTitles.add("执业类别");
		List<List<String>> values = new ArrayList<>();
		int len = 2;
		for (Integer i = 0; i < len; i++) {
			List<String> s = new ArrayList<>();
			s.add(String.valueOf(i));
			s.add("371428199111183212");
			s.add("633180341827878912");
			s.add("3123");
			values.add(s);
		}
		FileOutputStream os = new FileOutputStream("d:/1.xlsx");
		ExcelService excelService = ExcelFactory.getExcelService(ExcelEnum.XLSX);
		excelService.writeExcel("demo", "1", 1, headerTitles, values);
		excelService.writeExcel("demo2", "2", 2, headerTitles, values);
		excelService.writeAndClose(os);

		os = new FileOutputStream("d:/1.xls");
		excelService = ExcelFactory.getExcelService(ExcelEnum.XLS);
		excelService.writeExcel("demo", "1", 1, headerTitles, values);
		excelService.writeExcel("demo2", "2", 2, headerTitles, values);
		excelService.writeAndClose(os);
	}

	public static void test4() throws FileNotFoundException {
		Map<String, String> headerTitleMap = new LinkedHashMap<>();
		headerTitleMap.put("userName", "标题1");
		headerTitleMap.put("realName", "标题2");
		headerTitleMap.put("gender", "标题3");
		ExcelService excelService = ExcelFactory.getExcelService(ExcelEnum.XLSX);
		excelService.load(new FileInputStream("D:\\Downloads\\示例模板.xlsx"));
		PrintUtil.console(excelService.readExcel(1, 3, 4, headerTitleMap));
		PrintUtil.console(excelService.getSheetNames());
	}

	public static void main(String[] args) throws FileNotFoundException {
		test4();

	}

}
