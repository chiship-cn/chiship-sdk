package cn.chiship.sdk.core.util.excel;

import cn.chiship.sdk.core.util.excel.core.ExcelEnum;
import cn.chiship.sdk.core.util.excel.core.ExcelHSSFService;
import cn.chiship.sdk.core.util.excel.core.ExcelService;
import cn.chiship.sdk.core.util.excel.core.ExcelXSSFService;
import org.apache.commons.lang3.ObjectUtils;

/**
 * ExcelFactory工厂类,根据系统用户配置，生成及缓存对应的上传下载接口实现
 *
 * @author lj
 */
public class ExcelFactory {

	private ExcelFactory() {
	}

	/**
	 * 获取 ExcelService
	 * @param excelEnum
	 * @return ExcelService
	 */
	public static ExcelService getExcelService(ExcelEnum excelEnum) {
		ExcelService excelService = null;
		if (ObjectUtils.isEmpty(excelService)) {
			if (ExcelEnum.XLS.equals(excelEnum)) {
				excelService = new ExcelHSSFService();
			}
			if (ExcelEnum.XLSX.equals(excelEnum)) {
				excelService = new ExcelXSSFService();
			}
		}
		return excelService;
	}

}
