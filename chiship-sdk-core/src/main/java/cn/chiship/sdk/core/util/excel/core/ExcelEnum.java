package cn.chiship.sdk.core.util.excel.core;

/**
 * @author lijian DfsEnum 分布式存储枚举 ，因dfs表存的是数据字典表的id，这里省一次数据库查询，所以就用数据字典的id
 */
public enum ExcelEnum {

	/**
	 * xls
	 */
	XLS("xls"),

	/**
	 * xlsx
	 */
	XLSX("xlsx"),

	/**
	 * csv
	 */
	CSV("csv"),

	;

	private String type;

	ExcelEnum(String type) {
		this.type = type;
	}

	/**
	 * 根据标识返回枚举
	 * @param type 标识
	 * @return ExcelEnum
	 */
	public static ExcelEnum getExcelEnum(String type) {
		ExcelEnum[] dfsEnums = values();
		for (ExcelEnum excelEnum : dfsEnums) {
			if (excelEnum.getType().equals(type)) {
				return excelEnum;
			}
		}
		return null;
	}

	public String getType() {
		return type;
	}

}