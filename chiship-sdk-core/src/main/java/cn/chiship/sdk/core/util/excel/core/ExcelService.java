package cn.chiship.sdk.core.util.excel.core;

import cn.chiship.sdk.core.base.BaseResult;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * @author lijian
 */
public interface ExcelService {

	/**
	 * 文件载入源
	 * @param file 文件
	 */
	void load(String file);

	/**
	 * 输入流载入源
	 * @param in 文件流
	 */
	void load(InputStream in);

	/**
	 * 获得文件载入源流
	 * @return 文件流
	 */
	InputStream getInputStream() throws IOException;

	/**
	 * sheet索引形式读取内容
	 * @param sheetIndex 索引 从1开始
	 * @param firstHeaderRow 表头 从1开始
	 * @param firstDataRow 数据
	 * @param headerTitleMap 表头对应 格式如：自定义代码,表头名称
	 * @return BaseResult
	 */
	BaseResult readExcel(Integer sheetIndex, Integer firstHeaderRow, Integer firstDataRow,
			Map<String, String> headerTitleMap);

	/**
	 * sheet名称形式读取内容
	 * @param sheetName 名称
	 * @param firstHeaderRow 表头 从1开始
	 * @param firstDataRow 数据
	 * @param headerTitleMap 表头对应 格式如：自定义代码,表头名称
	 * @return BaseResult
	 */
	BaseResult readExcel(String sheetName, Integer firstHeaderRow, Integer firstDataRow,
			Map<String, String> headerTitleMap);

	/**
	 * 获得excel文件流
	 * @param sheetName sheet名称
	 * @param sheetTitle sheet标题
	 * @param headerNames 行标题
	 * @param values 数值
	 * @return ByteArrayInputStream
	 */
	ByteArrayInputStream getWriteExcelByteArrayInputStream(String sheetName, String sheetTitle,
			List<String> headerNames, List<List<String>> values);

	/**
	 * 获得excel字节数组
	 * @param sheetName sheet名称
	 * @param sheetTitle sheet标题
	 * @param headerNames 行标题
	 * @param values 数值
	 * @return byte[]
	 */
	byte[] getWriteExcelBytes(String sheetName, String sheetTitle, List<String> headerNames, List<List<String>> values);

	/**
	 * 写入指定位置文件
	 * @param filePath 文件路径
	 * @param fileName 文件名称
	 * @param sheetName sheet名称
	 * @param sheetTitle sheet标题
	 * @param headerNames 行标题
	 * @param values 数值
	 * @return BaseResult
	 */
	BaseResult writeExcel(String filePath, String fileName, String sheetName, String sheetTitle,
			List<String> headerNames, List<List<String>> values);

	/**
	 * 通过输出文件流写出
	 * @param os 输出文件流
	 * @param sheetName sheet名称
	 * @param sheetTitle sheet标题
	 * @param headerNames 行标题
	 * @param values 数值
	 */
	void writeExcel(OutputStream os, String sheetName, String sheetTitle, List<String> headerNames,
			List<List<String>> values);

	/**
	 * 通过默认表格配置写出
	 * @param sheetName sheet名称
	 * @param sheetTitle sheet标题
	 * @param sheetNum 第几张sheet
	 * @param headerNames 行标题
	 * @param values 数值
	 */
	void writeExcel(String sheetName, String sheetTitle, Integer sheetNum, List<String> headerNames,
			List<List<String>> values);

	/**
	 * 输出指定文件流
	 * @param os
	 */
	void writeAndClose(OutputStream os);

	/**
	 * 获取Sheet名称
	 * @return List
	 */
	List<String> getSheetNames();

}
