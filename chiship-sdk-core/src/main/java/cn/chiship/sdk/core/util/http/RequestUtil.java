package cn.chiship.sdk.core.util.http;

import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * request工具类
 *
 * @author lijian
 */
public class RequestUtil {

	/**
	 * 移除request指定参数
	 * @param request 请求
	 * @param paramName 参数名
	 * @return 结果
	 */
	public String removeParam(HttpServletRequest request, String paramName) {
		String queryString = "";
		Enumeration<String> keys = request.getParameterNames();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (key.equals(paramName)) {
				continue;
			}
			if ("".equals(queryString)) {
				queryString = key + "=" + request.getParameter(key);
			}
			else {
				queryString += "&" + key + "=" + request.getParameter(key);
			}
		}
		return queryString;
	}

	/**
	 * 获取请求basePath
	 * @param request 请求
	 * @return 结果
	 */
	public static String getBasePath(HttpServletRequest request) {
		StringBuilder basePath = new StringBuilder();
		String scheme = request.getScheme();
		String domain = request.getServerName();
		int port = request.getServerPort();
		basePath.append(scheme);
		basePath.append("://");
		basePath.append(domain);
		if (BaseConstants.NETWORKING_PROTOCOL_HTTP.equalsIgnoreCase(scheme)
				&& BaseConstants.HTTP_PORT_DEFAULT != port) {
			basePath.append(":").append(port);
		}
		else if (BaseConstants.NETWORKING_PROTOCOL_HTTPS.equalsIgnoreCase(scheme)
				&& port != BaseConstants.HTTPS_PORT_DEFAULT) {
			basePath.append(":").append(port);
		}
		return basePath.toString();
	}

	/**
	 * 请求中参数转Map
	 * @param request 请求
	 * @return 结果
	 */
	public static Map<String, String> getParameterMap(HttpServletRequest request) {
		Map<String, String> result = new HashMap<>(2);
		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String parameterName = parameterNames.nextElement();
			result.put(parameterName, request.getParameter(parameterName));
		}
		return result;
	}

	/**
	 * 请求参数转List
	 * @param request 请求
	 * @return 结果
	 */
	public static List<String> getParameterList(HttpServletRequest request) {
		Enumeration<String> enumeration = request.getParameterNames();
		List<String> parameterValueList = new LinkedList<>();
		while (enumeration.hasMoreElements()) {
			String parameterName = enumeration.nextElement();
			parameterValueList.add(request.getParameter(parameterName));
		}
		return parameterValueList;
	}

	/**
	 * body请求参数转换Map
	 * @param request 请求
	 * @return 结果
	 */
	public static Map<String, String> getRequestBodyMap(HttpServletRequest request) throws IOException {
		Map<String, String> result = new HashMap<>(2);
		CustomRequestWrapper requestWrapper = new CustomRequestWrapper(request);
		String body = requestWrapper.getBody();
		if (!StringUtil.isNullOrEmpty(body)) {
			Object jsonObj = JSON.parse(body);
			if (jsonObj instanceof JSONObject) {
				JSONObject json = JSON.parseObject(body);
				Set<String> keys = json.keySet();
				for (String key : keys) {
					if (!StringUtil.isNullOrEmpty(json.getString(key))) {
						result.put(key, json.getString(key));
					}
				}
			}
		}
		return result;
	}

	/**
	 * body请求参数转换List,for参数加密
	 * @param request 请求
	 * @return 结果
	 * @throws IOException 异常
	 */
	public static List<String> getRequestBodyList(HttpServletRequest request) throws IOException {
		List<String> parameterValueList = new LinkedList<>();
		CustomRequestWrapper requestWrapper = new CustomRequestWrapper(request);
		String body = requestWrapper.getBody();
		if (!StringUtil.isNullOrEmpty(body)) {
			try {
				Object jsonObj = JSON.parse(body);
				if (jsonObj instanceof JSONArray) {
					JSONArray array = JSON.parseArray(body);
					for (Integer i = 0; i < array.size(); i++) {
						Object o = array.get(i);
						if (o instanceof String && !StringUtil.isNullOrEmpty(array.getString(i))) {
							parameterValueList.add(array.getString(i));
						}
						if (o instanceof Integer && !StringUtil.isNullOrEmpty(array.getString(i))) {
							parameterValueList.add(array.getString(i));
						}
						if (o instanceof Byte && !StringUtil.isNullOrEmpty(array.getString(i))) {
							parameterValueList.add(array.getString(i));
						}
						if (o instanceof JSONObject) {
							JSONObject json = array.getJSONObject(i);
							Set<String> keys = json.keySet();
							for (String key : keys) {
								if (!StringUtil.isNullOrEmpty(json.getString(key))) {
									parameterValueList.add(json.getString(key));
								}
							}
						}
					}
				}
				else if (jsonObj instanceof JSONObject) {
					JSONObject json = JSON.parseObject(body);
					Set<String> keys = json.keySet();
					for (String key : keys) {
						if (!StringUtil.isNullOrEmpty(json.getString(key))) {
							parameterValueList.add(json.getString(key));
						}
					}
				}
				else {
					parameterValueList.add(body);
				}
			}
			catch (JSONException e) {
				parameterValueList.add(body);
			}

		}
		return parameterValueList;
	}

}
