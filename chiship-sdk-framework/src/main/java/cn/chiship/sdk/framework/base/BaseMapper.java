package cn.chiship.sdk.framework.base;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @param <Record>
 * @param <Example>
 * @author lj 2019-06-16
 */
public interface BaseMapper<Record, Example> {

	/**
	 * 插入记录
	 * @param record 实体
	 * @return 结果
	 */
	int insert(Record record);

	/**
	 * 插入记录有效字段
	 * @param record 实体
	 * @return 结果
	 */
	int insertSelective(Record record);

	/**
	 * 根据条件删除记录
	 * @param example 条件
	 * @return 结果
	 */
	int deleteByExample(Example example);

	/**
	 * 根据主键删除记录
	 * @param id 主键
	 * @return 结果
	 */
	int deleteByPrimaryKey(Object id);

	/**
	 * 根据条件更新有效字段
	 * @param record 实体
	 * @param example 条件
	 * @return 结果
	 */
	int updateByExampleSelective(@Param("record") Record record, @Param("example") Example example);

	/**
	 * 根据条件更新记录
	 * @param record 实体
	 * @param example 条件
	 * @return 结果
	 */
	int updateByExample(@Param("record") Record record, @Param("example") Example example);

	/**
	 * 根据主键更新记录有效字段
	 * @param record 条件
	 * @return 结果
	 */
	int updateByPrimaryKeySelective(Record record);

	/**
	 * 根据主键更新记录
	 * @param record 实体
	 * @return 结果
	 */
	int updateByPrimaryKey(Record record);

	/**
	 * 根据条件查询记录
	 * @param example 条件
	 * @return 结果
	 */
	List<Record> selectByExample(Example example);

	/**
	 * 根据主键查询记录
	 * @param id 主键
	 * @return 结果
	 */
	Record selectByPrimaryKey(Object id);

	/**
	 * 根据条件查询记录数量
	 * @param example 条件
	 * @return 结果
	 */
	long countByExample(Example example);

}
