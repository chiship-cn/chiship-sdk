package cn.chiship.sdk.framework.base;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.excel.core.ExcelService;
import cn.chiship.sdk.framework.pojo.dto.export.ExportDto;
import cn.chiship.sdk.framework.pojo.dto.export.ExportTransferDataDto;
import cn.chiship.sdk.framework.pojo.dto.export.ImportDto;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author lj 2018-05-17
 */
public interface BaseService<Record, Example> {

	/**
	 * 插入记录有效字段
	 * @param record 插入数据的实体
	 * @return 结果
	 */
	BaseResult insertSelective(Record record);

	/**
	 * 根据条件删除记录
	 * @param example 删除记录条件
	 * @return 结果
	 */
	BaseResult deleteByExample(Example example);

	/**
	 * 根据主键删除记录
	 * @param id 主键
	 * @return 结果
	 */
	BaseResult deleteByPrimaryKey(Object id);

	/**
	 * 根据条件更新有效字段
	 * @param record 实体
	 * @param example 条件
	 * @return 结果
	 */
	BaseResult updateByExampleSelective(Record record, Example example);

	/**
	 * 根据主键更新记录有效字段
	 * @param record 实体
	 * @return 结果
	 */
	BaseResult updateByPrimaryKeySelective(Record record);

	/**
	 * 根据条件查询记录
	 * @param example 条件
	 * @return 结果
	 */
	List<Record> selectByExample(Example example);

	/**
	 * 根据主键查询记录
	 * @param id 主键
	 * @return 结果
	 */
	Record selectByPrimaryKey(Object id);

	/**
	 * 根据主键查找记录详情
	 * @param id 主键
	 * @return 结果
	 */
	BaseResult selectDetailsByPrimaryKey(Object id);

	/**
	 * 根据条件查询记录数量
	 * @param example 条件
	 * @return 结果
	 */
	long countByExample(Example example);

	/**
	 * 分页
	 * @param pageVo 分页对象
	 * @param example 条件
	 * @return 结果
	 */
	PageVo selectPageByExample(PageVo pageVo, Example example);

	/**
	 * 导出
	 * @param response 响应
	 * @param exportDto 参数集合
	 * @throws Exception 异常
	 */
	void exportData(HttpServletResponse response, ExportDto exportDto) throws Exception;

	/**
	 * 异步导出
	 * @param response 响应
	 * @param taskId 任务ID
	 * @param exportDto 参数集合
	 */
	void asyncExportData(HttpServletResponse response, String taskId, ExportDto exportDto);

	/**
	 * 组装导出数据
	 * @param exportDto 参数集合
	 * @return ExportDataDto
	 */
	ExportTransferDataDto assemblyExportData(ExportDto exportDto);

	/**
	 * 下载模板
	 * @param exportType 类型
	 * @return ExportDataDto
	 * @throws IOException 异常
	 */
	ExportTransferDataDto downloadTemplate(String exportType) throws IOException;

	/**
	 * 异步导入数据
	 * @param taskId
	 * @param inputStream
	 * @param importDto
	 */
	void asyncImportData(String taskId, InputStream inputStream, ImportDto importDto);

	/**
	 * 组装导入数据
	 * @param taskId
	 * @param excelService
	 * @param importDto
	 */
	void assemblyImportData(String taskId, ExcelService excelService, ImportDto importDto);

	/**
	 * 获得导出实时进度
	 * @param response
	 * @param taskId
	 * @throws Exception
	 */
	void getExportProcessStatus(HttpServletResponse response, String taskId) throws Exception;

	/**
	 * 获得数据导入实时进度
	 * @param taskId 任务ID
	 * @return 进度
	 */
	BaseResult getImportProcessStatus(String taskId);

	/**
	 * 根据字段校验数据是否存在
	 * @param id 主键
	 * @param field 字段名
	 * @param value 字段值
	 * @return 结果
	 */
	BaseResult validateExistByField(String id, String field, String value);

	/**
	 * 对对象数据重组
	 * @param record
	 * @return JSONObject
	 */
	JSONObject assembleData(Record record);

	/**
	 * 对列表数据重组
	 * @param records
	 * @return List<JSONObject>
	 */
	List<JSONObject> assembleData(List<Record> records);

}
