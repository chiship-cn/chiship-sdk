package cn.chiship.sdk.framework.config;

import cn.chiship.sdk.framework.properties.ChishipDefaultProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lijian
 */
@Configuration
@EnableConfigurationProperties(value = { ChishipDefaultProperties.class })
public class ChishipPropertiesConfig {

	@Bean
	public ChishipDefaultProperties chishipDefaultProperties() {
		return new ChishipDefaultProperties();
	}

}
