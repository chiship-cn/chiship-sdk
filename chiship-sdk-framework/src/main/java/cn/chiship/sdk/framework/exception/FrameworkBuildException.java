package cn.chiship.sdk.framework.exception;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.exception.CoreBuildException;
import cn.chiship.sdk.core.exception.model.CommonExceptionVo;
import cn.chiship.sdk.framework.exception.custom.FrameworkErrorException;

/**
 * @author lj
 */
public class FrameworkBuildException extends CoreBuildException {

	@Override
	public BaseResult buildException(String requestId, Exception e) {

		BaseResult baseResult = new FrameworkErrorException(e).formatException(requestId);
		CommonExceptionVo commonExceptionVo = (CommonExceptionVo) baseResult.getData();

		if (0 != commonExceptionVo.getErrorCode()) {
			return baseResult;
		}
		else {
			return super.buildException(requestId, e);
		}
	}

}
