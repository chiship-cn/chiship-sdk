package cn.chiship.sdk.framework.exception.mode;

/**
 * restful接口结果常量枚举类
 *
 * @author lijian
 */
public enum FrameworkExceptionEnum {

	/**
	 * 框架类 5002开头
	 */
	HTTP_MESSAGE_NOT_READABLE(5002001, "jackson反序列化失败!"),;

	/**
	 * 自定义状态码
	 */
	private int errorCode;

	/**
	 * 状态信息
	 */
	private String errorMessage;

	FrameworkExceptionEnum(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
