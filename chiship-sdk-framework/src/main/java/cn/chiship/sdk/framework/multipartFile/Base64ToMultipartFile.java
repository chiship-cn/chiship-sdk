package cn.chiship.sdk.framework.multipartFile;

import cn.chiship.sdk.core.util.Base64Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Random;

/**
 * base64转为multipartFile工具类
 *
 * @author lijian
 */

public class Base64ToMultipartFile implements MultipartFile {

	private static final Logger LOGGER = LoggerFactory.getLogger(Base64ToMultipartFile.class);

	private Random random = new Random();

	private String originalFilename;

	private final byte[] imgContent;

	private final String header;

	public Base64ToMultipartFile(String originalFilename, byte[] imgContent, String header) {
		this.imgContent = imgContent;
		this.header = header.split(";")[0];
		String ext = this.header.split("/")[1];
		this.originalFilename = (originalFilename != null ? originalFilename
				: (System.currentTimeMillis() + random.nextInt() * 10000)) + "." + ext;
	}

	@Override
	public String getName() {
		return this.originalFilename;
	}

	@Override
	public String getOriginalFilename() {
		return this.originalFilename;
	}

	@Override
	public String getContentType() {
		return header.split(":")[1];
	}

	@Override
	public boolean isEmpty() {
		return imgContent == null || imgContent.length == 0;
	}

	@Override
	public long getSize() {
		return imgContent.length;
	}

	@Override
	public byte[] getBytes() throws IOException {
		return imgContent;
	}

	@Override
	public InputStream getInputStream() {
		return new ByteArrayInputStream(imgContent);
	}

	@Override
	public void transferTo(File dest) {
		try (FileOutputStream fos = new FileOutputStream(dest)) {
			fos.write(imgContent);
		}
		catch (IOException e) {
			LOGGER.error("发生异常", e);
		}
	}

	/**
	 * base64转multipartFile
	 * @param originalFilename 源文件名
	 * @param base64 base64
	 * @return MultipartFile
	 */
	public static MultipartFile base64Convert(String originalFilename, String base64) {

		String[] baseStr = base64.split(",");
		byte[] b = Base64Util.decode(baseStr[1]);
		for (int i = 0; i < b.length; ++i) {
			if (b[i] < 0) {
				b[i] += 256;
			}
		}
		return new Base64ToMultipartFile(originalFilename, b, baseStr[0]);
	}

}
