package cn.chiship.sdk.framework.mybatis.plugin;

import cn.chiship.sdk.core.util.DateUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.DefaultCommentGenerator;

/**
 * 生成model中，字段增加注释
 *
 * @author lijian
 */
public class CommentGenerator extends DefaultCommentGenerator {

	@Override
	public void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

		// 获取表注释
		String remarks = introspectedTable.getRemarks();

		topLevelClass.addJavaDocLine("/**");
		topLevelClass.addJavaDocLine(" * " + remarks + "实体");
		topLevelClass.addJavaDocLine(" *");
		topLevelClass.addJavaDocLine(genAuthorComment());
		topLevelClass.addJavaDocLine(genDateComment());
		topLevelClass.addJavaDocLine(" */");
	}

	@Override
	public void addFieldComment(Field field, IntrospectedTable introspectedTable,
			IntrospectedColumn introspectedColumn) {

		super.addFieldComment(field, introspectedTable, introspectedColumn);
		if (introspectedColumn.getRemarks() != null && !"".equals(introspectedColumn.getRemarks())) {
			field.addJavaDocLine("/**");
			field.addJavaDocLine(" * " + introspectedColumn.getRemarks());
			field.addJavaDocLine(" */");
		}

	}

	/**
	 * 生成作者注释
	 */
	private String genAuthorComment() {
		return " * @author " + System.getProperties().getProperty("user.name");
	}

	/**
	 * 生成日期注释
	 */
	private String genDateComment() {
		return " * @date " + DateUtils.getDate();
	}

}
