package cn.chiship.sdk.framework.mybatis.plugin;

import cn.chiship.sdk.core.util.DateUtils;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.*;

import java.util.List;

/**
 * @author lj
 */
public class MybatisPlugin extends PluginAdapter {

	@Override
	public boolean validate(List<String> list) {
		return true;
	}

	/**
	 * 生成Mapper接口
	 */
	@Override
	public boolean clientGenerated(Interface javaElement, TopLevelClass topLevelClass,
			IntrospectedTable introspectedTable) {
		String remarks = introspectedTable.getRemarks();
		javaElement.addJavaDocLine("/**");
		javaElement.addJavaDocLine(" * " + remarks + "Mapper");
		javaElement.addJavaDocLine(" *");
		javaElement.addJavaDocLine(genAuthorComment());
		javaElement.addJavaDocLine(genDateComment());
		javaElement.addJavaDocLine(" */");
		return true;
	}

	@Override
	public boolean modelExampleClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		String remarks = introspectedTable.getRemarks();
		topLevelClass.addJavaDocLine("/**");
		topLevelClass.addJavaDocLine(" * " + remarks + "Example");
		topLevelClass.addJavaDocLine(" *");
		topLevelClass.addJavaDocLine(genAuthorComment());
		topLevelClass.addJavaDocLine(genDateComment());
		topLevelClass.addJavaDocLine(" */");
		return true;
	}

	/**
	 * 生成作者注释
	 */
	private String genAuthorComment() {
		return " * @author " + System.getProperties().getProperty("user.name");
	}

	/**
	 * 生成日期注释
	 */
	private String genDateComment() {
		return " * @date " + DateUtils.getDate();
	}

}
