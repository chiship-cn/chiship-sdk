package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "小程序用户手机号授权登录或注册")
public class AppletLoginOrRegisterDto {

	@ApiModelProperty(value = "手机号", required = true)
	@NotEmpty(message = "手机号" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11, max = 11)
	private String mobile;

	@ApiModelProperty(value = "小程序唯一授权ID")
	@NotNull(message = "小程序唯一授权ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String openId;

	@ApiModelProperty(value = "昵称")
	@Length(max = 10)
	private String nickName;

	@ApiModelProperty(value = "头像")
	private String avatar;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
