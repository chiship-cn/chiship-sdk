package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * Base64上传表单
 *
 * @author lijian
 */
@ApiModel(value = "Base64上传表单")
public class Base64UploadDto {

	@ApiModelProperty(value = "文件名称", required = true)
	@NotNull(message = "文件名称,如：example" + BaseTipConstants.NOT_EMPTY)
	private String fileName;

	@ApiModelProperty(value = "所在文件夹", required = true)
	@NotNull(message = "所在文件夹" + BaseTipConstants.NOT_EMPTY)
	private String catalogId;

	@ApiModelProperty(value = "Base64资源", required = true)
	@NotNull(message = "Base64资源" + BaseTipConstants.NOT_EMPTY)
	private String base64;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

}
