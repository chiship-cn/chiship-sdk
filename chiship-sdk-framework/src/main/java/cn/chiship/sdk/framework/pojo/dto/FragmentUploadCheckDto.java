package cn.chiship.sdk.framework.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文件分片上传检测表单
 *
 * @author lj
 */
@ApiModel(value = "文件分片上传检测表单")
public class FragmentUploadCheckDto {

	@ApiModelProperty(value = "文件夹ID", required = true)
	private String catalogId;

	@ApiModelProperty(value = "文件MD5信息码", required = true)
	private String fileMd5;

	@ApiModelProperty(value = "源文件名称", required = true)
	private String originalFileName;

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getFileMd5() {
		return fileMd5;
	}

	public void setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

}
