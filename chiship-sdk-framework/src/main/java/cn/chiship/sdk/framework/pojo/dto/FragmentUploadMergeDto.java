package cn.chiship.sdk.framework.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文件分片上传检测表单
 *
 * @author lj
 */
@ApiModel(value = "文件分片合并表单")
public class FragmentUploadMergeDto extends FragmentUploadCheckDto {

	@ApiModelProperty(value = "文件大小", required = true)
	private Long fileSize;

	@ApiModelProperty(value = "文件类型", required = true)
	private String contentType;

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
