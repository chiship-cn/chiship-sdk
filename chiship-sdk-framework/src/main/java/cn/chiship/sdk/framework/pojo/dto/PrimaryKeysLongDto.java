package cn.chiship.sdk.framework.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author lijian
 */
@ApiModel(value = "数字类型主键")
public class PrimaryKeysLongDto {

	@ApiModelProperty(value = "主键集合")
	private List<Long> ids;

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

}
