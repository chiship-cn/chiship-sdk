package cn.chiship.sdk.framework.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author lijian
 */
@ApiModel(value = "字符类型主键")
public class PrimaryKeysStringDto {

	@ApiModelProperty(value = "主键集合")
	private List<String> ids;

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

}
