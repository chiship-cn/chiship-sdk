package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel(value = "字符表单")
public class StringDto {

    @ApiModelProperty(value = "参数", required = true)
    @NotNull(message = "参数" + BaseTipConstants.NOT_EMPTY)
    private String param;

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
