package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "基本用户登录表单")
public class UserBaseLoginDto {

	@ApiModelProperty(value = "用户名", required = true)
	@NotNull(message = "用户名" + BaseTipConstants.NOT_EMPTY)
	private String username;

	@ApiModelProperty(value = "密码", required = true)
	@NotNull(message = "密码" + BaseTipConstants.NOT_EMPTY)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
