package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "用户+图片验证码登录表单")
public class UserCaptchaLoginDto extends UserBaseLoginDto {

	@ApiModelProperty(value = "图形验证码")
	@NotNull(message = "图形验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "图形验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String verificationCode;

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

}
