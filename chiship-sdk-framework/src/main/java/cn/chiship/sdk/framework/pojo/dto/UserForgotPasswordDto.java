package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author lijian 忘记密码
 */
@ApiModel(value = "用户忘记密码表单")
public class UserForgotPasswordDto {

	@ApiModelProperty(value = "手机号", required = true)
	@NotNull(message = "手机号" + BaseTipConstants.NOT_EMPTY)
	@Pattern(regexp = RegexConstants.MOBILE, message = BaseTipConstants.MOBILE)
	private String mobile;

	@ApiModelProperty(value = "密码", required = true)
	@NotNull(message = "密码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, message = "密码" + BaseTipConstants.LENGTH_MIN)
	private String password;

	@ApiModelProperty(value = "密码确认", required = true)
	@NotNull(message = "密码确认" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, message = "密码确认" + BaseTipConstants.LENGTH_MIN)
	private String passwordAgain;

	@ApiModelProperty(value = "验证码", required = true)
	@NotNull(message = "验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String verificationCode;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordAgain() {
		return passwordAgain;
	}

	public void setPasswordAgain(String passwordAgain) {
		this.passwordAgain = passwordAgain;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

}
