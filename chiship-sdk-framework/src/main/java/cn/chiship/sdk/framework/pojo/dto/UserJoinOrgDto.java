package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "用户加入组织表单")
public class UserJoinOrgDto {

	@ApiModelProperty(value = "真实姓名", required = true)
	@NotNull(message = "真实姓名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, max = 10, message = "真实姓名" + BaseTipConstants.LENGTH_MIN_MAX)
	private String realName;

	@ApiModelProperty(value = "邀请码", required = true)
	@NotNull(message = "邀请码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, message = "邀请吗" + BaseTipConstants.LENGTH_MIN)
	private String invitationCode;

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

}
