package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "手机号验证码登录表单")
public class UserMobileLoginDto {

	@ApiModelProperty(value = "手机号", required = true)
	@NotNull(message = "手机号" + BaseTipConstants.NOT_EMPTY)
	private String mobile;

	@ApiModelProperty(value = "验证码", required = true)
	@NotNull(message = "验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String mobileVerificationCode;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobileVerificationCode() {
		return mobileVerificationCode;
	}

	public void setMobileVerificationCode(String mobileVerificationCode) {
		this.mobileVerificationCode = mobileVerificationCode;
	}

}
