package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author lijian 修改邮箱
 */
@ApiModel(value = "用户邮箱修改表单")
public class UserModifyEmailDto {

	@ApiModelProperty(value = "现手机号验证码")
	@NotNull(message = "现手机号验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "现手机号验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String verificationCode;

	@ApiModelProperty(value = "新邮箱", required = true)
	@NotNull(message = "新邮箱" + BaseTipConstants.NOT_EMPTY)
	@Pattern(regexp = RegexConstants.EMAIL, message = BaseTipConstants.EMAIL)
	private String newEmail;

	@ApiModelProperty(value = "新邮箱验证码", required = true)
	@NotNull(message = "新邮箱验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "新邮箱验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String emailVerificationCode;

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}

	public String getEmailVerificationCode() {
		return emailVerificationCode;
	}

	public void setEmailVerificationCode(String emailVerificationCode) {
		this.emailVerificationCode = emailVerificationCode;
	}

}
