package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author lijian 修改手机号
 */
@ApiModel(value = "用户手机号修改表单")
public class UserModifyMobileDto {

	@ApiModelProperty(value = "原手机号验证码", required = true)
	@NotNull(message = "原手机号验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "原手机号验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String verificationCode;

	@ApiModelProperty(value = "新手机号", required = true)
	@NotNull(message = "新手机号" + BaseTipConstants.NOT_EMPTY)
	@Pattern(regexp = RegexConstants.MOBILE, message = BaseTipConstants.MOBILE)
	private String newMobile;

	@ApiModelProperty(value = "新手机号验证码", required = true)
	@NotNull(message = "新手机号验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "新手机号验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String newVerificationCode;

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public String getNewMobile() {
		return newMobile;
	}

	public void setNewMobile(String newMobile) {
		this.newMobile = newMobile;
	}

	public String getNewVerificationCode() {
		return newVerificationCode;
	}

	public void setNewVerificationCode(String newVerificationCode) {
		this.newVerificationCode = newVerificationCode;
	}

}
