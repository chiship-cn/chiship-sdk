package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author lijian 修改密码
 */
@ApiModel(value = "用户密码修改表单")
public class UserModifyPasswordDto {

	@ApiModelProperty(value = "手机号", required = true)
	@NotEmpty(message = "手机号" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11, max = 11)
	@Pattern(regexp = RegexConstants.MOBILE, message = BaseTipConstants.MOBILE)
	private String mobile;

	@ApiModelProperty(value = "旧密码", required = true)
	@NotNull(message = "旧密码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, message = "旧密码" + BaseTipConstants.LENGTH_MIN)
	private String oldPassword;

	@ApiModelProperty(value = "新密码", required = true)
	@NotNull(message = "新密码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, message = "新密码" + BaseTipConstants.LENGTH_MIN)
	private String newPassword;

	@ApiModelProperty(value = "新密码确认", required = true)
	@NotNull(message = "新密码确认" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, message = "新密码确认" + BaseTipConstants.LENGTH_MIN)
	private String newPasswordAgain;

	@ApiModelProperty(value = "验证码", required = true)
	@NotNull(message = "验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String verificationCode;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPasswordAgain() {
		return newPasswordAgain;
	}

	public void setNewPasswordAgain(String newPasswordAgain) {
		this.newPasswordAgain = newPasswordAgain;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

}
