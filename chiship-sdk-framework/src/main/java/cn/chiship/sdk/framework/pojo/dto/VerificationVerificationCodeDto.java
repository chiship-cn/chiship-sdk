package cn.chiship.sdk.framework.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "验证码验证表单")
public class VerificationVerificationCodeDto {

	@ApiModelProperty(value = "设备号", required = true)
	@NotNull(message = "设备号" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, message = "设备号" + BaseTipConstants.LENGTH_MIN)
	private String device;

	@ApiModelProperty(value = "设备类型  0：手机  1：邮箱", required = true)
	@NotNull(message = "设备类型" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Min(1)
	private Byte deviceType;

	@ApiModelProperty(value = "验证码", required = true)
	@NotNull(message = "验证码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 4, max = 6, message = "验证码" + BaseTipConstants.LENGTH_MIN_MAX)
	private String code;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public Byte getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(Byte deviceType) {
		this.deviceType = deviceType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
