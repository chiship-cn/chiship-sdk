package cn.chiship.sdk.framework.pojo.dto.export;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.util.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * 导出DTO
 *
 * @author lj
 */
@ApiModel(value = "数据导出表单")
public class ExportDto {

	@ApiModelProperty(value = "导出文件类型", required = true)
	@NotNull(message = "导出文件类型" + BaseTipConstants.NOT_EMPTY)
	private String exportType;

	@ApiModelProperty(value = "参数条件", required = true)
	@NotNull(message = "参数条件" + BaseTipConstants.NOT_EMPTY)
	private Map<String, Object> paramMap;

	@ApiModelProperty(value = "是否异步")
	private Boolean async;

	@ApiModelProperty(value = "是否模板")
	private Boolean template;

	public String getExportType() {
		return exportType;
	}

	public void setExportType(String exportType) {
		this.exportType = exportType;
	}

	public Boolean getAsync() {
		if (StringUtil.isNull(async)) {
			async = false;
		}
		return async;
	}

	public void setAsync(Boolean async) {
		this.async = async;
	}

	public Boolean getTemplate() {
		if (StringUtil.isNull(template)) {
			template = false;
		}
		return template;
	}

	public void setTemplate(Boolean template) {
		this.template = template;
	}

	public Map<String, Object> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, Object> paramMap) {
		this.paramMap = paramMap;
	}

}
