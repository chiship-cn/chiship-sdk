package cn.chiship.sdk.framework.pojo.dto.export;

import java.util.List;

/**
 * 导出传输数据DTO
 *
 * @author lj
 */
public class ExportTransferDataDto {

	/**
	 * 导出文件类型
	 */
	private String exportType;

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 表格Sheet名称
	 */
	private String sheetName;

	/**
	 * 表格Sheet标题
	 */
	private String sheetTitle;

	/**
	 * 标题
	 */
	private List<String> labels;

	/**
	 * 数值集合
	 */
	private List<List<String>> values;

	/**
	 * 总数
	 */
	private Integer total;

	/**
	 * 字节数组
	 */
	private byte[] bytes;

	public ExportTransferDataDto(String fileName, String sheetName, String sheetTitle, List<String> labels,
			List<List<String>> values, Integer total) {
		this.fileName = fileName;
		this.sheetName = sheetName;
		this.sheetTitle = sheetTitle;
		this.labels = labels;
		this.values = values;
		this.total = total;
	}

	public ExportTransferDataDto(String exportType, String fileName, String sheetName, String sheetTitle,
			List<String> labels, List<List<String>> values, Integer total) {
		this.exportType = exportType;
		this.fileName = fileName;
		this.sheetName = sheetName;
		this.sheetTitle = sheetTitle;
		this.labels = labels;
		this.values = values;
		this.total = total;
	}

	public ExportTransferDataDto(String fileName, byte[] bytes, String exportType) {
		this.exportType = exportType;
		this.fileName = fileName;
		this.bytes = bytes;
	}

	public String getExportType() {
		return exportType;
	}

	public void setExportType(String exportType) {
		this.exportType = exportType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public String getSheetTitle() {
		return sheetTitle;
	}

	public void setSheetTitle(String sheetTitle) {
		this.sheetTitle = sheetTitle;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public List<List<String>> getValues() {
		return values;
	}

	public void setValues(List<List<String>> values) {
		this.values = values;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

}
