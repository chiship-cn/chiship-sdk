package cn.chiship.sdk.framework.pojo.dto.export;

import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.util.StringUtil;
import io.swagger.annotations.ApiModel;

import java.util.Map;

/**
 * 数据导入DTO
 *
 * @author lj
 */
@ApiModel(value = "数据导入表单")
public class ImportDto {

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 是否保存源文件
	 */
	private Boolean isSave;

	/**
	 * 导入文件类型
	 */
	private String contentType;

	/**
	 * 执行用户
	 */
	private CacheUserVO cacheUserVO;

	/**
	 * 参数条件
	 */
	private Map<String, Object> paramMap;

	public ImportDto(String fileName, Boolean isSave, String contentType, CacheUserVO cacheUserVO,
			Map<String, Object> paramMap) {
		this.fileName = fileName;
		this.isSave = isSave;
		this.contentType = contentType;
		this.cacheUserVO = cacheUserVO;
		this.paramMap = paramMap;
	}

	public Map<String, Object> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, Object> paramMap) {
		this.paramMap = paramMap;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean getSave() {
		if (StringUtil.isNull(isSave)) {
			isSave = false;
		}
		return isSave;
	}

	public void setSave(Boolean save) {
		isSave = save;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public CacheUserVO getCacheUserVO() {
		return cacheUserVO;
	}

	public void setCacheUserVO(CacheUserVO cacheUserVO) {
		this.cacheUserVO = cacheUserVO;
	}

}
