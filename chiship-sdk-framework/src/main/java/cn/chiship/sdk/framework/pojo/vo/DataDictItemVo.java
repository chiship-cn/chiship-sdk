package cn.chiship.sdk.framework.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author lj
 */
@ApiModel(value = "数据字典组数据项视图")
public class DataDictItemVo {

	@ApiModelProperty(value = "字典组数据项主键")
	private String id;

	@ApiModelProperty(value = "字典组主键")
	private String dataDictId;

	@ApiModelProperty(value = "字典组数据项编码")
	private String dataDictItemCode;

	@ApiModelProperty(value = "字典组数据项名称")
	private String dataDictItemName;

	@ApiModelProperty(value = "颜色")
	private String colorValue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDataDictId() {
		return dataDictId;
	}

	public void setDataDictId(String dataDictId) {
		this.dataDictId = dataDictId;
	}

	public String getDataDictItemCode() {
		return dataDictItemCode;
	}

	public void setDataDictItemCode(String dataDictItemCode) {
		this.dataDictItemCode = dataDictItemCode;
	}

	public String getDataDictItemName() {
		return dataDictItemName;
	}

	public void setDataDictItemName(String dataDictItemName) {
		this.dataDictItemName = dataDictItemName;
	}

	public String getColorValue() {
		return colorValue;
	}

	public void setColorValue(String colorValue) {
		this.colorValue = colorValue;
	}

}
