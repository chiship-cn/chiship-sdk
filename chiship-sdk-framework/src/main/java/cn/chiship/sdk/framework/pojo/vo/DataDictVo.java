package cn.chiship.sdk.framework.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author lj
 */
@ApiModel(value = "数据字典组视图")
public class DataDictVo {

	@ApiModelProperty(value = "字典组主键")
	private String id;

	@ApiModelProperty(value = "字典组编码")
	private String dataDictCode;

	@ApiModelProperty(value = "字典组名称")
	private String dataDictName;

	@ApiModelProperty(value = "数据字典组数据项")
	private List<DataDictItemVo> dataDictItemVos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDataDictCode() {
		return dataDictCode;
	}

	public void setDataDictCode(String dataDictCode) {
		this.dataDictCode = dataDictCode;
	}

	public String getDataDictName() {
		return dataDictName;
	}

	public void setDataDictName(String dataDictName) {
		this.dataDictName = dataDictName;
	}

	public List<DataDictItemVo> getDataDictItemVos() {
		return dataDictItemVos;
	}

	public void setDataDictItemVos(List<DataDictItemVo> dataDictItemVos) {
		this.dataDictItemVos = dataDictItemVos;
	}

}
