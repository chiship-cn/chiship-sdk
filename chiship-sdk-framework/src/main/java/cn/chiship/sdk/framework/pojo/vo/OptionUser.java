package cn.chiship.sdk.framework.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author lj
 */
@ApiModel(value = "操作用户视图")
public class OptionUser implements Serializable {

	@ApiModelProperty(value = "主键")
	private String id;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "组织编号")
	private Object orgId;

	@ApiModelProperty(value = "公司名称")
	private String orgName;

	@ApiModelProperty(value = "用户类型")
	private String userType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Object getOrgId() {
		return orgId;
	}

	public void setOrgId(Object orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}