package cn.chiship.sdk.framework.pojo.vo;

import cn.chiship.sdk.core.util.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author lijian
 */
@ApiModel(value = "分页视图")
public class PageVo {

	@ApiModelProperty(value = "当前页")
	private Long current;

	@ApiModelProperty(value = "每页多少条")
	private Long size;

	@ApiModelProperty(value = "排序")
	private String sort;

	@ApiModelProperty(value = "记录")
	private List records;

	@ApiModelProperty(value = "总记录数")
	private Long total;

	@ApiModelProperty(value = "总页数")
	private Long pages;

	public PageVo() {
	}

	public PageVo(Long current, Long size) {
		this.current = current;
		this.size = size;
	}

	public PageVo(Long current, Long size, String sort) {
		this.current = current;
		this.size = size;
		this.sort = sort;
	}

	/**
	 * 页码
	 */
	public Long getFirstResult() {
		return (this.getCurrent() - 1) * this.getSize();
	}

	public List getRecords() {
		return records;
	}

	public void setRecords(List records) {
		this.records = records;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getSize() {
		if (StringUtil.isNull(size)) {
			size = 20L;
		}
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Long getCurrent() {
		if (StringUtil.isNull(current)) {
			current = 1L;
		}
		return current;
	}

	public void setCurrent(Long current) {
		this.current = current;
	}

	public Long getPages() {
		return pages;
	}

	public void setPages(Long pages) {
		this.pages = pages;
	}

	public String getSort() {
		return StringUtil.getOrderByValue(sort);
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

}
