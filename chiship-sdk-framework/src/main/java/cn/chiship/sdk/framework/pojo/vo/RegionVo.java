package cn.chiship.sdk.framework.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author LiJian
 */
@ApiModel(value = "地区视图")
public class RegionVo {

	@ApiModelProperty(value = "主键")
	private Long id;

	@ApiModelProperty(value = "名字")
	private String name;

	@ApiModelProperty(value = "所属父级")
	private Long pid;

	@ApiModelProperty(value = "首字母")
	private String firstLetter;

	@ApiModelProperty(value = "全拼")
	private String fullPinyin;

	@ApiModelProperty(value = "级别")
	private String level;

	@ApiModelProperty(value = "区划编码")
	private String adCode;

	@ApiModelProperty(value = "子元素")
	private List<RegionVo> regionVos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	public String getFullPinyin() {
		return fullPinyin;
	}

	public void setFullPinyin(String fullPinyin) {
		this.fullPinyin = fullPinyin;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAdCode() {
		return adCode;
	}

	public void setAdCode(String adCode) {
		this.adCode = adCode;
	}

	public List<RegionVo> getRegionVos() {
		return regionVos;
	}

	public void setRegionVos(List<RegionVo> regionVos) {
		this.regionVos = regionVos;
	}

}
