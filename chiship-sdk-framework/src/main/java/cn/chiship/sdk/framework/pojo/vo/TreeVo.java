package cn.chiship.sdk.framework.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author lijian
 */
@ApiModel(value = "树形结构视图")
public class TreeVo {

    @ApiModelProperty(value = "节点ID")
    private String id;

    @ApiModelProperty(value = "父节点ID")
    private String pid;

    @ApiModelProperty(value = "节点名称")
    private String label;

    @ApiModelProperty(value = "是否叶子节点")
    private Boolean leaf;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "子元素")
    private List<TreeVo> children;

    @ApiModelProperty(value = "扩展字段")
    private String ext;

    @ApiModelProperty(value = "扩展字段1")
    private String ext1;

    public TreeVo(String id, String pid, String label) {
        this.id = id;
        this.pid = pid;
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public List<TreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeVo> children) {
        this.children = children;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

}
