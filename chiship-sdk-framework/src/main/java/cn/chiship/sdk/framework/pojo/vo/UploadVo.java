package cn.chiship.sdk.framework.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author lj
 */
@ApiModel(value = "文件上传结果视图")
public class UploadVo {

    @ApiModelProperty(value = "文件存储ID")
    private String id;

    @ApiModelProperty(value = "文件原名称")
    private String originalName;

    @ApiModelProperty(value = "文件UUID")
    private String uuid;

    @ApiModelProperty(value = "文件大小")
    private Long size;

    @ApiModelProperty(value = "文件类型")
    private Byte type;

    @ApiModelProperty(value = "文件扩展名")
    private String ext;

    @ApiModelProperty(value = "文件存储路径")
    private String path;

    @ApiModelProperty(value = "文件上传者")
    private String uploader;

    @ApiModelProperty(value = "文件上传时间")
    private Long uploadTime;

    public UploadVo(String id, String originalName, String uuid, Long size, Byte type, String ext, String path,
                    String uploader, Long uploadTime) {
        this.id = id;
        this.originalName = originalName;
        this.uuid = uuid;
        this.size = size;
        this.type = type;
        this.ext = ext;
        this.path = path;
        this.uploader = uploader;
        this.uploadTime = uploadTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public Long getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Long uploadTime) {
        this.uploadTime = uploadTime;
    }

}
