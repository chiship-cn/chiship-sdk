package cn.chiship.sdk.framework.properties;

import cn.chiship.sdk.core.util.StringUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipDefaultProperties.PREFIX)
public class ChishipDefaultProperties {

	public static final String PREFIX = "chiship";

	/**
	 * 项目ID
	 */
	private String id = "12345678";

	/**
	 * 项目名称
	 */
	private String name = "chiship";

	/**
	 * 项目标题
	 */
	private String title = "Chiship";

	/**
	 * 版本
	 */
	private String version = "1.1";

	/**
	 * 版权年份
	 */
	private String copyrightYear = "2023";

	/**
	 * 生成主键方式
	 */
	private String keyGenerator = "SNOWFLAKE";

	/**
	 * 演示开关
	 */
	private Boolean demoEnabled = false;

	/**
	 * 日志拦截开关
	 */
	private boolean enableLogInterception = true;

	/**
	 * 唯一Key
	 */
	private String appKey = "chiship";

	/**
	 * 唯一锁
	 */
	private String appSecret = "chiship";

	/**
	 * 唯一ID
	 */
	private String appId = "chiship";

	/**
	 * 签名串
	 */
	private String signKey = "chiship";

	/**
	 * 私钥
	 */
	private String privateKey = "chiship";

	/**
	 * 公钥
	 */
	private String publicKey = "chiship";

	/**
	 * 接口白名单
	 */
	private String interfaceWhite = "login";

	/**
	 * 导入文件默认存储文件位置
	 */
	private String importDataCatalog;

	/**
	 * 服务主机
	 */
	private String serverDomain;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCopyrightYear() {
		return copyrightYear;
	}

	public void setCopyrightYear(String copyrightYear) {
		this.copyrightYear = copyrightYear;
	}

	public String getKeyGenerator() {
		if (StringUtil.isNullOrEmpty(keyGenerator)) {
			keyGenerator = "SNOWFLAKE";
		}
		return keyGenerator;
	}

	public void setKeyGenerator(String keyGenerator) {
		this.keyGenerator = keyGenerator;
	}

	public Boolean getDemoEnabled() {
		return demoEnabled;
	}

	public void setDemoEnabled(Boolean demoEnabled) {
		this.demoEnabled = demoEnabled;
	}

	public boolean isEnableLogInterception() {
		return enableLogInterception;
	}

	public void setEnableLogInterception(boolean enableLogInterception) {
		this.enableLogInterception = enableLogInterception;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getSignKey() {
		return signKey;
	}

	public void setSignKey(String signKey) {
		this.signKey = signKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getInterfaceWhite() {
		return interfaceWhite;
	}

	public void setInterfaceWhite(String interfaceWhite) {
		this.interfaceWhite = interfaceWhite;
	}

	public String getImportDataCatalog() {
		return importDataCatalog;
	}

	public void setImportDataCatalog(String importDataCatalog) {
		this.importDataCatalog = importDataCatalog;
	}

	public String getServerDomain() {
		return serverDomain;
	}

	public void setServerDomain(String serverDomain) {
		this.serverDomain = serverDomain;
	}

}
