package cn.chiship.sdk.framework.util;

import cn.chiship.sdk.core.util.CamelCaseUtils;

/**
 * @author lj
 */
public class FrameworkUtil {

	private static final String ADD = "+";

	private FrameworkUtil() {

	}

	/**
	 * 格式化排序
	 * @param sort 排序
	 * @return 结果
	 */
	public static String formatSort(String sort) {
		if (sort != null) {
			if (ADD.equals(sort.substring(0, 1))) {
				return CamelCaseUtils.toUnderlineName(sort.substring(1)) + " ASC";
			}
			else {
				return CamelCaseUtils.toUnderlineName(sort.substring(1)) + " DESC";
			}
		}
		return " id desc";
	}

}
