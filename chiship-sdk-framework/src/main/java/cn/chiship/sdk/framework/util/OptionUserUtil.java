package cn.chiship.sdk.framework.util;

import cn.chiship.sdk.cache.vo.CacheOrganizationVo;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.vo.OptionUser;
import com.alibaba.fastjson.JSON;

/**
 * 操作者信息工具类
 *
 * @author lijian
 */
public class OptionUserUtil {

	private OptionUserUtil() {
	}

	/**
	 * 当前登录用户转换为JSON字符串
	 * @param userVO 登陆用户
	 * @return 结果
	 */
	public static String getCurrentOptionUser(CacheUserVO userVO) {
		OptionUser optionUser = new OptionUser();
		optionUser.setId(userVO.getId());
		optionUser.setUserName(userVO.getUsername());
		optionUser.setRealName(userVO.getRealName());
		optionUser.setUserType(userVO.getUserTypeEnum().getType());
		CacheOrganizationVo organizationVo = userVO.getCacheOrganizationVo();
		if (!StringUtil.isNull(organizationVo)) {
			optionUser.setOrgName(organizationVo.getName());
			optionUser.setOrgId(organizationVo.getId());
		}
		return JSON.toJSONString(optionUser);
	}

	public static OptionUser getCurrentOptionUser(String optionUserStr) {
		return JSON.parseObject(optionUserStr, OptionUser.class);
	}

}
