package cn.chiship.sdk.framework.util;

import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.ContentTypeEnum;
import cn.chiship.sdk.core.enums.FileExtEnum;
import cn.chiship.sdk.core.enums.HeaderEnum;
import cn.chiship.sdk.core.text.Convert;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 客户端工具类
 *
 * @author lijian
 */
public class ServletUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServletUtil.class);

	/**
	 * 定义移动端请求的所有可能类型
	 */
	private final static String[] AGENT = { "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser" };

	/**
	 * 获取String参数消息头
	 */
	public static String getHeader(HeaderEnum headerEnum) {
		return getHeader(headerEnum.getName());
	}

	/**
	 * 获取String参数消息头
	 */
	public static String getHeader(String name) {
		return getRequest().getHeader(name);
	}

	/**
	 * 获取String参数
	 */
	public static String getParameter(String name) {
		return getRequest().getParameter(name);
	}

	/**
	 * 获取String参数
	 */
	public static String getParameter(String name, String defaultValue) {
		return Convert.toStr(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取Integer参数
	 */
	public static Integer getParameterToInt(String name) {
		return Convert.toInt(getRequest().getParameter(name));
	}

	/**
	 * 获取所有request请求参数
	 * @return 参数
	 */
	public static Map<String, Object> getParameterValues() {
		Map<String, Object> params = new HashMap<>(7);
		Enumeration<String> parameterNames = getRequest().getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String name = parameterNames.nextElement();
			Object value = getRequest().getParameter(name);
			params.put(name, value);

		}
		return params;
	}

	/**
	 * 获取Integer参数
	 */
	public static Integer getParameterToInt(String name, Integer defaultValue) {
		return Convert.toInt(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取Long参数
	 */
	public static Long getParameterToLong(String name, Long defaultValue) {
		return Convert.toLong(getRequest().getParameter(name), defaultValue);
	}

	/**
	 * 获取request
	 */
	public static HttpServletRequest getRequest() {
		return getRequestAttributes().getRequest();
	}

	/**
	 * 获取response
	 */
	public static HttpServletResponse getResponse() {
		return getRequestAttributes().getResponse();
	}

	/**
	 * 获取session
	 */
	public static HttpSession getSession() {
		return getRequest().getSession();
	}

	public static ServletRequestAttributes getRequestAttributes() {
		RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
		return (ServletRequestAttributes) attributes;
	}

	/**
	 * 将字符串渲染到客户端
	 * @param response 渲染对象
	 * @param string 待渲染的字符串
	 * @return null
	 */
	public static String renderString(HttpServletResponse response, String string) {
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(string);
		}
		catch (IOException e) {
			LOGGER.error("发生异常", e);
		}
		return null;
	}

	/**
	 * 是否是Ajax异步请求
	 * @param request
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		String accept = request.getHeader("accept");
		if (accept != null && accept.indexOf(ContentTypeEnum.APPLICATION_JSON.getName()) != -1) {
			return true;
		}

		String xRequestedWith = request.getHeader("X-Requested-With");
		String a = "XMLHttpRequest";
		if (xRequestedWith != null && xRequestedWith.indexOf(a) != -1) {
			return true;
		}

		String uri = request.getRequestURI();
		if (StringUtil.inStringIgnoreCase(uri, FileExtEnum.FILE_EXT_JSON.getName(),
				FileExtEnum.FILE_EXT_XML.getName())) {
			return true;
		}

		String ajax = request.getParameter("__ajax");
		return (StringUtil.inStringIgnoreCase(ajax, "json", "xml"));

	}

	/**
	 * 判断User-Agent 是不是来自于手机
	 */
	public static boolean checkAgentIsMobile(String ua) {
		String winNt = "Windows NT";
		boolean flag = false;
		boolean check = !ua.contains(winNt) || (ua.contains(winNt) && ua.contains("compatible; MSIE 9.0;"));
		if (check) {
			// 排除 苹果桌面系统
			String a = "Macintosh";

			if (!ua.contains(winNt) && !ua.contains(a)) {
				for (String item : AGENT) {
					if (ua.contains(item)) {
						flag = true;
						break;
					}
				}
			}
		}
		return flag;
	}

	/**
	 * 获得分页实例
	 * @return PageVo
	 */
	public static PageVo getPageVo() {
		PageVo pageVo = new PageVo(getParameterToLong(BaseConstants.PAGE_NUM, 1L),
				getParameterToLong(BaseConstants.PAGE_SIZE, 20L),
				getParameter(BaseConstants.PAGE_SORT, "-gmt_modified"));
		return pageVo;
	}

}
