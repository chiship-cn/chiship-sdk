package cn.chiship.sdk.framework.util;

import cn.chiship.sdk.core.util.ip.IpUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 资源文件读取工具
 *
 * @author lijian
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringContextUtil.class);

	private static ApplicationContext context = null;

	private SpringContextUtil() {
		super();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;
	}

	/**
	 * 根据名称获取bean
	 * @param beanName 实体名称
	 * @return 结果
	 */
	public static Object getBean(String beanName) {
		return context.getBean(beanName);
	}

	/**
	 * 根据bean名称获取指定类型bean
	 * @param beanName bean名称
	 * @param clazz 返回的bean类型,若类型不匹配,将抛出异常
	 */
	public static <T> T getBean(String beanName, Class<T> clazz) {
		return context.getBean(beanName, clazz);
	}

	/**
	 * 根据类型获取bean
	 * @param clazz 类
	 * @return 结果
	 */
	public static <T> T getBean(Class<T> clazz) {
		T t = null;
		Map<String, T> map = context.getBeansOfType(clazz);
		for (Map.Entry<String, T> entry : map.entrySet()) {
			t = entry.getValue();
		}
		return t;
	}

	/**
	 * 是否包含bean
	 * @param beanName 实体名称
	 * @return 结果
	 */
	public static boolean containsBean(String beanName) {
		return context.containsBean(beanName);
	}

	/**
	 * 是否是单例
	 * @param beanName 实体名称
	 * @return 结果
	 */
	public static boolean isSingleton(String beanName) {
		return context.isSingleton(beanName);
	}

	/**
	 * bean的类型
	 * @param beanName 实体名称
	 * @return 结果
	 */
	public static Class getType(String beanName) {
		return context.getType(beanName);
	}

	/**
	 * 获取当前环境
	 */
	public static String getActiveProfile() {
		String[] profiles = context.getEnvironment().getActiveProfiles();
		if (profiles.length == 0) {
			return "default";
		}
		return profiles[0];
	}

	/**
	 * 获取当前运行URL
	 */
	public static void printRunningInfo() {
		String osName = System.getProperty("os.name");
		String osVersion = System.getProperty("os.version");
		String osArch = System.getProperty("os.arch");
		String javaVersion = System.getProperty("java.version");
		String ip = IpUtils.getHostIp();
		String port = context.getEnvironment().getProperty("server.port");
		String path = context.getEnvironment().getProperty("server.servlet.context-path");
		if (StringUtils.isEmpty(path)) {
			path = "";
		}
		LOGGER.info("\n\t----------------------------------------------------------\n\t"
				+ "Application  is running! Access URLs:\n\t" + "ActiveProfile: \t\t"
				+ SpringContextUtil.getActiveProfile() + "\n\t" + "Local访问网址: \t\thttp://localhost:" + port + path
				+ "\n\t" + "Network访问网址: \thttp://" + ip + ":" + port + path + "\n\t" + "Swagger访问网址: \thttp://" + ip
				+ ":" + port + path + "/doc.html\n\t" + "----------------------------------------------------------\n\t"
				+ "操作系统：" + osName + "（" + osVersion + "）\t\t系统架构：" + osArch + "\t\tJAVA版本：" + javaVersion + "\n\t"
				+ "----------------------------------------------------------");
	}

}
