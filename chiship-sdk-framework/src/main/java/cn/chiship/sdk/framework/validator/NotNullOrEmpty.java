package cn.chiship.sdk.framework.validator;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.framework.validator.impl.NotNullOrEmptyValidator;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author lijian
 */
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotNullOrEmptyValidator.class)
public @interface NotNullOrEmpty {

	String message() default BaseTipConstants.NOT_EMPTY;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}