package cn.chiship.sdk.framework.validator.impl;

import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.validator.NotNullOrEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 校验不为null或空
 *
 * @author lijian
 */

public class NotNullOrEmptyValidator implements ConstraintValidator<NotNullOrEmpty, String> {

	@Override
	public void initialize(NotNullOrEmpty notNullConstraint) {
		PrintUtil.console("NotNullOrEmptyValidator initialize");
	}

	@Override
	public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
		return !StringUtil.isNullOrEmpty(s);
	}

}
