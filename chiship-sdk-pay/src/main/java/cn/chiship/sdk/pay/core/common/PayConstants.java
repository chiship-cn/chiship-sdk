package cn.chiship.sdk.pay.core.common;

/**
 * 全局常量
 *
 * @author lijian
 */
public class PayConstants {

	/**
	 * 微信支付通知接口后缀
	 */
	public static final String WX_NOTIFY_SUFFIX_PAY = "/wxV3/payNotify/{TRADE_TYPE}";

	/**
	 * 微信退款通知接口后缀
	 */
	public static final String WX_NOTIFY_SUFFIX_REFUND = "/wxV3/refundNotify";

	/**
	 * 微信转账通知接口后缀
	 */
	public static final String WX_NOTIFY_SUFFIX_TRANSFER = "/wxV3/transferNotify";

	/**
	 * 支付宝支付通知接口后缀
	 */
	public static final String ZFB_NOTIFY_SUFFIX_PAY = "/ali/payNotify/{TRADE_TYPE}";

	/**
	 * 返回状态码
	 */
	public final static String RETURN_CODE = "return_code";

	/**
	 * 返回信息
	 */
	public final static String RETURN_MSG = "return_msg";

	/**
	 * 业务结果
	 */
	public final static String RESULT_CODE = "result_code";

	/**
	 * 错误代码
	 */
	public final static String ERR_CODE = "err_code";

	/**
	 * 错误代码描述
	 */
	public final static String ERR_CODE_DES = "err_code_des";

	/**
	 * 参数校验不通过的字段名
	 */
	public final static String ERR_PARAM = "err_param";

	public final static String SUCCESS = "SUCCESS";

}
