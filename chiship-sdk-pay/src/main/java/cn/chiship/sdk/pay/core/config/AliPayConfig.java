package cn.chiship.sdk.pay.core.config;

import cn.chiship.sdk.core.properties.CommonConfigProperties;

/**
 * 支付宝支付配置文件
 *
 * @author lijian
 */
public class AliPayConfig {

    private String prefix = "pay.ali.";

    CommonConfigProperties commonConfigProperties = CommonConfigProperties.getInstance();

    /**
     * 商户主体
     */
    private String mchEntity;
    /**
     * 应用ID
     */
    private String appId;

    /**
     * 应用私钥
     */
    private String rsaPrivateKey;

    /**
     * 支付宝公钥
     */
    private String aliPayPublicKey;


    public AliPayConfig() {

    }

    /**
     * 创建配置对象
     *
     * @param appId
     * @param privateKey
     * @param publicKey
     */
    public AliPayConfig(String appId, String privateKey, String publicKey) {
        this.appId = appId;
        this.rsaPrivateKey = privateKey;
        this.aliPayPublicKey = publicKey;
    }

    public String getMchEntity() {
        return commonConfigProperties.getValue(prefix + "mch-entity", mchEntity);
    }

    public String getAppId() {
        return commonConfigProperties.getValue(prefix + "app-id", appId);
    }

    public String getRsaPrivateKey() {
        return commonConfigProperties.getValue(prefix + "primary-key", rsaPrivateKey);
    }

    public String getAliPayPublicKey() {
        return commonConfigProperties.getValue(prefix + "public-key", aliPayPublicKey);
    }

}
