package cn.chiship.sdk.pay.core.config;

import cn.chiship.sdk.core.properties.CommonConfigProperties;

/**
 * 微信支付v3配置文件
 *
 * @author lijian
 */
public class WxPayV3Config {

	private String prefix = "pay.wx.";

	CommonConfigProperties commonConfigProperties = CommonConfigProperties.getInstance();

	/**
	 * 商户主体
	 */
	private String mchEntity;

	/**
	 * 商户号
	 */
	private String mchId;

	/**
	 * 私钥
	 */
	private String privateKey;

	/**
	 * 证书序列号
	 */
	private String serialNumber;

	/**
	 * 秘钥
	 */
	private String mchKey;

	/**
	 * 支付平台
	 */
	private String appId;

	public WxPayV3Config() {

	}

	/**
	 * 创建配置对象
	 * @param appId
	 */
	public WxPayV3Config(String appId) {
		this.appId = appId;
	}

	/**
	 * 创建配置对象
	 * @param mchId
	 * @param appId
	 * @param privateKey
	 * @param serialNumber
	 * @param mchKey
	 */
	public WxPayV3Config(String mchId, String appId, String privateKey, String serialNumber, String mchKey) {
		this.mchId = mchId;
		this.privateKey = privateKey;
		this.serialNumber = serialNumber;
		this.mchKey = mchKey;
		this.appId = appId;
	}

	public String getAppId() {
		return commonConfigProperties.getValue(prefix + "app-id", appId);
	}

	public String getMchId() {
		return commonConfigProperties.getValue(prefix + "mch-id", mchId);
	}

	public String getMchEntity() {
		return commonConfigProperties.getValue(prefix + "mch-entity", mchEntity);
	}

	public String getPrivateKey() {
		return commonConfigProperties.getValue(prefix + "v3.primary-key", privateKey);
	}

	public String getSerialNumber() {
		return commonConfigProperties.getValue(prefix + "v3.serial-number", serialNumber);
	}

	public String getMchKey() {
		return commonConfigProperties.getValue(prefix + "v3.key", mchKey);
	}

}
