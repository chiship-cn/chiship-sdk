package cn.chiship.sdk.pay.core.enums;

/**
 * @author lijian 支付方式工厂类枚举
 */
public enum PayClassEnum {

	/**
	 * 微信支付V3
	 */
	PAY_WX_V3("wx_v3", "cn.chiship.sdk.pay.factory.WxPayV3Factory"),

	/**
	 * 支付宝支付
	 */
	PAY_ZFB("zfb", "cn.chiship.sdk.pay.factory.AliPayFactory"),

	;

	public String code;

	public String value;

	PayClassEnum(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public static String getValue(String code) {
		PayClassEnum[] payClassEnums = values();
		for (PayClassEnum payClassEnum : payClassEnums) {
			if (payClassEnum.getCode().equals(code)) {
				return payClassEnum.getValue();
			}
		}
		return null;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
