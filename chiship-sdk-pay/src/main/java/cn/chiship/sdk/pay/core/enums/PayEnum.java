package cn.chiship.sdk.pay.core.enums;

/**
 * @author lijian 支付方式枚举
 */
public enum PayEnum {

	/**
	 * 微信支付V3
	 */
	PAY_WX_V3("wx_v3", "微信V3"),

	/**
	 * 支付宝支付
	 */
	PAY_ZFB("zfb", "支付宝"),

	;

	public String type;

	public String message;

	PayEnum(String type, String message) {
		this.type = type;
		this.message = message;
	}

	/**
	 * 根据标识返回枚举
	 * @param type 标识
	 * @return PayEnum
	 */
	public static PayEnum getPayEnum(String type) {
		PayEnum[] payEnums = values();
		for (PayEnum payEnum : payEnums) {
			if (payEnum.getType().equals(type)) {
				return payEnum;
			}
		}
		throw new RuntimeException("不支持的支付渠道，目前支持微信支付(wx_v3)、支付宝支付(zfb)");
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
