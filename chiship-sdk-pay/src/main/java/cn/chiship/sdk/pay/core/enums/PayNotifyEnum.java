package cn.chiship.sdk.pay.core.enums;

/**
 * @author lijian 支付通知枚举
 */
public enum PayNotifyEnum {

	/**
	 * 付款
	 */
	PAY_NOTIFY("pay", "付款"),

	/**
	 * 退款
	 */
	PAY_REFUND("refund", "退款"),

	/**
	 * 转账
	 */
	PAY_TRANSFER("transfer", "转账"),

	;

	public String type;

	public String message;

	PayNotifyEnum(String type, String message) {
		this.type = type;
		this.message = message;
	}

	/**
	 * 根据标识返回枚举
	 * @param type 标识
	 * @return PayEnum
	 */
	public static PayNotifyEnum getPayEnum(String type) {
		PayNotifyEnum[] payEnums = values();
		for (PayNotifyEnum payEnum : payEnums) {
			if (payEnum.getType().equals(type)) {
				return payEnum;
			}
		}
		throw new RuntimeException("不支持的支付通知渠道！");
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
