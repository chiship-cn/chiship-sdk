package cn.chiship.sdk.pay.core.enums;

/**
 * 交易方式
 *
 * @author lj
 */
public enum TradeTypeEnum {

	/**
	 * PC支付
	 */
	PC("PC", "PC支付"),

	/**
	 * H5支付
	 */
	H5("H5", "H5支付"),

	/**
	 * 公众号支付
	 */
	OFFICIAL_ACCOUNT("OFFICIAL_ACCOUNT", "公众号支付"),
	/**
	 * 小程序支付
	 */
	MINI_PROGRAM("MINI_PROGRAM", "小程序支付"),

	/**
	 * APP支付
	 */
	SDK("SDK_PAY", "SDK支付"),

	/**
	 * 二维码支付
	 */
	QR_CODE("QR_CODE", "二维码支付"),

	/**
	 * 扫码支付
	 */
	SCAN("USER_SCAN", "扫码支付"),;

	public String code;

	public String message;

	TradeTypeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * 根据标识返回枚举
	 * @param code 标识
	 * @return TradeTypeEnum
	 */
	public static TradeTypeEnum getTradeTypeEnum(String code) {
		TradeTypeEnum[] tradeTypeEnums = values();
		for (TradeTypeEnum tradeTypeEnum : tradeTypeEnums) {
			if (tradeTypeEnum.getCode().equals(code)) {
				return tradeTypeEnum;
			}
		}
		throw new RuntimeException("不支持的交易方式！");
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
