package cn.chiship.sdk.pay.core.model;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.StringUtil;

/**
 * 账单下载
 *
 * @author lj
 */
public class PayBillDownloadModel {

	/**
	 * 账单日期,格式为yyyy-MM-dd 支付宝月账单格式为yyyy-MM
	 */
	private String billDate;

	/**
	 * 账单类型 --支付宝 默认trade ----trade：商户基于支付宝交易收单的业务账单；
	 * ----signcustomer：基于商户支付宝余额收入及支出等资金变动的账务账单； ----merchant_act：营销活动账单，包含营销活动的发放，核销记录
	 * ----trade_zft_merchant：直付通二级商户查询交易的业务账单； ----zft_acc：直付通平台商查询二级商户流水使用，返回所有二级商户流水。
	 * --微信 默认ALL ----ALL（默认值），返回当日所有订单信息（不含充值退款订单） ----SUCCESS，返回当日成功支付的订单（不含充值退款订单）
	 * ----REFUND，返回当日退款订单（不含充值退款订单） ----RECHARGE_REFUND，返回当日充值退款订单
	 */
	private String billType;

	public PayBillDownloadModel(String billDate) {
		this.billDate = billDate;
		checkDate();
	}

	public PayBillDownloadModel(String billDate, String billType) {
		this.billDate = billDate;
		this.billType = billType;

		checkDate();
		if (StringUtil.isNullOrEmpty(billType)) {
			throw new BusinessException("账单类型不能为空");
		}

	}

	void checkDate() {
		if (StringUtil.isNullOrEmpty(billDate)) {
			throw new BusinessException("账单日期不能为空");
		}
		String[] dates = billDate.split("-");
		if (dates.length < 2 || dates.length > 3) {
			throw new BusinessException("账单日期格式不合法，格式应为：yyyy-MM-dd yyyy-MM");
		}
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

}
