package cn.chiship.sdk.pay.core.model;

/**
 * 支付配置传输对象
 *
 * @author lijian
 */
public class PayDTO {

	/**
	 * 支付宝、微信应用标识
	 */
	private String appId;

	/**
	 * 支付宝、微信私钥
	 */
	private String privateKey;

	/**
	 * 支付宝支付公钥
	 */
	private String publicKey;

	/**
	 * 微信商户号
	 */
	private String mchId;

	/**
	 * 微信v2、v3秘钥
	 */
	private String secretKey;

	/**
	 * 证书序列号
	 */
	private String serialNumber;

	/**
	 * 推荐构造支付宝支付参数
	 * @param appId
	 * @param privateKey
	 * @param publicKey
	 */
	public PayDTO(String appId, String privateKey, String publicKey) {
		this.appId = appId;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	/**
	 * 推荐构造相同主体不同应用支付参数
	 * @param appId
	 */
	public PayDTO(String appId) {
		this.appId = appId;
	}

	/**
	 * 推荐构造微信支付v3使用动态商户，动态应用支付参数
	 * @param mchId
	 * @param appId
	 * @param privateKey
	 * @param secretKey
	 * @param serialNumber
	 */
	public PayDTO(String mchId, String appId, String privateKey, String secretKey, String serialNumber) {
		this.appId = appId;
		this.privateKey = privateKey;
		this.mchId = mchId;
		this.secretKey = secretKey;
		this.serialNumber = serialNumber;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

}
