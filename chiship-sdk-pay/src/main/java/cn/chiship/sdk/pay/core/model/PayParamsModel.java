package cn.chiship.sdk.pay.core.model;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.StringUtil;

/**
 * @author lijian 支付参数
 */
public class PayParamsModel {

	/**
	 * 订单号
	 */
	private String orderId;

	/**
	 * 订单名称
	 */
	private String orderName;

	/**
	 * 订单描述
	 */
	private String orderDesc;

	/**
	 * 支付金额 单位是：元
	 */
	private Double totalAmount;

	/**
	 * 通知主机
	 */
	private String notifyDomain;

	/**
	 * 支付成功后返回界面
	 */
	private String returnUrl;

	/**
	 * 支付环境IP
	 */
	private String createIp;

	/**
	 * 支付时OpenId
	 */
	private String openId;

	public PayParamsModel() {
	}

	public PayParamsModel(String orderId, String orderName, String orderDesc, Double totalAmount, String notifyDomain,
			String createIp) {
		this.orderId = orderId;
		this.orderName = orderName;
		this.orderDesc = orderDesc;
		this.totalAmount = totalAmount;
		this.notifyDomain = notifyDomain;
		this.createIp = createIp;
	}

	public String getOrderId() {
		if (orderId.length() < 6) {
			throw new BusinessException("订单号长度必须大于6");
		}
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getNotifyDomain() {
		return notifyDomain;
	}

	public void setNotifyDomain(String notifyDomain) {
		this.notifyDomain = notifyDomain;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getCreateIp() {
		if (StringUtil.isNull(createIp)) {
			return "";
		}
		return createIp;
	}

	public void setCreateIp(String createIp) {
		this.createIp = createIp;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

}
