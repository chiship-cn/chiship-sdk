package cn.chiship.sdk.pay.core.model;

/**
 * 退款实体类
 *
 * @author lj
 */
public class PayRefundModel {

	/**
	 * 退款单号(系统内部)
	 */
	private String refundNo;

	/**
	 * 支付交易号
	 */
	private String businessId;

	/**
	 * 支付金额 单位是：元
	 */
	private Double totalAmount;

	/**
	 * 退款金额
	 */
	private Double refundAmount;

	/**
	 * 退款原因
	 */
	private String refundReason;

	/**
	 * 通知主机
	 */
	private String notifyDomain;

	public PayRefundModel(String refundNo, String businessId, Double totalAmount, Double refundAmount,
			String refundReason, String notifyDomain) {
		this.refundNo = refundNo;
		this.businessId = businessId;
		this.totalAmount = totalAmount;
		this.refundAmount = refundAmount;
		this.refundReason = refundReason;
		this.notifyDomain = notifyDomain;
	}

	public String getRefundNo() {
		return refundNo;
	}

	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getNotifyDomain() {
		return notifyDomain;
	}

	public void setNotifyDomain(String notifyDomain) {
		this.notifyDomain = notifyDomain;
	}

}
