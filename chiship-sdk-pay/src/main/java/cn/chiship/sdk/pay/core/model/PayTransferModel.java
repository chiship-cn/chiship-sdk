package cn.chiship.sdk.pay.core.model;

/**
 * 单笔转账实体类
 *
 * @author lj
 */

public class PayTransferModel {

	/**
	 * 转账编号
	 */
	private String transferNo;

	/**
	 * 接收人员
	 */
	private String receiveUserId;

	/**
	 * 转账金额 单位是：元
	 */
	private Double transferAmount;

	/**
	 * 转账名称
	 */
	private String transferName;

	/**
	 * 转账备注
	 */
	private String transferRemark;

	/**
	 * 通知主机
	 */
	private String notifyDomain;

	public PayTransferModel(String transferNo, String receiveUserId, Double transferAmount, String transferName,
			String transferRemark, String notifyDomain) {
		this.transferNo = transferNo;
		this.receiveUserId = receiveUserId;
		this.transferAmount = transferAmount;
		this.transferName = transferName;
		this.transferRemark = transferRemark;
		this.notifyDomain = notifyDomain;
	}

	public String getTransferNo() {
		return transferNo;
	}

	public void setTransferNo(String transferNo) {
		this.transferNo = transferNo;
	}

	public String getReceiveUserId() {
		return receiveUserId;
	}

	public void setReceiveUserId(String receiveUserId) {
		this.receiveUserId = receiveUserId;
	}

	public Double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(Double transferAmount) {
		this.transferAmount = transferAmount;
	}

	public String getTransferName() {
		return transferName;
	}

	public void setTransferName(String transferName) {
		this.transferName = transferName;
	}

	public String getTransferRemark() {
		return transferRemark;
	}

	public void setTransferRemark(String transferRemark) {
		this.transferRemark = transferRemark;
	}

	public String getNotifyDomain() {
		return notifyDomain;
	}

	public void setNotifyDomain(String notifyDomain) {
		this.notifyDomain = notifyDomain;
	}

}
