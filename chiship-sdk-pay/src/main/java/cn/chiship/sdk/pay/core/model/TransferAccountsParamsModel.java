package cn.chiship.sdk.pay.core.model;

/**
 * @author LiJian 转账实体参数
 */
public class TransferAccountsParamsModel {

	/**
	 * 订单
	 */
	private String orderId;

	/**
	 * 订单名称
	 */
	private String orderName;

	/**
	 * 对方账号
	 */
	private String payeeAccount;

	/**
	 * 对方真实姓名
	 */
	private String payeeRealName;

	/**
	 * 转账金额
	 */
	private Double money;

	/**
	 * 备注
	 */
	private String remark;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getPayeeAccount() {
		return payeeAccount;
	}

	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}

	public String getPayeeRealName() {
		return payeeRealName;
	}

	public void setPayeeRealName(String payeeRealName) {
		this.payeeRealName = payeeRealName;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
