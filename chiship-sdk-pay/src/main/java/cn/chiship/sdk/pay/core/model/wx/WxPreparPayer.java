package cn.chiship.sdk.pay.core.model.wx;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信预支付支付者
 *
 * @author lijian
 */
public class WxPreparPayer {

	/**
	 * 用户标识
	 */
	@JSONField(name = "openid")
	private String openId;

	public WxPreparPayer(String openId) {
		this.openId = openId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

}
