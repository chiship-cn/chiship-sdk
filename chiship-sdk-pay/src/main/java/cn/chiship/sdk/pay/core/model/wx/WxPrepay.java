
package cn.chiship.sdk.pay.core.model.wx;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信预支付实体类
 *
 * @author lijian
 */
public class WxPrepay {

	/**
	 * 应用ID
	 */
	@JSONField(name = "appid")
	private String appId;

	/**
	 * 直连商户号
	 */
	@JSONField(name = "mchid")
	private String mchId;

	/**
	 * 商品描述
	 */
	@JSONField(name = "description")
	private String description;

	/**
	 * 商户订单号
	 */
	@JSONField(name = "out_trade_no")
	private String outTradeNo;

	/**
	 * 附加数据
	 */
	@JSONField(name = "attach")
	private String attach;

	/**
	 * 异步通知地址
	 */
	@JSONField(name = "notify_url")
	private String notifyUrl;

	/**
	 * 电子发票入口开放标识
	 */
	@JSONField(name = "support_fapiao")
	private Boolean supportFapiao;

	/**
	 * 订单金额信息
	 */
	@JSONField(name = "amount")
	private WxPrepayAmount amount;

	/**
	 * 支付者信息
	 */
	@JSONField(name = "payer")
	private WxPreparPayer payer;

	/**
	 * 推荐native支付构建实体
	 * @param mchId
	 * @param appId
	 * @param outTradeNo
	 * @param description
	 * @param notifyUrl
	 * @param amount
	 */
	public WxPrepay(String mchId, String appId, String outTradeNo, String description, String notifyUrl,
			WxPrepayAmount amount) {
		this.mchId = mchId;
		this.appId = appId;
		this.outTradeNo = outTradeNo;
		this.description = description;
		this.notifyUrl = notifyUrl;
		this.amount = amount;
	}

	/**
	 * 推荐JSAPI支付构建实体
	 * @param mchId
	 * @param appId
	 * @param outTradeNo
	 * @param description
	 * @param notifyUrl
	 * @param amount
	 * @param payer
	 */
	public WxPrepay(String mchId, String appId, String outTradeNo, String description, String notifyUrl,
			WxPrepayAmount amount, WxPreparPayer payer) {
		this.mchId = mchId;
		this.appId = appId;
		this.outTradeNo = outTradeNo;
		this.description = description;
		this.notifyUrl = notifyUrl;
		this.amount = amount;
		this.payer = payer;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public Boolean getSupportFapiao() {
		return supportFapiao;
	}

	public void setSupportFapiao(Boolean supportFapiao) {
		this.supportFapiao = supportFapiao;
	}

	public WxPrepayAmount getAmount() {
		return amount;
	}

	public void setAmount(WxPrepayAmount amount) {
		this.amount = amount;
	}

	public WxPreparPayer getPayer() {
		return payer;
	}

	public void setPayer(WxPreparPayer payer) {
		this.payer = payer;
	}

}
