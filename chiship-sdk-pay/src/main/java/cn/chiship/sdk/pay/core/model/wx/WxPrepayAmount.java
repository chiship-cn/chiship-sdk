package cn.chiship.sdk.pay.core.model.wx;

import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信预支付订单金额
 *
 * @author lijian
 */
public class WxPrepayAmount {

	/**
	 * 支付金额（单位：分）
	 */
	@JSONField(name = "total")
	private Integer total;

	/**
	 * 币种，默认CNY
	 */
	@JSONField(name = "currency")
	private String currency;

	public WxPrepayAmount(Integer total) {
		this.total = total;
	}

	public WxPrepayAmount(Integer total, String currency) {
		this.total = total;
		this.currency = currency;
	}

	public Integer getTotal() {
		return this.total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getCurrency() {
		if (StringUtil.isNullOrEmpty(currency)) {
			return "CNY";
		}
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
