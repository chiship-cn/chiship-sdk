package cn.chiship.sdk.pay.core.model.wx;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信退款实体
 *
 * @author lijian
 */
public class WxRefund {

	/**
	 * 支付交易号
	 */
	@JSONField(name = "transaction_id")
	private String transactionId;

	/**
	 * 商户订单号
	 */
	@JSONField(name = "out_trade_no")
	private String outTradeNo;

	/**
	 * 商户退款单号
	 */
	@JSONField(name = "out_refund_no")
	private String outRefundNo;

	/**
	 * 退款原因
	 */
	@JSONField(name = "reason")
	private String reason;

	/**
	 * 退款结果回调
	 */
	@JSONField(name = "notify_url")
	private String notifyUrl;

	/**
	 * 退款信息
	 */
	@JSONField(name = "amount")
	private WxRefundAmount amount;

	public WxRefund(String outRefundNo, String transactionId, String reason, String notifyUrl, WxRefundAmount amount) {
		this.outRefundNo = outRefundNo;
		this.transactionId = transactionId;
		this.reason = reason;
		this.notifyUrl = notifyUrl;
		this.amount = amount;
	}

	public WxRefund(String transactionId, String outTradeNo, String outRefundNo, String reason, String notifyUrl,
			WxRefundAmount amount) {
		this.transactionId = transactionId;
		this.outTradeNo = outTradeNo;
		this.outRefundNo = outRefundNo;
		this.reason = reason;
		this.notifyUrl = notifyUrl;
		this.amount = amount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public WxRefundAmount getAmount() {
		return amount;
	}

	public void setAmount(WxRefundAmount amount) {
		this.amount = amount;
	}

}
