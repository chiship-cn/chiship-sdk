package cn.chiship.sdk.pay.core.model.wx;

import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信退款订单金额
 *
 * @author lijian
 */
public class WxRefundAmount {

	/**
	 * 退款金额（单位：分）
	 */
	@JSONField(name = "refund")
	private Integer refund;

	/**
	 * 订单总金额（单位：分）
	 */
	@JSONField(name = "total")
	private Integer total;

	/**
	 * 币种，默认CNY
	 */
	@JSONField(name = "currency")
	private String currency;

	public WxRefundAmount(Integer refund, Integer total) {
		this.refund = refund;
		this.total = total;
	}

	public WxRefundAmount(Integer refund, Integer total, String currency) {
		this.refund = refund;
		this.total = total;
		this.currency = currency;
	}

	public Integer getRefund() {
		return refund;
	}

	public void setRefund(Integer refund) {
		this.refund = refund;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getCurrency() {
		if (StringUtil.isNullOrEmpty(currency)) {
			return "CNY";
		}
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
