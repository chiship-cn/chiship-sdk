package cn.chiship.sdk.pay.core.model.wx;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * 微信转账
 */
public class WxTransfer {

	/**
	 * 商户appid
	 */
	@JSONField(name = "appid")
	private String appId;

	/**
	 * 商家批次单号
	 */
	@JSONField(name = "out_batch_no")
	private String batchNo;

	/**
	 * 批次名称
	 */
	@JSONField(name = "batch_name")
	private String batchName;

	/**
	 * 批次备注
	 */
	@JSONField(name = "batch_remark")
	private String batchRemark;

	/**
	 * 转账总金额 单位：分
	 */
	@JSONField(name = "total_amount")
	private Integer totalAmount;

	/**
	 * 转账总笔数
	 */
	@JSONField(name = "total_num")
	private Integer totalNum;

	/**
	 * 转账明细列表
	 */
	@JSONField(name = "transfer_detail_list")
	private List<WxTransferItem> wxTransferItems;

	/**
	 * 退款结果回调
	 */
	@JSONField(name = "notify_url")
	private String notifyUrl;

	public WxTransfer(String appId, String batchNo, String batchName, String batchRemark, Integer totalAmount,
			Integer totalNum, List<WxTransferItem> wxTransferItems, String notifyUrl) {
		this.appId = appId;
		this.batchNo = batchNo;
		this.batchName = batchName;
		this.batchRemark = batchRemark;
		this.totalAmount = totalAmount;
		this.totalNum = totalNum;
		this.wxTransferItems = wxTransferItems;
		this.notifyUrl = notifyUrl;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getBatchRemark() {
		return batchRemark;
	}

	public void setBatchRemark(String batchRemark) {
		this.batchRemark = batchRemark;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	public List<WxTransferItem> getWxTransferItems() {
		return wxTransferItems;
	}

	public void setWxTransferItems(List<WxTransferItem> wxTransferItems) {
		this.wxTransferItems = wxTransferItems;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

}
