package cn.chiship.sdk.pay.core.model.wx;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信转账条目
 */
public class WxTransferItem {

	/**
	 * 商家明细单号
	 */
	@JSONField(name = "out_detail_no")
	private String itemNo;

	/**
	 * 转账金额 单位为“分”
	 */
	@JSONField(name = "transfer_amount")
	private Integer amount;

	/**
	 * 转账备注
	 */
	@JSONField(name = "transfer_remark")
	private String remark;

	/**
	 * 收款用户openid
	 */
	@JSONField(name = "openid")
	private String openId;

	public WxTransferItem(String itemNo, Integer amount, String remark, String openId) {
		this.itemNo = itemNo;
		this.amount = amount;
		this.remark = remark;
		this.openId = openId;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

}
