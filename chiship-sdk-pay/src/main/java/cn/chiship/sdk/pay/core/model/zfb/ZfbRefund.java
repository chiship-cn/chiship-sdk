package cn.chiship.sdk.pay.core.model.zfb;

/**
 * 退款实体类
 *
 * @author lj
 */
public class ZfbRefund {

	/**
	 * 退款单号(系统内部)
	 */
	private String refundNo;

	/**
	 * 支付交易号
	 */
	private String transactionId;

	/**
	 * 退款金额
	 */
	private Double refundAmount;

	/**
	 * 退款原因
	 */
	private String refundReason;

	public ZfbRefund(String refundNo, String transactionId, Double refundAmount, String refundReason) {
		this.refundNo = refundNo;
		this.transactionId = transactionId;
		this.refundAmount = refundAmount;
		this.refundReason = refundReason;
	}

	public String getRefundNo() {
		return refundNo;
	}

	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

}
