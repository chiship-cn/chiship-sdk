package cn.chiship.sdk.pay.core.model.zfb;

/**
 * 支付宝转账
 */
public class ZfbTransfer {

	/**
	 * 商家单号
	 */
	private String transferNo;

	/**
	 * 接收用户
	 */
	private String userId;

	/**
	 * 转账总金额 单位：元
	 */
	private Double transferAmount;

	/**
	 * 转账名称
	 */
	private String transferName;

	/**
	 * 转账备注
	 */
	private String transferRemark;

	public ZfbTransfer(String transferNo, String userId, Double transferAmount, String transferName,
			String transferRemark) {
		this.transferNo = transferNo;
		this.userId = userId;
		this.transferAmount = transferAmount;
		this.transferName = transferName;
		this.transferRemark = transferRemark;
	}

	public String getTransferNo() {
		return transferNo;
	}

	public void setTransferNo(String transferNo) {
		this.transferNo = transferNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(Double transferAmount) {
		this.transferAmount = transferAmount;
	}

	public String getTransferName() {
		return transferName;
	}

	public void setTransferName(String transferName) {
		this.transferName = transferName;
	}

	public String getTransferRemark() {
		return transferRemark;
	}

	public void setTransferRemark(String transferRemark) {
		this.transferRemark = transferRemark;
	}

}
