package cn.chiship.sdk.pay.factory;

import cn.chiship.sdk.pay.core.config.AliPayConfig;
import cn.chiship.sdk.pay.core.model.PayDTO;
import cn.chiship.sdk.pay.services.PaymentService;
import cn.chiship.sdk.pay.services.impl.AliPayServiceImpl;

/**
 * 阿里支付服务接口工厂类
 *
 * @author lj
 */
public class AliPayFactory {

	private AliPayFactory() {
	}

	/**
	 * 默认配置形式生成对象
	 * @return PaymentService
	 */
	public static PaymentService getPaymentService() {
		return new AliPayServiceImpl();
	}

	/**
	 * 使用指定参数配置生成对象
	 * @param payDTO 支付配置
	 * @return PaymentService
	 */
	public static PaymentService getPaymentService(PayDTO payDTO) {
		String appId = payDTO.getAppId();
		String privateKey = payDTO.getPrivateKey();
		String publicKey = payDTO.getPublicKey();
		AliPayConfig aliPayConfig = new AliPayConfig(appId, privateKey, publicKey);
		return new AliPayServiceImpl(aliPayConfig);
	}

}
