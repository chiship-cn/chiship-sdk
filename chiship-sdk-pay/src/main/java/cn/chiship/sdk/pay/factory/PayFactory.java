package cn.chiship.sdk.pay.factory;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.pay.core.enums.PayClassEnum;
import cn.chiship.sdk.pay.core.enums.PayEnum;
import cn.chiship.sdk.pay.core.model.PayDTO;
import cn.chiship.sdk.pay.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * DfsFactory工厂类,根据系统用户配置，生成及缓存对应的上传下载接口实现
 *
 * @author lj
 */
public class PayFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayFactory.class);

    /**
     * 获取 PaymentService
     *
     * @param payEnum
     * @return PaymentService
     */
    public static PaymentService getPaymentService(PayEnum payEnum) {
        Class cls;
        try {
            cls = Class.forName(PayClassEnum.getValue(payEnum.getType()));
            Method staticMethod = cls.getDeclaredMethod("getPaymentService");
            PaymentService paymentService = (PaymentService) staticMethod.invoke(cls);
            return paymentService;
        } catch (Exception e) {
            throw new BusinessException("获取支付服务发生异常:" + e.getMessage());
        }
    }

    /**
     * 获取 FileStorageService
     *
     * @param payEnum
     * @param payDTO
     * @return PaymentService
     */
    public static PaymentService getPaymentService(PayEnum payEnum, PayDTO payDTO) {
        Class cls;
        try {
            cls = Class.forName(PayClassEnum.getValue(payEnum.getType()));
            Method staticMethod = cls.getDeclaredMethod("getPaymentService", PayDTO.class);
            PaymentService paymentService = (PaymentService) staticMethod.invoke(cls, payDTO);
            return paymentService;
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("获取支付服务发生异常:" + e.getMessage());
        }
    }

}
