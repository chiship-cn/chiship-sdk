package cn.chiship.sdk.pay.factory;

import cn.chiship.sdk.pay.core.config.WxPayV3Config;
import cn.chiship.sdk.pay.core.model.PayDTO;
import cn.chiship.sdk.pay.services.PaymentService;
import cn.chiship.sdk.pay.services.impl.WxPayV3ServiceImpl;

/**
 * 微信支付v3服务接口工厂类
 *
 * @author lj
 */
public class WxPayV3Factory {

	private WxPayV3Factory() {
	}

	/**
	 * 默认配置形式生成对象
	 * @return PaymentService
	 */
	public static PaymentService getPaymentService() {
		return new WxPayV3ServiceImpl();
	}

	/**
	 * 使用指定参数配置生成对象
	 * @param payDTO 支付配置
	 * @return PaymentService
	 */
	public static PaymentService getPaymentService(PayDTO payDTO) {
		String appId = payDTO.getAppId();
		String privateKey = payDTO.getPrivateKey();
		String mchId = payDTO.getMchId();
		String secretKey = payDTO.getSecretKey();
		String serialNumber = payDTO.getSerialNumber();
		WxPayV3Config wxPayV3Config = new WxPayV3Config(mchId, appId, privateKey, secretKey, serialNumber);
		return new WxPayV3ServiceImpl(wxPayV3Config);
	}

}
