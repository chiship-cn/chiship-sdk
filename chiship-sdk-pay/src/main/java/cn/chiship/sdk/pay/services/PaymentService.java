package cn.chiship.sdk.pay.services;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.pay.core.enums.TradeTypeEnum;
import cn.chiship.sdk.pay.core.model.PayBillDownloadModel;
import cn.chiship.sdk.pay.core.model.PayParamsModel;
import cn.chiship.sdk.pay.core.model.PayRefundModel;
import cn.chiship.sdk.pay.core.model.PayTransferModel;

/**
 * 支付服务网关
 *
 * @author lj
 */
public interface PaymentService {

	/**
	 * 支付
	 * @param payParamsModel
	 * @param tradeTypeEnum
	 * @return 结果
	 */
	BaseResult doPay(PayParamsModel payParamsModel, TradeTypeEnum tradeTypeEnum);

	/**
	 * 订单查询
	 * @param businessId 业务号
	 * @param isOrder true 订单号查询 false 流水号查询
	 * @return 结果
	 */
	BaseResult doQuery(String businessId, Boolean isOrder);

	/**
	 * 下载账单
	 * @param payBillDownloadModel
	 * @return 结果
	 */
	BaseResult downloadBill(PayBillDownloadModel payBillDownloadModel);

	/**
	 * 退款
	 * @param payRefundModel
	 * @return 结果
	 */
	BaseResult doRefund(PayRefundModel payRefundModel);

	/**
	 * 退款查询
	 * @param refundId 商户退款单号
	 * @param orderNo 商户订单号
	 * @return 结果
	 */
	BaseResult doRefundQuery(String refundId, String orderNo);

	/**
	 * 转账
	 * @param payTransferModel
	 * @return 结果
	 */
	BaseResult doTransfer(PayTransferModel payTransferModel);

	/**
	 * 转账查询
	 * @param transferNo
	 * @return 结果
	 */
	BaseResult doTransferQuery(String transferNo);

	/**
	 * 商户余额查询
	 * @return 结果
	 */
	BaseResult doBalanceQuery();

}
