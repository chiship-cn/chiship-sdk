package cn.chiship.sdk.pay.services.impl;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.pay.core.common.PayConstants;
import cn.chiship.sdk.pay.core.config.AliPayConfig;
import cn.chiship.sdk.pay.core.enums.TradeTypeEnum;
import cn.chiship.sdk.pay.core.model.PayBillDownloadModel;
import cn.chiship.sdk.pay.core.model.PayParamsModel;
import cn.chiship.sdk.pay.core.model.PayRefundModel;
import cn.chiship.sdk.pay.core.model.PayTransferModel;
import cn.chiship.sdk.pay.core.model.zfb.ZfbRefund;
import cn.chiship.sdk.pay.core.model.zfb.ZfbTransfer;
import cn.chiship.sdk.pay.core.util.PayUtil;
import cn.chiship.sdk.pay.services.PaymentService;
import cn.chiship.sdk.pay.core.util.AliPayUtil;

/**
 * 支付宝支付服务实现
 *
 * @author lj
 */
public class AliPayServiceImpl implements PaymentService {

    private AliPayConfig aliPayConfig;

    private AliPayUtil aliPayUtil;

    public AliPayServiceImpl() {
        this.aliPayConfig = new AliPayConfig();
        PrintUtil.console("默认构建支付句柄", this.aliPayConfig.getAppId());
        this.aliPayUtil = AliPayUtil.getInstance().config(this.aliPayConfig);
    }

    public AliPayServiceImpl(AliPayConfig aliPayConfig) {
        this.aliPayConfig = aliPayConfig;
        PrintUtil.console("传递对象构建支付句柄", this.aliPayConfig.getAppId());
        this.aliPayUtil = AliPayUtil.getInstance().config(this.aliPayConfig);
    }

    @Override
    public BaseResult doPay(PayParamsModel payParamsModel, TradeTypeEnum tradeTypeEnum) {
        PrintUtil.console("支付APPID", aliPayConfig.getAppId());
        BaseResult baseResult;
        String openId = payParamsModel.getOpenId();
        String orderId = payParamsModel.getOrderId();
        String orderName = payParamsModel.getOrderName();
        String orderDesc = payParamsModel.getOrderDesc();
        Double totalAmount = payParamsModel.getTotalAmount();
        String returnUrl = payParamsModel.getReturnUrl();
        String notifyUrl = payParamsModel.getNotifyDomain()
                + PayConstants.ZFB_NOTIFY_SUFFIX_PAY.replace("{TRADE_TYPE}", tradeTypeEnum.getCode());
        PrintUtil.console("支付宝支付通知", notifyUrl);
        switch (tradeTypeEnum.getCode()) {
            case "PC":
                // PC支付
                if (StringUtil.isNullOrEmpty(returnUrl)) {
                    return BaseResult.error("支付宝支付，缺少参数【returnUrl】");
                }
                baseResult = aliPayUtil.doPagePay(orderId, totalAmount, orderName, orderDesc, returnUrl, notifyUrl);
                break;
            case "H5":
                // H5支付
                if (StringUtil.isNullOrEmpty(returnUrl)) {
                    return BaseResult.error("支付宝支付，缺少参数【returnUrl】");
                }
                baseResult = aliPayUtil.doWapPay(orderId, totalAmount, orderName, orderDesc, returnUrl, notifyUrl);
                break;
            case "QR_CODE":
                // 二维码支付
                baseResult = aliPayUtil.doQrCodePay(orderId, totalAmount, orderName, orderDesc, notifyUrl);
                break;
            case "MINI_PROGRAM":
                // 小程序支付
                if (StringUtil.isNullOrEmpty(openId)) {
                    return BaseResult.error("正在发起" + tradeTypeEnum.getMessage() + "，缺少参数【openId】");
                }
                baseResult = aliPayUtil.doJsApiPay(openId, orderId, totalAmount, orderName, orderDesc, notifyUrl);
                break;
            default:
                return BaseResult.error(tradeTypeEnum.getMessage() + ":不支持此支付通道");

        }
        return baseResult;
    }

    @Override
    public BaseResult doQuery(String businessId, Boolean isOrder) {
        return aliPayUtil.doQuery(businessId, isOrder);
    }

    @Override
    public BaseResult downloadBill(PayBillDownloadModel payBillDownloadModel) {
        return aliPayUtil.downloadBill(payBillDownloadModel);
    }

    @Override
    public BaseResult doRefund(PayRefundModel payRefundModel) {
        BaseResult baseResult = PayUtil.checkRefund(payRefundModel);
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        ZfbRefund zfbRefund = new ZfbRefund(payRefundModel.getRefundNo(), payRefundModel.getBusinessId(),
                payRefundModel.getRefundAmount(), payRefundModel.getRefundReason());
        return aliPayUtil.doRefund(zfbRefund);
    }

    @Override
    public BaseResult doRefundQuery(String refundId, String orderNo) {
        return aliPayUtil.doRefundQuery(refundId, orderNo);
    }

    @Override
    public BaseResult doTransfer(PayTransferModel payTransferModel) {
        ZfbTransfer zfbTransfer = new ZfbTransfer(payTransferModel.getTransferNo(), payTransferModel.getReceiveUserId(),
                payTransferModel.getTransferAmount(), payTransferModel.getTransferName(),
                payTransferModel.getTransferRemark());
        return aliPayUtil.doTransfer(zfbTransfer);
    }

    @Override
    public BaseResult doTransferQuery(String transferNo) {
        return BaseResult.error("不支持支付宝转账查询");
    }

    @Override
    public BaseResult doBalanceQuery() {
        return aliPayUtil.doBalanceQuery();
    }

}
