package cn.chiship.sdk.third.ali;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.exception.ExceptionUtil;
import cn.chiship.sdk.core.exception.custom.SystemErrorException;
import cn.chiship.sdk.core.properties.CommonConfigProperties;
import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.third.core.common.ThirdConstants;
import cn.chiship.sdk.third.core.model.BaseConfigModel;
import com.aliyun.dm20151123.models.SingleSendMailRequest;
import com.aliyun.dm20151123.models.SingleSendMailResponse;
import com.aliyun.dm20151123.models.SingleSendMailResponseBody;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;

/**
 * @author lijian 阿里云邮件工具
 */
public class AliMailUtil {

    CommonConfigProperties commonConfigProperties = CommonConfigProperties.getInstance();

    private static final Logger LOGGER = LoggerFactory.getLogger(AliMailUtil.class);

    private BaseConfigModel baseConfigModel;

    private static AliMailUtil instance;

    /**
     * 发送者
     */
    private String sendEmailAccount;
    /**
     * 是否回执
     */
    private Boolean isReply;

    private AliMailUtil() {
    }

    public static synchronized AliMailUtil getInstance() {
        if (instance == null) {
            instance = new AliMailUtil();
        }
        return instance;
    }

    public AliMailUtil config() {
        try {
            this.baseConfigModel = new BaseConfigModel(commonConfigProperties.getValue("third.sms.ali.accessKey"),
                    commonConfigProperties.getValue("third.sms.ali.accessSecret"));
            if (StringUtil.isNullOrEmpty(this.baseConfigModel.getAppKey())
                    || StringUtil.isNullOrEmpty(baseConfigModel.getAppSecret())) {
                throw new SystemErrorException(
                        "兄弟,请确保阿里云短信的各个属性[ALI_SMS_ACCESS_KEY_ID、ALI_SMS_ACCESS_KEY_SECRET]配置存在或值不为空!");
            }
        } catch (MissingResourceException e) {
            throw new SystemErrorException(ThirdConstants.ERROR_EXIST_TIP_1);
        }
        return this;
    }

    public AliMailUtil config(BaseConfigModel baseConfigModel) {
        this.baseConfigModel = baseConfigModel;
        return this;
    }

    public AliMailUtil load(String sendEmailAccount, Boolean isReply) {
        this.sendEmailAccount = sendEmailAccount;
        this.isReply = isReply;
        return this;
    }

    public BaseResult sendEmail(String receiverEmail, String subject, String content) {
        if (StringUtil.isNull(this.isReply)) {
            this.isReply = false;
        }
        if (StringUtil.isNullOrEmpty(this.sendEmailAccount)) {
            return BaseResult.error("请先调用load方法加载发送者!");
        }
        if (StringUtil.isNullOrEmpty(receiverEmail)) {
            return BaseResult.error("请输入接收者邮箱!");
        }
        if (StringUtil.isNullOrEmpty(subject)) {
            return BaseResult.error("请输入邮件标题!");
        }
        if (subject.length() > 100) {
            return BaseResult.error("邮件标题不能大于100个字符!");
        }
        if (StringUtil.isNullOrEmpty(content)) {
            return BaseResult.error("请输入邮件内容!");
        }
        if (content.length() > 80000) {
            return BaseResult.error("邮件内容不能超过80k字符");
        }
        try {
            Config config = new Config().setAccessKeyId(this.baseConfigModel.getAppKey())
                    .setAccessKeySecret(this.config().baseConfigModel.getAppSecret());
            config.endpoint = "dm.aliyuncs.com";
            com.aliyun.dm20151123.Client client = new com.aliyun.dm20151123.Client(config);
            SingleSendMailRequest singleSendMailRequest = new SingleSendMailRequest()
                    .setAccountName(this.sendEmailAccount)
                    //类型：为发信地址
                    .setAddressType(1)
                    .setToAddress(receiverEmail)
                    .setSubject(subject)
                    .setHtmlBody(content)
                    .setReplyToAddress(this.isReply);
            RuntimeOptions runtime = new RuntimeOptions();
            SingleSendMailResponse mailResponse = client.singleSendMailWithOptions(singleSendMailRequest, runtime);
            SingleSendMailResponseBody responseBody = mailResponse.body;
            Map<String, String> resultMap = new HashMap<>();
            resultMap.put("requestId", responseBody.requestId);
            resultMap.put("envId", responseBody.getEnvId());
            return BaseResult.ok(resultMap);
        } catch (TeaException error) {
            com.aliyun.teautil.Common.assertAsString(error.message);
            return BaseResult.error(error.message);
        } catch (Exception e) {
            TeaException error = new TeaException(e.getMessage(), e);
            com.aliyun.teautil.Common.assertAsString(error.message);
            LOGGER.error("AliMailUtil发生错误:{}", e.getLocalizedMessage());
            return ExceptionUtil.formatException(e);
        }
    }

    public static void main(String[] args) {
        PrintUtil.console(AliMailUtil.getInstance()
                .config()
                .load("system@email.chiship.cn",Boolean.FALSE)
                .sendEmail("974028417@qq.com", "测试无需绘制", "124312433"));
    }

}
