package cn.chiship.sdk.third.baidu;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.third.core.common.ThirdConstants;
import com.alibaba.fastjson.JSON;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 百度AIchangliang
 *
 * @author lijian
 */
public class BaiDuAiConstant extends ThirdConstants {

	private static final String ERROR_CODE = "error_code";

	public static final String AIP_SERVER_HOST = "https://aip.baidubce.com";

	/**
	 * 获得accessToken
	 */
	public static final String GET_TOKEN = "/oauth/2.0/token";

	/**
	 * 身份证识别
	 */
	public static final String ID_CARD = "/rest/2.0/ocr/v1/idcard";

	/**
	 * 银行卡识别
	 */
	public static final String BANK_CARD = "/rest/2.0/ocr/v1/bankcard";

	/**
	 * 营业执照识别
	 */
	public static final String BUSINESS_LICENSE = "/rest/2.0/ocr/v1/business_license";

	/**
	 * 护照识别
	 */
	public static final String PASS_PORT = "/rest/2.0/ocr/v1/passport";

	/**
	 * 社保卡识别
	 */
	public static final String SOCIAL_SECURITY_CARD = "/rest/2.0/ocr/v1/social_security_card";

	/**
	 * 车牌识别
	 */
	public static final String LICENSE_PLATE = "/rest/2.0/ocr/v1/license_plate";

	/**
	 * 公共请求头
	 * @return 结果
	 */
	public static Map<String, String> commonHeaders() {
		Map<String, String> headers = new HashMap<>(2);
		return headers;
	}

	public static String getCodeStatusMessage(Integer code) {

		Map<String, String> codeStatus = new HashMap<>(2);

		codeStatus.put("SDK102", "读取图片文件错误");
		codeStatus.put("0", "操作成功");
		codeStatus.put("6", "无权限访问该用户数据，创建应用时未勾选相关接口");
		codeStatus.put("216015", "模块关闭");
		codeStatus.put("216100", "非法参数");
		codeStatus.put("216101", "参数数量不够");
		codeStatus.put("216102", "业务不支持");
		codeStatus.put("216103", "参数太长");
		codeStatus.put("216110", "APP ID不存在");
		codeStatus.put("216111", "非法用户ID");
		codeStatus.put("216200", "空的图片");
		codeStatus.put("216201", "图片格式错误");
		codeStatus.put("216202", "图片大小错误");
		codeStatus.put("216300", "DB错误");
		codeStatus.put("216400", "后端系统错误");
		codeStatus.put("216401", "内部错误");
		codeStatus.put("216500", "未知错误");
		codeStatus.put("216600", "身份证的ID格式错误");
		codeStatus.put("216601", "身份证的ID和名字不匹配");
		codeStatus.put("216630", "识别错误");
		codeStatus.put("216631", "识别银行卡错误（通常为检测不到银行卡）");
		codeStatus.put("216632", "unknown error");
		codeStatus.put("216633", "识别身份证错误（通常为检测不到身份证）");
		codeStatus.put("216634", "检测错误");
		codeStatus.put("216635", "获取mask图片错误");
		codeStatus.put("222202", "图片中没有人脸");
		codeStatus.put("222203", "无法解析人脸");

		codeStatus.put("282000", "业务逻辑层内部错误");
		codeStatus.put("282001", "业务逻辑层后端服务错误");
		codeStatus.put("282100", "图片压缩转码错误");
		codeStatus.put("282102", "未检测到图片中识别目标，请确保图片中包含对应卡证票据，出现此问题的原因一般为：您上传了非卡证图片、图片不完整或模糊");
		codeStatus.put("282103", "图片目标识别错误，请确保图片中包含对应卡证票据，出现此问题的原因一般为：您上传了非卡证图片、图片不完整或模糊");

		return codeStatus.get(code.toString());

	}

	public static BaseResult analysisAipHttpResponse(BaseResult baseResult) {
		if (!baseResult.isSuccess()) {
			return baseResult;
		}
		com.alibaba.fastjson.JSONObject json = JSON.parseObject(StringUtil.getString(baseResult.getData()));
		if (json.containsKey(ERROR_CODE)) {
			String msg = getCodeStatusMessage(json.getInteger(ERROR_CODE));
			return BaseResult.error(msg == null
					? "错误码:【" + json.getInteger(ERROR_CODE) + "】,描述：【" + json.getString("error_msg") + "】" : msg);
		}
		else {
			return BaseResult.ok(json);
		}
	}

	public static BaseResult analysisJson(JSONObject json) {
		Boolean success = false;
		String msg = null;
		if (json.has(ERROR_CODE)) {
			Integer errorCode = json.getInt(ERROR_CODE);
			if (0 == errorCode) {
				success = true;
			}
			msg = getCodeStatusMessage(json.getInt(ERROR_CODE));
			if (StringUtil.isNullOrEmpty(msg)) {
				msg = "错误码:【" + errorCode + "】,描述：【" + json.getString("error_msg") + "】";
			}

		}
		else {
			success = true;
		}
		if (Boolean.TRUE.equals(success)) {
			return BaseResult.ok(json);
		}
		return BaseResult.error(msg);

	}

}
