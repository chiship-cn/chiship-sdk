package cn.chiship.sdk.third.baidu;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.third.baidu.model.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Set;

/**
 * 结果分析
 *
 * @author lijian
 */
public class BaiDuAiResultAnalyze {

	private static final String WORDS = "words";

	private static final String WORDS_RESULT = "words_result";

	private BaiDuAiResultAnalyze() {
	}

	/**
	 * 解析身份证
	 * @param baseResult 解析内容
	 * @return 结果
	 */
	public static BaseResult analyzeIdCard(BaseResult baseResult) {
		if (baseResult.isSuccess()) {
			JSONObject json = (JSONObject) baseResult.getData();
			JSONObject wordsResult = recombinationJSON(json.getJSONObject(WORDS_RESULT));
			IdCardVo idCardVo = JSON.parseObject(wordsResult.toString(), IdCardVo.class);
			return BaseResult.ok(idCardVo);
		}
		else {
			return baseResult;
		}
	}

	/**
	 * 解析银行卡
	 * @param baseResult 解析内容
	 * @return 结果
	 */
	public static BaseResult analyzeBankCard(BaseResult baseResult) {
		if (baseResult.isSuccess()) {
			JSONObject json = (JSONObject) baseResult.getData();
			JSONObject result = json.getJSONObject("result");
			BankCardVo bankCardVo = JSON.parseObject(result.toString(), BankCardVo.class);
			return BaseResult.ok(bankCardVo);
		}
		else {
			return baseResult;
		}
	}

	/**
	 * 解析营业执照
	 * @param baseResult
	 * @return 结果
	 */
	public static BaseResult analyzeBusinessLicense(BaseResult baseResult) {
		if (baseResult.isSuccess()) {
			JSONObject json = (JSONObject) baseResult.getData();
			JSONObject wordsResult = recombinationJSON(json.getJSONObject(WORDS_RESULT));
			BusinessLicenseVo businessLicenseVo = JSON.parseObject(wordsResult.toString(), BusinessLicenseVo.class);
			return BaseResult.ok(businessLicenseVo);
		}
		else {
			return baseResult;
		}
	}

	/**
	 * 解析护照
	 * @param baseResult
	 * @return 结果
	 */
	public static BaseResult analyzePassport(BaseResult baseResult) {
		if (baseResult.isSuccess()) {
			JSONObject json = (JSONObject) baseResult.getData();
			JSONObject wordsResult = recombinationJSON(json.getJSONObject(WORDS_RESULT));
			PassportVo passportVo = JSON.parseObject(wordsResult.toString(), PassportVo.class);
			return BaseResult.ok(passportVo);
		}
		else {
			return baseResult;
		}

	}

	/**
	 * 解析社保卡
	 * @param baseResult
	 * @return 结果
	 */
	public static BaseResult analyzeSocialSecurityCard(BaseResult baseResult) {
		if (baseResult.isSuccess()) {
			JSONObject json = (JSONObject) baseResult.getData();
			JSONObject wordsResult = recombinationJSON(json.getJSONObject(WORDS_RESULT));
			SocialSecurityCardVo socialSecurityCardVo = JSON.parseObject(wordsResult.toString(),
					SocialSecurityCardVo.class);
			return BaseResult.ok(socialSecurityCardVo);
		}
		else {
			return baseResult;
		}

	}

	/**
	 * 解析社保卡
	 * @param baseResult
	 * @return 结果
	 */
	public static BaseResult analyzeLicensePlate(BaseResult baseResult) {
		if (baseResult.isSuccess()) {
			JSONObject json = (JSONObject) baseResult.getData();
			JSONObject wordsResult = json.getJSONObject(WORDS_RESULT);
			LicensePlateVo licensePlateVo = JSON.parseObject(wordsResult.toString(), LicensePlateVo.class);
			return BaseResult.ok(licensePlateVo);
		}
		else {
			return baseResult;
		}

	}

	/**
	 * 重组WorldResult
	 * @param wordsResult json
	 * @return 结果
	 */
	private static JSONObject recombinationJSON(JSONObject wordsResult) {
		JSONObject json = new JSONObject();
		Set<String> keys = wordsResult.keySet();
		for (String key : keys) {
			json.put(key, wordsResult.getJSONObject(key).getString(WORDS));
		}
		return json;
	}

}
