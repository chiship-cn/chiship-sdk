package cn.chiship.sdk.third.baidu;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.exception.ExceptionUtil;
import cn.chiship.sdk.core.properties.CommonConfigProperties;
import cn.chiship.sdk.core.util.ImageUtil;
import cn.chiship.sdk.third.baidu.singleton.AipFaceClientSingleton;
import cn.chiship.sdk.third.core.model.baidu.BaiDuOcrConfigModel;
import com.baidu.aip.face.AipFace;
import com.baidu.aip.face.FaceVerifyRequest;
import com.baidu.aip.face.MatchRequest;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author lijian
 */
public class BaiDuFaceUtils {

    CommonConfigProperties commonConfigProperties = CommonConfigProperties.getInstance();

    private BaiDuOcrConfigModel baiDuOcrConfigModel;

    private static BaiDuFaceUtils instance;

    private BaiDuFaceUtils() {
    }

    public static synchronized BaiDuFaceUtils getInstance() {
        if (instance == null) {
            instance = new BaiDuFaceUtils();
        }
        return instance;
    }

    public BaiDuFaceUtils config() {
        this.baiDuOcrConfigModel = new BaiDuOcrConfigModel(commonConfigProperties.getValue("third.ai.baidu.apiKey"),
                commonConfigProperties.getValue("third.ai.baidu.secretKey"),
                commonConfigProperties.getValue("third.ai.baidu.appId"));
        return this;
    }

    public BaiDuFaceUtils config(BaiDuOcrConfigModel baiDuOcrConfigModel) {
        this.baiDuOcrConfigModel = baiDuOcrConfigModel;
        return this;
    }

    /**
     * 人脸对比
     *
     * @param image1
     * @param isNetwork1
     * @param image2
     * @param isNetwork2
     * @return 结果
     */
    public BaseResult faceMatch(String image1, Boolean isNetwork1, String image2, Boolean isNetwork2) {

        try {
            AipFace client = AipFaceClientSingleton.getInstance(this.baiDuOcrConfigModel).getAipOcr();
            if (Boolean.TRUE.equals(isNetwork1)) {
                image1 = ImageUtil.getImgBase64ByUrl(image1);
            } else {
                image1 = ImageUtil.getImgStrByFile(image1);
            }
            if (Boolean.TRUE.equals(isNetwork2)) {
                image2 = ImageUtil.getImgBase64ByUrl(image2);
            } else {
                image2 = ImageUtil.getImgStrByFile(image2);
            }
            MatchRequest req1 = new MatchRequest(image1, "BASE64");
            MatchRequest req2 = new MatchRequest(image2, "BASE64");
            ArrayList<MatchRequest> requests = new ArrayList<>();
            requests.add(req1);
            requests.add(req2);
            JSONObject res = client.match(requests);
            BaseResult baseResult = BaiDuAiConstant.analysisJson(res);
            if (baseResult.isSuccess()) {

                com.alibaba.fastjson.JSONObject data = com.alibaba.fastjson.JSON
                        .parseObject(baseResult.getData().toString());
                baseResult.setData(data.getJSONObject("result").getString("score"));
            }
            return baseResult;
        } catch (Exception e) {
            return ExceptionUtil.formatException(e);
        }
    }

    /**
     * 在线活体检测
     *
     * @param image
     * @param isNetwork
     * @return 结果
     */
    public BaseResult faceVerify(String image, Boolean isNetwork) {
        AipFace client = AipFaceClientSingleton.getInstance(this.baiDuOcrConfigModel).getAipOcr();

        try {
            if (Boolean.TRUE.equals(isNetwork)) {
                image = ImageUtil.getImgBase64ByUrl(image);
            } else {
                image = ImageUtil.getImgStrByFile(image);
            }
            FaceVerifyRequest req = new FaceVerifyRequest(image, "BASE64");
            ArrayList<FaceVerifyRequest> list = new ArrayList<>();
            list.add(req);
            JSONObject res = client.faceverify(list);
            BaseResult baseResult = BaiDuAiConstant.analysisJson(res);
            if (baseResult.isSuccess()) {
                com.alibaba.fastjson.JSONObject data = com.alibaba.fastjson.JSON
                        .parseObject(baseResult.getData().toString());
                baseResult.setData(data.getJSONObject("result").getString("face_liveness"));
            }
            return baseResult;
        } catch (Exception e) {
            return ExceptionUtil.formatException(e);
        }
    }

}
