package cn.chiship.sdk.third.baidu.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 银行卡解析模型
 *
 * @author lj
 */
public class BankCardVo {

	/**
	 * 银行卡卡号
	 */
	private String cardNumber;

	/**
	 * 有效期
	 */
	private String validDate;

	/**
	 * 银行卡类型，0：不能识别; 1：借记卡; 2：贷记卡（原信用卡大部分为贷记卡）; 3：准贷记卡; 4：预付费卡
	 */
	private String cardType;

	/**
	 * 银行名，不能识别时为空
	 */
	private String bankName;

	/**
	 * 持卡人姓名，不能识别时为空
	 */
	private String holderName;

	public String getCardNumber() {
		return cardNumber;
	}

	@JSONField(name = "bank_card_number")
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getValidDate() {
		return validDate;
	}

	@JSONField(name = "valid_date")
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public String getCardType() {
		return cardType;
	}

	@JSONField(name = "bank_card_type")
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getBankName() {
		return bankName;
	}

	@JSONField(name = "bank_name")
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getHolderName() {
		return holderName;
	}

	@JSONField(name = "holder_name")
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

}
