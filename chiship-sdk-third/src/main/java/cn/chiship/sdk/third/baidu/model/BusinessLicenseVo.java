package cn.chiship.sdk.third.baidu.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 营业执照解析模型
 *
 * @author lijian
 */
public class BusinessLicenseVo {

	/**
	 * 单位名称
	 */
	private String companyName;

	/**
	 * 社会信用代码
	 */
	private String creditCode;

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 地址
	 */
	private String address;

	/**
	 * 有效期
	 */
	private String termValidity;

	/**
	 * 法人
	 */
	private String legalPerson;

	/**
	 * 证件编号
	 */
	private String certificateNo;

	/**
	 * 注册资本
	 */
	private String registeredCapital;

	/**
	 * 实收资本
	 */
	private String paidCapital;

	/**
	 * 经营范围
	 */
	private String nature;

	/**
	 * 组成形式
	 */
	private String composition;

	/**
	 * 成立日期
	 */
	private String incorporationDate;

	/**
	 * 核准日期
	 */
	private String approvalDate;

	/**
	 * 登记机关
	 */
	private String registrationAuthority;

	/**
	 * 税务登记号
	 */
	private String taxRegistrationNo;

	public String getCompanyName() {
		return companyName;
	}

	@JSONField(name = "单位名称")
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCreditCode() {
		return creditCode;
	}

	@JSONField(name = "社会信用代码")
	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}

	public String getTermValidity() {
		return termValidity;
	}

	public String getType() {
		return type;
	}

	@JSONField(name = "类型")
	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	@JSONField(name = "地址")
	public void setAddress(String address) {
		this.address = address;
	}

	@JSONField(name = "有效期")
	public void setTermValidity(String termValidity) {
		this.termValidity = termValidity;
	}

	public String getLegalPerson() {
		return legalPerson;
	}

	@JSONField(name = "法人")
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}

	public String getCertificateNo() {
		return certificateNo;
	}

	@JSONField(name = "证件编号")
	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getNature() {
		return nature;
	}

	public String getRegisteredCapital() {
		return registeredCapital;
	}

	@JSONField(name = "注册资本")
	public void setRegisteredCapital(String registeredCapital) {
		this.registeredCapital = registeredCapital;
	}

	public String getPaidCapital() {
		return paidCapital;
	}

	@JSONField(name = "实收资本")
	public void setPaidCapital(String paidCapital) {
		this.paidCapital = paidCapital;
	}

	@JSONField(name = "经营范围")
	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getComposition() {
		return composition;
	}

	@JSONField(name = "组成形式")
	public void setComposition(String composition) {
		this.composition = composition;
	}

	public String getIncorporationDate() {
		return incorporationDate;
	}

	@JSONField(name = "成立日期")
	public void setIncorporationDate(String incorporationDate) {
		this.incorporationDate = incorporationDate;
	}

	public String getApprovalDate() {
		return approvalDate;
	}

	@JSONField(name = "核准日期")
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getRegistrationAuthority() {
		return registrationAuthority;
	}

	@JSONField(name = "登记机关")
	public void setRegistrationAuthority(String registrationAuthority) {
		this.registrationAuthority = registrationAuthority;
	}

	public String getTaxRegistrationNo() {
		return taxRegistrationNo;
	}

	@JSONField(name = "税务登记号")
	public void setTaxRegistrationNo(String taxRegistrationNo) {
		this.taxRegistrationNo = taxRegistrationNo;
	}

}
