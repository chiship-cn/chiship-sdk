package cn.chiship.sdk.third.baidu.model;

import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.annotation.JSONField;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 身份证解析模型
 *
 * @author lj
 */
public class IdCardVo {

	private static final String STRING_CQ = "长期";

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

	/**
	 * 真实姓名
	 */
	private String realName;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 民族
	 */
	private String nation;

	/**
	 * 生日
	 */
	private String birthday;

	/**
	 * 身份证号码
	 */
	private String idNumber;

	/**
	 * 地址
	 */
	private String address;

	/**
	 * 签发机关
	 */
	private String issueOffice;

	/**
	 * 签发日期
	 */
	private String issueDate;

	/**
	 * 失效日期
	 */
	private String expirationDate;

	public String getRealName() {
		return realName;
	}

	@JSONField(name = "姓名")
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getSex() {
		return sex;
	}

	@JSONField(name = "性别")
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNation() {
		return nation;
	}

	@JSONField(name = "民族")
	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getBirthday() throws ParseException {
		if (StringUtil.isNullOrEmpty(birthday)) {
			return birthday;
		}
		return DateUtils.dateTime(sdf.parse(birthday));
	}

	@JSONField(name = "出生")
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getIdNumber() {
		return idNumber;
	}

	@JSONField(name = "公民身份号码")
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getAddress() {
		return address;
	}

	@JSONField(name = "住址")
	public void setAddress(String address) {
		this.address = address;
	}

	public String getIssueOffice() {
		return issueOffice;
	}

	@JSONField(name = "签发机关")
	public void setIssueOffice(String issueOffice) {
		this.issueOffice = issueOffice;
	}

	public String getIssueDate() throws ParseException {
		if (StringUtil.isNullOrEmpty(issueDate)) {
			return issueDate;
		}
		return DateUtils.dateTime(sdf.parse(issueDate));
	}

	@JSONField(name = "签发日期")
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getExpirationDate() throws ParseException {
		if (StringUtil.isNullOrEmpty(expirationDate)) {
			return expirationDate;
		}
		if (STRING_CQ.equals(expirationDate)) {
			return expirationDate;
		}
		return DateUtils.dateTime(sdf.parse(expirationDate));
	}

	@JSONField(name = "失效日期")
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

}
