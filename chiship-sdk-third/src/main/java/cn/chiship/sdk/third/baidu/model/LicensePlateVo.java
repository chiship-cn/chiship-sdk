package cn.chiship.sdk.third.baidu.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 车牌解析模型
 *
 * @author lj
 */
public class LicensePlateVo {

	private String number;

	/**
	 * 车牌颜色 blue、green、yellow、white
	 */
	private String color;

	public String getNumber() {
		return number;
	}

	@JSONField(name = "number")
	public void setNumber(String number) {
		this.number = number;
	}

	public String getColor() {
		switch (color) {
		case "blue":
			return "蓝牌";
		case "green":
			return "绿牌";
		case "yellow":
			return "黄牌";
		case "white":
			return "白牌";
		default:
			return color;
		}
	}

	@JSONField(name = "color")
	public void setColor(String color) {
		this.color = color;
	}

}
