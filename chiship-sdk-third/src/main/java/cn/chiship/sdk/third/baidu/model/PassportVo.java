package cn.chiship.sdk.third.baidu.model;

import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.annotation.JSONField;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 护照解析模型
 *
 * @author lj
 */
public class PassportVo {

	private static final String STRING_CQ = "长期";

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

	/**
	 * 真实姓名
	 */
	private String realName;

	/**
	 * 真实姓名拼音
	 */
	private String realNamePinYin;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 生日
	 */
	private String birthday;

	/**
	 * 出生地
	 */
	private String birthplace;

	/**
	 * 国家码
	 */
	private String countryCode;

	/**
	 * 国籍
	 */
	private String nationality;

	/**
	 * 护照号
	 */
	private String passportNo;

	/**
	 * 护照签发地
	 */
	private String passportIssuingPlace;

	/**
	 * 护照类型
	 */
	private String passportType;

	/**
	 * 有效期至
	 */
	private String validUntil;

	/**
	 * 签发机关
	 */
	private String issueOffice;

	/**
	 * 签发日期
	 */
	private String issueDate;

	public String getRealName() {
		return realName;
	}

	@JSONField(name = "姓名")
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getRealNamePinYin() {
		return realNamePinYin;
	}

	@JSONField(name = "姓名拼音")
	public void setRealNamePinYin(String realNamePinYin) {
		this.realNamePinYin = realNamePinYin;
	}

	public String getSex() {
		return sex;
	}

	@JSONField(name = "性别")
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() throws ParseException {
		if (StringUtil.isNullOrEmpty(birthday)) {
			return birthday;
		}
		return DateUtils.dateTime(sdf.parse(birthday));
	}

	@JSONField(name = "生日")
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBirthplace() {
		return birthplace;
	}

	@JSONField(name = "出生地点")
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}

	public String getCountryCode() {
		return countryCode;
	}

	@JSONField(name = "国家码")
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNationality() {
		return nationality;
	}

	@JSONField(name = "国籍")
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPassportNo() {
		return passportNo;
	}

	@JSONField(name = "护照号码")
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportIssuingPlace() {
		return passportIssuingPlace;
	}

	@JSONField(name = "护照签发地点")
	public void setPassportIssuingPlace(String passportIssuingPlace) {
		this.passportIssuingPlace = passportIssuingPlace;
	}

	public String getPassportType() {
		return passportType;
	}

	@JSONField(name = "护照类型")
	public void setPassportType(String passportType) {
		this.passportType = passportType;
	}

	public String getValidUntil() throws ParseException {
		if (StringUtil.isNullOrEmpty(validUntil)) {
			return validUntil;
		}
		if (STRING_CQ.equals(validUntil)) {
			return validUntil;
		}
		return DateUtils.dateTime(sdf.parse(validUntil));
	}

	@JSONField(name = "有效期至")
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}

	public String getIssueOffice() {
		return issueOffice;
	}

	@JSONField(name = "签发机关")
	public void setIssueOffice(String issueOffice) {
		this.issueOffice = issueOffice;
	}

	public String getIssueDate() {
		return issueDate;
	}

	@JSONField(name = "签证日期")
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

}
