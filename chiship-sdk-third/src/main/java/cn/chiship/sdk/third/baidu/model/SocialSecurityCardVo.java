package cn.chiship.sdk.third.baidu.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 社保卡解析模型
 *
 * @author lj
 */
public class SocialSecurityCardVo {

	/**
	 * 真实姓名
	 */
	private String realName;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 生日
	 */
	private String birthday;

	/**
	 * 银行卡号
	 */
	private String bankCardNumber;

	/**
	 * 卡号
	 */
	private String cardNumber;

	/**
	 * 社会保障卡号
	 */
	private String socialSecurityNumber;

	/**
	 * 签发日期
	 */
	private String issueDate;

	/**
	 * 失效日期
	 */
	private String expirationDate;

	public String getRealName() {
		return realName;
	}

	@JSONField(name = "name")
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getSex() {
		return sex;
	}

	@JSONField(name = "sex")
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	@JSONField(name = "birth_date")
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBankCardNumber() {
		return bankCardNumber;
	}

	@JSONField(name = "bank_card_number")
	public void setBankCardNumber(String bankCardNumber) {
		this.bankCardNumber = bankCardNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	@JSONField(name = "card_number")
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	@JSONField(name = "social_security_number")
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getIssueDate() {
		return issueDate;
	}

	@JSONField(name = "issue_date")
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	@JSONField(name = "expiry_date")
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

}
