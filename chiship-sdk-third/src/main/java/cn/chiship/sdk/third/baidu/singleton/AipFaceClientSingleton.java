package cn.chiship.sdk.third.baidu.singleton;

import cn.chiship.sdk.third.core.model.baidu.BaiDuOcrConfigModel;
import com.baidu.aip.face.AipFace;

/**
 * @author lijian AipOCR单利模式
 */
public class AipFaceClientSingleton {

    private static AipFace client = null;

    private static AipFaceClientSingleton instance;

    private AipFaceClientSingleton() {
    }

    public static synchronized AipFaceClientSingleton getInstance(BaiDuOcrConfigModel baiDuOcrConfigModel) {

        if (instance == null) {
            instance = new AipFaceClientSingleton();
            client = new AipFace(baiDuOcrConfigModel.getAppId(), baiDuOcrConfigModel.getAppKey(),
                    baiDuOcrConfigModel.getAppSecret());
            client.setConnectionTimeoutInMillis(2000);
            client.setSocketTimeoutInMillis(60000);
        }
        return instance;

    }

    public synchronized AipFace getAipOcr() {

        return client;

    }

}
