package cn.chiship.sdk.third.baidu.singleton;

import cn.chiship.sdk.third.core.model.baidu.BaiDuOcrConfigModel;
import com.baidu.aip.ocr.AipOcr;

/**
 * @author lijian AipOCR单利模式
 */
public class AipOcrClientSingleton {

    private static AipOcr client = null;

    private static AipOcrClientSingleton instance;

    private AipOcrClientSingleton() {
    }

    public static synchronized AipOcrClientSingleton getInstance(BaiDuOcrConfigModel baiDuOcrConfigModel) {

        if (instance == null) {
            instance = new AipOcrClientSingleton();
            client = new AipOcr(baiDuOcrConfigModel.getAppId(), baiDuOcrConfigModel.getAppKey(),
                    baiDuOcrConfigModel.getAppSecret());
            client.setConnectionTimeoutInMillis(2000);
            client.setSocketTimeoutInMillis(60000);
        }
        return instance;

    }

    public synchronized AipOcr getAipOcr() {

        return client;

    }

}
