package cn.chiship.sdk.third.config;

import cn.chiship.sdk.third.properties.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lijian
 */
@Configuration
@EnableConfigurationProperties(value = {
        ChishipWxPubProperties.class,
        ChishipWxMiniProgramProperties.class,
        ChishipWxWorkProperties.class,
        ChishipDingTalkProperties.class,
        ChishipZfbMpProperties.class,
        ChishipYsProperties.class})
public class ThirdPropertiesConfig {

    @Bean
    public ChishipWxPubProperties chishipWxPubProperties() {
        return new ChishipWxPubProperties();
    }

    @Bean
    public ChishipWxMiniProgramProperties chishipWxMiniProgramProperties() {
        return new ChishipWxMiniProgramProperties();
    }

    @Bean
    public ChishipWxWorkProperties chishipWxWorkProperties() {
        return new ChishipWxWorkProperties();
    }

    @Bean
    public ChishipDingTalkProperties chishipDingTalkProperties() {
        return new ChishipDingTalkProperties();
    }

    @Bean
    public ChishipZfbMpProperties chishipZfbMpProperties() {
        return new ChishipZfbMpProperties();
    }

    @Bean
    public ChishipYsProperties chishipYsProperties() {
        return new ChishipYsProperties();
    }

}
