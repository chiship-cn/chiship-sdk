package cn.chiship.sdk.third.core.common;

import cn.chiship.sdk.core.base.constants.BaseConstants;

/**
 * @author lijian
 */
public class ThirdConstants extends BaseConstants {

	public static final String PROPERTIES_FILE_NAME = "chiship-third";

	public static final String STRING_OK = "OK";

	public static final String ERROR_EXIST_TIP_1 = "兄弟,请确保'chiship-third'文件存在!";

	/**
	 * 缓存微信公众号AccessToken
	 */
	public static final String REDIS_WEIXIN_PUB_ACCESS_TOKEN = "THIRD:WX_PUB_ACCESS_TOKEN";

	/**
	 * 缓存百度AIPAccessToken
	 */
	public static final String REDIS_BAI_DU_AIP_ACCESS_TOKEN = "THIRD:BAI_DU_AIP_ACCESS_TOKEN";

	/**
	 * 缓存微信小程序AccessToken
	 */
	public static final String REDIS_WEIXIN_MINI_PROGRAM_ACCESS_TOKEN = "THIRD:WX_MINI_PROGRAM_ACCESS_TOKEN";

	/**
	 * 缓存企业微信AccessToken
	 */
	public static final String REDIS_WEIXIN_WORK_ACCESS_TOKEN = "THIRD:WX_WORK_ACCESS_TOKEN";

	/**
	 * 缓存钉钉accessToken
	 */
	public static final String REDIS_DING_TALK_ACCESS_TOKEN = "THIRD:DING_TALK_ACCESS_TOKEN";

	/**
	 * 缓存萤石accessToken
	 */
	public static final String REDIS_YS_ACCESS_TOKEN = "THIRD:DING_TALK_ACCESS_TOKEN";

}
