package cn.chiship.sdk.third.core.common;

/**
 * @author lijian
 * 三方授权渠道
 */
public enum ThirdOauthChannelEnum {

    /**
     * 企业微信Web
     */
    OAUTH_CHANNEL_WX_WORK_PAGE(1, "企业微信Web"),


    /**
     * 钉钉
     */
    OAUTH_CHANNEL_DING_TALK(2, "钉钉"),

    /**
     * 微信公众号
     */
    OAUTH_CHANNEL_WX_PUB(3, "微信公众号"),


    /**
     * 企业微信网页
     */
    OAUTH_CHANNEL_WX_WORK_WEB(4, "企业微信网页"),

    ;

    private Integer type;

    private String message;

    ThirdOauthChannelEnum(Integer type, String message) {
        this.type = type;
        this.message = message;
    }


    public Integer getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
