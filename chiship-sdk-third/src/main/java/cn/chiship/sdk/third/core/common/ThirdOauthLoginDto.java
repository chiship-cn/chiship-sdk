package cn.chiship.sdk.third.core.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 微信认证表单
 *
 * @author lijian
 */
@ApiModel(value = "三方授权认证表单")
public class ThirdOauthLoginDto {

    @ApiModelProperty(value = "AppId(预留)")
    private String appId;

    @ApiModelProperty(value = "认证授权渠道", required = true)
    private ThirdOauthChannelEnum oauthChannel;

    @ApiModelProperty(value = "认证成功，跳转路径", required = true)
    private String callback;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public ThirdOauthChannelEnum getOauthChannel() {
        return oauthChannel;
    }

    public void setOauthChannel(ThirdOauthChannelEnum oauthChannel) {
        this.oauthChannel = oauthChannel;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

}
