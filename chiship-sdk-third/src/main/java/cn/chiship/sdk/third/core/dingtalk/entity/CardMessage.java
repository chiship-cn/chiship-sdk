package cn.chiship.sdk.third.core.dingtalk.entity;

import cn.chiship.sdk.core.util.DateUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * 卡片消息
 *
 * @author lijian
 */
public class CardMessage {

	/**
	 * 是否整体跳转
	 */
	private Boolean isWhole;

	/**
	 * 透出到会话列表和通知的文案。
	 */
	private String title;

	/**
	 * 消息内容，支持markdown，语法参考标准markdown语法。建议1000个字符以内。
	 */
	private String markdown;

	/**
	 * 使用整体跳转ActionCard样式时的标题。必须与single_url同时设置，最长20个字符。
	 */
	private String singleTile;

	/**
	 * 消息点击链接地址，当发送消息为小程序时支持小程序跳转链接，最长500个字符。
	 */
	private String singleUrl;

	/**
	 * 按钮是否横向
	 */
	private Boolean isRow;

	/**
	 * 按钮合集
	 */
	private Map<String, String> btnMaps;

	/**
	 * 整体跳转构造器
	 * @param title
	 * @param markdown
	 * @param singleTile
	 * @param singleUrl
	 */
	public CardMessage(String title, String markdown, String singleTile, String singleUrl) {
		this.isWhole = true;
		this.title = title;
		this.markdown = markdown;
		this.singleTile = singleTile;
		this.singleUrl = singleUrl;
	}

	/**
	 * 独立跳转构造器
	 * @param title
	 * @param markdown
	 * @param isRow
	 * @param btnMaps
	 */
	public CardMessage(String title, String markdown, Boolean isRow, Map<String, String> btnMaps) {
		this.isWhole = false;
		this.title = title;
		this.markdown = markdown;
		this.isRow = isRow;
		this.btnMaps = btnMaps;
	}

	public JSONObject toJSON() {
		return toJSON(false);
	}

	public JSONObject toJSON(Boolean time) {
		String message = getMarkdown();
		if (Boolean.TRUE.equals(time)) {
			message += "(" + DateUtils.getTime() + "发)";
		}

		JSONObject cardJson = new JSONObject();
		cardJson.put("title", getTitle());
		cardJson.put("markdown", message);
		if (Boolean.TRUE.equals(getWhole())) {
			cardJson.put("single_title", singleTile);
			cardJson.put("single_url", singleUrl);
		}
		else {
			cardJson.put("btn_orientation", Boolean.TRUE.equals(getRow()) ? 1 : 0);
			JSONArray btnArray = new JSONArray();
			for (Map.Entry<String, String> entry : getBtnMaps().entrySet()) {
				JSONObject btnJSON = new JSONObject();
				btnJSON.put("title", entry.getKey());
				btnJSON.put("action_url", entry.getValue());
				btnArray.add(btnJSON);
			}
			cardJson.put("btn_json_list", btnArray);
		}
		JSONObject json = new JSONObject();
		json.put("msgtype", "action_card");
		json.put("action_card", cardJson);
		return json;
	}

	public Boolean getWhole() {
		return isWhole;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMarkdown() {
		return markdown;
	}

	public void setMarkdown(String markdown) {
		this.markdown = markdown;
	}

	public String getSingleTile() {
		return singleTile;
	}

	public void setSingleTile(String singleTile) {
		this.singleTile = singleTile;
	}

	public String getSingleUrl() {
		return singleUrl;
	}

	public void setSingleUrl(String singleUrl) {
		this.singleUrl = singleUrl;
	}

	public Boolean getRow() {
		return isRow;
	}

	public void setRow(Boolean row) {
		isRow = row;
	}

	public Map<String, String> getBtnMaps() {
		return btnMaps;
	}

	public void setBtnMaps(Map<String, String> btnMaps) {
		this.btnMaps = btnMaps;
	}

}
