package cn.chiship.sdk.third.core.dingtalk.entity;

import cn.chiship.sdk.core.util.DateUtils;
import com.alibaba.fastjson.JSONObject;

/**
 * Markdown消息
 *
 * @author lijian
 */
public class MarkdownMessage {

	/**
	 * 首屏会话透出的展示内容
	 */
	private String title;

	/**
	 * markdown格式的消息，最大不超过5000字符。
	 */
	private String text;

	public MarkdownMessage(String title, String text) {
		this.title = title;
		this.text = text;
	}

	public JSONObject toJSON() {
		return toJSON(false);
	}

	public JSONObject toJSON(Boolean time) {
		String message = getText();
		if (Boolean.TRUE.equals(time)) {
			message += "(" + DateUtils.getTime() + "发)";
		}

		JSONObject markdownJson = new JSONObject();
		markdownJson.put("title", getTitle());
		markdownJson.put("text", message);
		JSONObject json = new JSONObject();
		json.put("msgtype", "markdown");
		json.put("markdown", markdownJson);
		return json;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
