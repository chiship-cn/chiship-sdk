package cn.chiship.sdk.third.core.dingtalk.entity;

import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * @author lijian 钉钉OA消息
 */
public class OaMessage {

	/**
	 * 消息跳转链接 https://open.dingtalk.com/document/isvapp/applink-structure
	 * https://applink.dingtalk.com/action/open_mini_app?type=2&miniAppId=&corpId=&agentId=&pVersion=1&packageType=1;
	 * eapp://pages/my/index?param1=aa&param2=bb
	 */
	private String messageUrl;

	/**
	 * 头部标题
	 */
	private String headTitle;

	/**
	 * 正文标题
	 */
	private String bodyTitle;

	/**
	 * 正文内容
	 */
	private String bodyContent;

	/**
	 * 作者
	 */
	private String author;

	/**
	 * 状态
	 */
	private String status;

	/**
	 * 状态颜色 如ff0000
	 */
	private String statusColor;

	/**
	 * 表单
	 */
	private Map<String, String> forms;

	/**
	 * 单行富文本文字
	 */
	private String richNumber;

	/**
	 * 单行富文本单位
	 */
	private String richUnit;

	/**
	 * 附件数量
	 */
	private Integer fileCount;

	public OaMessage(String messageUrl, String headTitle, String bodyTitle, String bodyContent, String author) {
		this.messageUrl = messageUrl;
		this.headTitle = headTitle;
		this.bodyTitle = bodyTitle;
		this.bodyContent = bodyContent;
		this.author = author;
	}

	public OaMessage(String messageUrl, String headTitle, String bodyTitle, String bodyContent, String author,
			Map<String, String> forms, String richNumber, String richUnit) {
		this.messageUrl = messageUrl;
		this.headTitle = headTitle;
		this.bodyTitle = bodyTitle;
		this.bodyContent = bodyContent;
		this.author = author;
		this.forms = forms;
		this.richNumber = richNumber;
		this.richUnit = richUnit;
	}

	public OaMessage(String messageUrl, String headTitle, String bodyTitle, String bodyContent, String author,
			String status, String statusColor, Map<String, String> forms, String richNumber, String richUnit) {
		this.messageUrl = messageUrl;
		this.headTitle = headTitle;
		this.bodyTitle = bodyTitle;
		this.bodyContent = bodyContent;
		this.author = author;
		this.status = status;
		this.statusColor = statusColor;
		this.forms = forms;
		this.richNumber = richNumber;
		this.richUnit = richUnit;
	}

	public JSONObject toJSON() {
		return toJSON(false);
	}

	public JSONObject toJSON(Boolean time) {
		String message = getBodyContent();
		if (Boolean.TRUE.equals(time)) {
			message += "(" + DateUtils.getTime() + "发)";
		}

		JSONObject oaJson = new JSONObject();

		// 头部
		JSONObject headJson = new JSONObject();
		headJson.put("bgcolor", "FFBBBBBB");
		headJson.put("text", getHeadTitle());

		// 状态栏
		JSONObject statusBarJson = new JSONObject();
		statusBarJson.put("status_value", getStatus());
		statusBarJson.put("status_bg", getStatusColor());

		// 消息体
		JSONObject bodyJson = new JSONObject();
		bodyJson.put("title", getBodyTitle());
		bodyJson.put("content", message);
		if (!StringUtil.isNull(getFileCount())) {
			bodyJson.put("file_count", getFileCount());
		}
		bodyJson.put("author", getAuthor());

		// 表单
		if (!StringUtil.isNull(getForms())) {
			JSONArray formArray = new JSONArray();
			for (Map.Entry<String, String> entry : getForms().entrySet()) {
				JSONObject formJSON = new JSONObject();
				formJSON.put("key", entry.getKey());
				formJSON.put("value", entry.getValue());
				formArray.add(formJSON);
			}
			bodyJson.put("form", formArray);
		}

		// 单行富文本消息
		JSONObject richJson = new JSONObject();
		richJson.put("num", getRichNumber());
		richJson.put("unit", getRichUnit());
		if (!StringUtil.isNullOrEmpty(getRichNumber())) {
			bodyJson.put("rich", richJson);
		}

		// 消息点击链接地址，当发送消息为小程序时支持小程序跳转链接
		oaJson.put("message_url", getMessageUrl());
		oaJson.put("pc_message_url", getMessageUrl());
		oaJson.put("head", headJson);
		if (!StringUtil.isNullOrEmpty(getStatus())) {
			oaJson.put("status_bar", statusBarJson);
		}
		oaJson.put("body", bodyJson);

		JSONObject json = new JSONObject();
		json.put("msgtype", "oa");
		json.put("oa", oaJson);
		return json;
	}

	public String getMessageUrl() {
		return messageUrl;
	}

	public void setMessageUrl(String messageUrl) {
		this.messageUrl = messageUrl;
	}

	public String getHeadTitle() {
		return headTitle;
	}

	public void setHeadTitle(String headTitle) {
		this.headTitle = headTitle;
	}

	public String getBodyTitle() {
		return bodyTitle;
	}

	public void setBodyTitle(String bodyTitle) {
		this.bodyTitle = bodyTitle;
	}

	public String getBodyContent() {
		return bodyContent;
	}

	public void setBodyContent(String bodyContent) {
		this.bodyContent = bodyContent;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusColor() {
		if (StringUtil.isNullOrEmpty(statusColor)) {
			return "0xFFF65E5E";
		}
		return "0xFF" + statusColor;
	}

	public void setStatusColor(String statusColor) {
		this.statusColor = statusColor;
	}

	public Integer getFileCount() {
		return fileCount;
	}

	public void setFileCount(Integer fileCount) {
		this.fileCount = fileCount;
	}

	public Map<String, String> getForms() {
		return forms;
	}

	public void setForms(Map<String, String> forms) {
		this.forms = forms;
	}

	public String getRichNumber() {
		return richNumber;
	}

	public void setRichNumber(String richNumber) {
		this.richNumber = richNumber;
	}

	public String getRichUnit() {
		if (StringUtil.isNullOrEmpty(richUnit)) {
			return "配置[richUnit]";
		}
		return richUnit;
	}

	public void setRichUnit(String richUnit) {
		this.richUnit = richUnit;
	}

}
