package cn.chiship.sdk.third.core.dingtalk.entity;

import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import com.alibaba.fastjson.JSONObject;

/**
 * @author lijian 钉钉文本消息
 */
public class TextMessage {

	/**
	 * 消息内容，建议500字符以内。
	 */
	private String content;

	public TextMessage(String content) {
		this.content = content;
	}

	public JSONObject toJSON() {
		return toJSON(false);
	}

	public JSONObject toJSON(Boolean time) {
		String message = getContent();
		if (Boolean.TRUE.equals(time)) {
			message += "(" + DateUtils.getTime() + "发)";
		}

		JSONObject textJson = new JSONObject();
		textJson.put("content", message);

		JSONObject json = new JSONObject();
		json.put("msgtype", "text");
		json.put("text", textJson);
		return json;
	}

	public String getContent() {
		if (StringUtil.isNullOrEmpty(content)) {
			return "内容为空,采用默认内容";
		}
		if (content.length() > 500) {
			System.out.println("内容超过500字符，自动截取处理！");
			content = content.substring(0, 500);
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
