package cn.chiship.sdk.third.core.dingtalk.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 钉钉基本用户信息
 *
 * @author lijian
 */
public class DingTalkBaseUserInfoVo {
    /**
     * 用户ID
     */
    @JSONField(name = "userid")
    private String userId;

    /**
     * 真实姓名
     */
    @JSONField(name = "name")
    private String realName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
