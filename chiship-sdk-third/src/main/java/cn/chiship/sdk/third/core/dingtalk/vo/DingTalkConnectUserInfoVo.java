package cn.chiship.sdk.third.core.dingtalk.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 钉钉用户通讯录个人信息
 *
 * @author lijian
 */
public class DingTalkConnectUserInfoVo {
    /**
     * 真实姓名
     */
    @JSONField(name = "nick")
    private String realName;

    /**
     * 手机号
     */
    @JSONField(name = "mobile")
    private String mobile;

    /**
     * 邮箱
     */
    @JSONField(name = "email")
    private String email;

    /**
     * 唯一unionId
     */
    @JSONField(name = "unionId")
    private String unionId;

    /**
     * 头像
     */
    @JSONField(name = "avatarUrl")
    private String avatar;

    /**
     * 用户的openId
     */
    @JSONField(name = "openId")
    private String openId;


    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
