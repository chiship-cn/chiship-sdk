package cn.chiship.sdk.third.core.dingtalk.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 钉钉用户访问凭证
 *
 * @author lijian
 */
public class DingTalkUserAccessTokenVo {
    /**
     * 所选企业corpId
     */
    @JSONField(name = "corpId")
    private String corpId;

    /**
     * 超时时间，单位秒。
     */
    @JSONField(name = "expireIn")
    private Long expireIn;

    /**
     * 生成的accessToken
     */
    @JSONField(name = "accessToken")
    private String accessToken;

    /**
     * 生成的refresh_token。可以使用此刷新token，定期的获取用户的accessToken
     */
    @JSONField(name = "refreshToken")
    private String refreshToken;


    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public Long getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(Long expireIn) {
        this.expireIn = expireIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
