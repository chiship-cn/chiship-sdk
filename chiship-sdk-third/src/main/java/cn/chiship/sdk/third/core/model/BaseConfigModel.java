package cn.chiship.sdk.third.core.model;

/**
 * @author lijian
 */
public class BaseConfigModel {

	private String appKey;

	private String appSecret;

	public BaseConfigModel() {
	}

	public BaseConfigModel(String appKey, String appSecret) {
		this.appKey = appKey;
		this.appSecret = appSecret;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

}
