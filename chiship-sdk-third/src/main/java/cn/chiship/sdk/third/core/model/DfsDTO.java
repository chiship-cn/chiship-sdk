package cn.chiship.sdk.third.core.model;

/**
 * 分布是存储传输DTO
 *
 * @author lj
 */
public class DfsDTO {

	/**
	 * 秘钥
	 */
	private String appKey;

	/**
	 * 秘钥
	 */
	private String appSecret;

	/**
	 * 地域节点
	 *
	 * @example oss-cn-hangzhou.aliyuncs.com
	 */
	private String endPort;

	/**
	 * 存储空间
	 *
	 * @example chiship
	 */
	private String buckName;

	/**
	 * 唯一标识
	 *
	 * @example chishipSdk
	 */
	private String root;

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getEndPort() {
		return endPort;
	}

	public void setEndPort(String endPort) {
		this.endPort = endPort;
	}

	public String getBuckName() {
		return buckName;
	}

	public void setBuckName(String buckName) {
		this.buckName = buckName;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

}
