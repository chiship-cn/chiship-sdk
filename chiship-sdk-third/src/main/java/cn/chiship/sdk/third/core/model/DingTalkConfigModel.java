package cn.chiship.sdk.third.core.model;

/**
 * @author lijian 钉钉配置
 */
public class DingTalkConfigModel extends BaseConfigModel {

    /**
     * 应用appId
     */
    private String miniAppId;

    /**
     * 企业corpId
     */
    private String corpId;

    /**
     * 原企业内部应用ID
     */
    private String agentId;

    public DingTalkConfigModel(String appKey, String appSecret) {
        super(appKey, appSecret);
    }

    public DingTalkConfigModel(String appKey, String appSecret, String agentId) {
        super(appKey, appSecret);
        this.agentId = agentId;
    }

    public DingTalkConfigModel(String appKey, String appSecret, String miniAppId, String corpId, String agentId) {
        super(appKey, appSecret);
        this.miniAppId = miniAppId;
        this.corpId = corpId;
        this.agentId = agentId;
    }

    public String getMiniAppId() {
        return miniAppId;
    }

    public void setMiniAppId(String miniAppId) {
        this.miniAppId = miniAppId;
    }

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
