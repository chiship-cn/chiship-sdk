package cn.chiship.sdk.third.core.model;

/**
 * @author lijian
 */
public class EmailConfigModel {

	/**
	 * 要连接的SMTP服务器
	 */
	private String mailHost;

	/**
	 * 要装入session的协议（smtp、pop3、imap、nntp）
	 */
	private String mailTransportProtocol;

	/**
	 * 缺省是false，如果为true，尝试使用AUTH命令认证用户
	 */
	private Boolean mailSmtpAuth;

	/**
	 * 要连接的SMTP服务器的端口号，如果connect没有指明端口号就使用它，缺省值25
	 */
	private String mailSmtpPort;

	/**
	 * 是否开启安全协议 默认false
	 */
	private Boolean mailIsSsl;

	/**
	 * 安全协议端口
	 */
	private String mailSmtpSslPort;

	/**
	 * SMTP的缺省用户名
	 */
	private String mailSendFromAddress;

	/**
	 * SMTP的缺省用户密码。
	 */
	private String mailSendFromAddressPassword;

	public EmailConfigModel() {
	}

	public EmailConfigModel(String mailHost, String mailTransportProtocol, Boolean mailSmtpAuth, String mailSmtpPort,
			Boolean mailIsSsl, String mailSmtpSslPort, String mailSendFromAddress, String mailSendFromAddressPassword) {
		this.mailHost = mailHost;
		this.mailTransportProtocol = mailTransportProtocol;
		this.mailSmtpAuth = mailSmtpAuth;
		this.mailSmtpPort = mailSmtpPort;
		this.mailIsSsl = mailIsSsl;
		this.mailSmtpSslPort = mailSmtpSslPort;
		this.mailSendFromAddress = mailSendFromAddress;
		this.mailSendFromAddressPassword = mailSendFromAddressPassword;
	}

	public String getMailHost() {
		return mailHost;
	}

	public void setMailHost(String mailHost) {
		this.mailHost = mailHost;
	}

	public String getMailTransportProtocol() {
		return mailTransportProtocol;
	}

	public void setMailTransportProtocol(String mailTransportProtocol) {
		this.mailTransportProtocol = mailTransportProtocol;
	}

	public Boolean getMailSmtpAuth() {
		return mailSmtpAuth;
	}

	public void setMailSmtpAuth(Boolean mailSmtpAuth) {
		this.mailSmtpAuth = mailSmtpAuth;
	}

	public String getMailSmtpPort() {
		return mailSmtpPort;
	}

	public void setMailSmtpPort(String mailSmtpPort) {
		this.mailSmtpPort = mailSmtpPort;
	}

	public Boolean getMailIsSsl() {
		return mailIsSsl;
	}

	public void setMailIsSsl(Boolean mailIsSsl) {
		this.mailIsSsl = mailIsSsl;
	}

	public String getMailSmtpSslPort() {
		return mailSmtpSslPort;
	}

	public void setMailSmtpSslPort(String mailSmtpSslPort) {
		this.mailSmtpSslPort = mailSmtpSslPort;
	}

	public String getMailSendFromAddress() {
		return mailSendFromAddress;
	}

	public void setMailSendFromAddress(String mailSendFromAddress) {
		this.mailSendFromAddress = mailSendFromAddress;
	}

	public String getMailSendFromAddressPassword() {
		return mailSendFromAddressPassword;
	}

	public void setMailSendFromAddressPassword(String mailSendFromAddressPassword) {
		this.mailSendFromAddressPassword = mailSendFromAddressPassword;
	}

}
