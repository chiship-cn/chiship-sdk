package cn.chiship.sdk.third.core.model;

/**
 * @author lijian 微信配置
 */
public class WeiXinConfigModel extends BaseConfigModel {

	public WeiXinConfigModel(String appKey, String appSecret) {
		super(appKey, appSecret);
	}

}
