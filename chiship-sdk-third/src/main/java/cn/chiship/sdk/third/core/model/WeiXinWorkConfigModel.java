package cn.chiship.sdk.third.core.model;

/**
 * @author lijian 微信配置
 */
public class WeiXinWorkConfigModel extends BaseConfigModel {

    /**
     * 企业ID
     */
    private String corpId;

    public WeiXinWorkConfigModel(String corpId, String appKey, String appSecret) {
        super(appKey, appSecret);
        this.corpId = corpId;
    }

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }
}
