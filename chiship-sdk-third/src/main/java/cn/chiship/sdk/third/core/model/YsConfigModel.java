package cn.chiship.sdk.third.core.model;

/**
 * @author lijian 微信配置
 */
public class YsConfigModel extends BaseConfigModel {

	public YsConfigModel(String appKey, String appSecret) {
		super(appKey, appSecret);
	}

}
