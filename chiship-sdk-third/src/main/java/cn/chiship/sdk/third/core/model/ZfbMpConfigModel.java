package cn.chiship.sdk.third.core.model;

/**
 * @author lijian 支付宝小程序配置
 */
public class ZfbMpConfigModel {

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 小程序私钥
     */
    private String privateKey;

    /**
     * AES解密Key
     */
    private String aesDecryptKey;

    /**
     * 支付宝公钥
     */
    private String publicKey;

    public ZfbMpConfigModel(String appId, String privateKey, String publicKey,String aesDecryptKey) {
        this.appId = appId;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.aesDecryptKey = aesDecryptKey;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAesDecryptKey() {
        return aesDecryptKey;
    }

    public void setAesDecryptKey(String aesDecryptKey) {
        this.aesDecryptKey = aesDecryptKey;
    }
}
