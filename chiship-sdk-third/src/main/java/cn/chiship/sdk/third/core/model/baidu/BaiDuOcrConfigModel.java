package cn.chiship.sdk.third.core.model.baidu;

import cn.chiship.sdk.third.core.model.BaseConfigModel;

/**
 * @author Administrator
 */
public class BaiDuOcrConfigModel extends BaseConfigModel {

	private String appId;

	public BaiDuOcrConfigModel(String appId) {
		this.appId = appId;
	}

	public BaiDuOcrConfigModel(String accesskeyId, String accesskeySecret, String appId) {
		super(accesskeyId, accesskeySecret);
		this.appId = appId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

}
