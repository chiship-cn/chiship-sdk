package cn.chiship.sdk.third.core.model.tencent;

import cn.chiship.sdk.core.properties.CommonConfigProperties;
import cn.chiship.sdk.third.core.model.BaseConfigModel;

/**
 * @author Administrator
 */
public class TencentCosConfigModel extends BaseConfigModel {

	private String prefix = "third.oss.tencent.";

	CommonConfigProperties commonConfigProperties = CommonConfigProperties.getInstance();

	/**
	 * 地域节点
	 *
	 * @example ap-beijing
	 */
	private String ossEndPort;

	/**
	 * 存储空间
	 *
	 * @example chiship
	 */
	private String buckName;

	/**
	 * 唯一标识
	 *
	 * @example chishipSdk
	 */
	private String root;

	public TencentCosConfigModel() {

	}

	public TencentCosConfigModel(String accesskeyId, String accesskeySecret, String ossEndPort, String buckName,
			String root) {
		super(accesskeyId, accesskeySecret);
		this.ossEndPort = ossEndPort;
		this.buckName = buckName;
		this.root = root;
	}

	@Override
	public String getAppKey() {
		return commonConfigProperties.getValue(prefix + "accessKey", super.getAppKey());
	}

	@Override
	public String getAppSecret() {
		return commonConfigProperties.getValue(prefix + "accessSecret", super.getAppSecret());
	}

	public String getOssEndPort() {
		return commonConfigProperties.getValue(prefix + "endPort", ossEndPort);
	}

	public void setOssEndPort(String ossEndPort) {
		this.ossEndPort = ossEndPort;
	}

	public String getBuckName() {
		return commonConfigProperties.getValue(prefix + "buckName", buckName);
	}

	public void setBuckName(String buckName) {
		this.buckName = buckName;
	}

	public String getRoot() {
		return commonConfigProperties.getValue(prefix + "root", root);
	}

	public void setRoot(String root) {
		this.root = root;
	}

}
