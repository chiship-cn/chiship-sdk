package cn.chiship.sdk.third.core.wx.enums;

/**
 * 微信多语言
 *
 * @author lijian
 */
public enum WeiXinLanguageEnum {

	/**
	 * 简体
	 */
	WX_ZH_CN("zh_CN"),

	/**
	 * 繁体
	 */
	WX_ZH_TW("zh_TW"),

	/**
	 * 英语
	 */
	WX_EN("en");

	/**
	 * 自定义状态码
	 */
	private String lang;

	WeiXinLanguageEnum(String lang) {
		this.lang = lang;
	}

	public String getLang() {
		return lang;
	}

}
