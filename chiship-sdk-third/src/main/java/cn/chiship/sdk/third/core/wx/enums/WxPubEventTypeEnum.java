package cn.chiship.sdk.third.core.wx.enums;

/**
 * @author lijian 公众号事件类型
 */

public enum WxPubEventTypeEnum {

	/**
	 * 订阅
	 */
	WX_PUB_EVENT_SUBSCRIBE("subscribe", "订阅"),
	/**
	 * 取消订阅
	 */
	WX_PUB_EVENT_UNSUBSCRIBE("unsubscribe", "取消订阅"),
	/**
	 * 上报地理位置
	 */
	WX_PUB_EVENT_LOCATION("location", "上报地理位置"),
	/**
	 * 菜单单击
	 */
	WX_PUB_EVENT_CLICK("click", "菜单单击"),
	/**
	 * 小视频消息
	 */
	WX_PUB_EVENT_VIEW("view", "菜单跳转"),
	/**
	 * 用户已关注时的事件推送
	 */
	WX_PUB_EVENT_SCAN("scan", "用户已关注时的事件推送"),;

	private String type;

	private String desc;

	WxPubEventTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}
