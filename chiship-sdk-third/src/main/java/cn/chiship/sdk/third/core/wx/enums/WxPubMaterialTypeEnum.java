package cn.chiship.sdk.third.core.wx.enums;

/**
 * @author lijian 素材类型枚举
 */

public enum WxPubMaterialTypeEnum {

	MATERIAL_TYPE_IMAGE("image", "图片"), MATERIAL_TYPE_VOICE("voice", "语音"), MATERIAL_TYPE_VIDEO("video", "视频"),;

	private String type;

	private String desc;

	WxPubMaterialTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}
