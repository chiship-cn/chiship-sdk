package cn.chiship.sdk.third.core.wx.enums;

/**
 * @author lijian 公众号菜单类型枚举
 */

public enum WxPubMenuTypeEnum {

	/**
	 * 单击菜单
	 */
	WX_PUB_MENU_CLICK("click", "单击菜单"),
	/**
	 * 浏览菜单
	 */
	WX_PUB_MENU_VIEW("view", "浏览菜单"),
	/**
	 * 小程序菜单
	 */
	WX_PUB_MENU_MP("miniprogram", "小程序菜单"),;

	private String type;

	private String desc;

	WxPubMenuTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
