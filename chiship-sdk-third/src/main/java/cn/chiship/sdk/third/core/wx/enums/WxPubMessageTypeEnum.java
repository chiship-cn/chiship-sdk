package cn.chiship.sdk.third.core.wx.enums;

/**
 * @author lijian 公众号消息类型
 */

public enum WxPubMessageTypeEnum {

	/**
	 * 文本消息
	 */
	WX_PUB_MESSAGE_TEXT("text", "文本"),
	/**
	 * 图片消息
	 */
	WX_PUB_MESSAGE_IMAGE("image", "图片"),
	/**
	 * 语音消息
	 */
	WX_PUB_MESSAGE_VOICE("voice", "语音"),
	/**
	 * 视频消息
	 */
	WX_PUB_MESSAGE_VIDEO("video", "视频"),
	/**
	 * 小视频消息
	 */
	WX_PUB_MESSAGE_SHORT_VIDEO("shortvideo", "小视频"),
	/**
	 * 地理位置消息
	 */
	WX_PUB_MESSAGE_LOCATION("location", "地理位置"),
	/**
	 * 链接消息
	 */
	WX_PUB_MESSAGE_LINK("link", "链接"),
	/**
	 * 事件消息
	 */
	WX_PUB_MESSAGE_EVENT("event", "事件"),

	/**
	 * 音乐消息
	 */
	WX_PUB_MESSAGE_MUSIC("music", "音乐"),
	/**
	 * 图文消息
	 */
	WX_PUB_MESSAGE_NEWS("news", "图文"),;

	private String type;

	private String desc;

	WxPubMessageTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}
