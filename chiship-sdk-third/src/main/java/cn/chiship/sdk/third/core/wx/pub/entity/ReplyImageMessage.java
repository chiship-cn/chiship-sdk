package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.core.util.XmlUtil;
import cn.chiship.sdk.third.core.wx.enums.WxPubMessageTypeEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author lijian 回复图片消息
 */
@XStreamAlias("xml")
public class ReplyImageMessage extends WxBaseMessageBean {

	@XStreamAlias("Image")
	public ReplyImageMessageItem replyImageMessageItem;

	/**
	 * 回复消息推荐构造器
	 * @param toUserName
	 * @param fromUserName
	 * @param mediaId
	 */
	public ReplyImageMessage(String toUserName, String fromUserName, String mediaId) {
		super(toUserName, fromUserName, System.currentTimeMillis(),
				WxPubMessageTypeEnum.WX_PUB_MESSAGE_IMAGE.getType());
		this.replyImageMessageItem = new ReplyImageMessageItem(mediaId);
	}

	/**
	 * 实体转XML <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName> <CreateTime>12345678</CreateTime>
	 * <MsgType><![CDATA[image]]></MsgType> <Image>
	 * <MediaId><![CDATA[media_id]]></MediaId> </Image> </xml>
	 * @return String
	 */
	public String toXml() {
		return XmlUtil.toXml(this);
	}

	public ReplyImageMessageItem getReplyImageMessageItem() {
		return replyImageMessageItem;
	}

	public void setReplyImageMessageItem(ReplyImageMessageItem replyImageMessageItem) {
		this.replyImageMessageItem = replyImageMessageItem;
	}

}
