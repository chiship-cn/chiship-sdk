package cn.chiship.sdk.third.core.wx.pub.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复图片消息MediaId
 *
 * @author lijian
 */
class ReplyImageMessageItem {

	/**
	 * 图片消息媒体id，可以调用获取临时素材接口拉取数据。
	 */
	@XStreamAlias("MediaId")
	public String mediaId;

	public ReplyImageMessageItem(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

}
