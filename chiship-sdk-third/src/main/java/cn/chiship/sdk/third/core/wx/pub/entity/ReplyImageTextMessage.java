package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.XmlUtil;
import cn.chiship.sdk.third.core.wx.enums.WxPubMessageTypeEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

/**
 * 回复图文消息
 *
 * @author lijian
 */
@XStreamAlias("xml")
public class ReplyImageTextMessage extends WxBaseMessageBean {

	private static final Integer NUMBER_EIGHT = 8;

	/**
	 * 图文消息个数；当用户发送文本、图片、语音、视频、图文、地理位置这六种消息时，开发者只能回复1条图文消息；其余场景最多可回复8条图文消息
	 */
	@XStreamAlias("ArticleCount")
	public Integer articleCount;

	@XStreamAlias("Articles")
	public List<ReplyImageTextMessageItem> replyImageTextMessageItems;

	/**
	 * 回复消息推荐构造器
	 * @param toUserName
	 * @param fromUserName
	 * @param replyImageTextMessageItems
	 */
	public ReplyImageTextMessage(String toUserName, String fromUserName,
			List<ReplyImageTextMessageItem> replyImageTextMessageItems) {
		super(toUserName, fromUserName, System.currentTimeMillis(), WxPubMessageTypeEnum.WX_PUB_MESSAGE_NEWS.getType());
		this.replyImageTextMessageItems = replyImageTextMessageItems;
		this.articleCount = this.replyImageTextMessageItems.size();
		if (this.articleCount > NUMBER_EIGHT) {
			throw new BusinessException("最多回复8条图文消息");
		}
	}

	/**
	 * 实体转XML <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName> <CreateTime>12345678</CreateTime>
	 * <MsgType><![CDATA[news]]></MsgType> <ArticleCount>1</ArticleCount> <Articles>
	 * <item> <Title><![CDATA[title1]]></Title>
	 * <Description><![CDATA[description1]]></Description>
	 * <PicUrl><![CDATA[picurl]]></PicUrl> <Url><![CDATA[url]]></Url> </item> </Articles>
	 * </xml>
	 * @return String
	 */
	public String toXml() {
		return XmlUtil.toXml(this);
	}

}
