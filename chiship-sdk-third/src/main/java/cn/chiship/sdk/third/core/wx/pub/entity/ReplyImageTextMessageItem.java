package cn.chiship.sdk.third.core.wx.pub.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author lijian 回复图文消息文章条目
 */
@XStreamAlias("item")
public class ReplyImageTextMessageItem {

	/**
	 * 图文消息标题
	 */
	@XStreamAlias("Title")
	public String title;

	/**
	 * 图文消息描述
	 */
	@XStreamAlias("Description")
	public String description;

	/**
	 * 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
	 */
	@XStreamAlias("PicUrl")
	public String picUrl;

	/**
	 * 点击图文消息跳转链接
	 */
	@XStreamAlias("Url")
	public String url;

	public ReplyImageTextMessageItem(String title, String description, String picUrl, String url) {
		this.title = title;
		this.description = description;
		this.picUrl = picUrl;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
