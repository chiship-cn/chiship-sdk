package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.core.util.XmlUtil;
import cn.chiship.sdk.third.core.wx.enums.WxPubMessageTypeEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复文本消息
 *
 * @author lijian
 */
@XStreamAlias("xml")
public class ReplyTextMessage extends WxBaseMessageBean {

	/**
	 * 消息内容
	 */
	@XStreamAlias("Content")
	private String content;

	/**
	 * 回复消息推荐构造器
	 * @param toUserName
	 * @param fromUserName
	 * @param content
	 */
	public ReplyTextMessage(String toUserName, String fromUserName, String content) {
		super(toUserName, fromUserName, System.currentTimeMillis(), WxPubMessageTypeEnum.WX_PUB_MESSAGE_TEXT.getType());
		this.content = content;
	}

	/**
	 * 实体转XML <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName> <CreateTime>12345678</CreateTime>
	 * <MsgType><![CDATA[text]]></MsgType> <Content><![CDATA[你好]]></Content> </xml>
	 * @return String
	 */
	public String toXml() {
		return XmlUtil.toXml(this);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
