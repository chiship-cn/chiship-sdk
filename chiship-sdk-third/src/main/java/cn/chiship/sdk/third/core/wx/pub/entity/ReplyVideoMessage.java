package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.core.util.XmlUtil;
import cn.chiship.sdk.third.core.wx.enums.WxPubMessageTypeEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author lijian 回复视频消息
 */
@XStreamAlias("xml")
public class ReplyVideoMessage extends WxBaseMessageBean {

	@XStreamAlias("Video")
	public ReplyVideoMessageItem replyVideoMessageItem;

	/**
	 * 回复消息推荐构造器
	 * @param toUserName
	 * @param fromUserName
	 * @param mediaId
	 */
	public ReplyVideoMessage(String toUserName, String fromUserName, String mediaId, String title, String description) {
		super(toUserName, fromUserName, System.currentTimeMillis(),
				WxPubMessageTypeEnum.WX_PUB_MESSAGE_VIDEO.getType());
		this.replyVideoMessageItem = new ReplyVideoMessageItem(mediaId, title, description);
	}

	/**
	 * 实体转XML <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName> <CreateTime>12345678</CreateTime>
	 * <MsgType><![CDATA[video]]></MsgType> <Video>
	 * <MediaId><![CDATA[media_id]]></MediaId> <Title><![CDATA[title]]></Title>
	 * <Description><![CDATA[description]]></Description> </Video> </xml>
	 * @return String
	 */
	public String toXml() {
		return XmlUtil.toXml(this);
	}

	public ReplyVideoMessageItem getReplyVideoMessageItem() {
		return replyVideoMessageItem;
	}

	public void setReplyVideoMessageItem(ReplyVideoMessageItem replyVideoMessageItem) {
		this.replyVideoMessageItem = replyVideoMessageItem;
	}

}
