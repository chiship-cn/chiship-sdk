package cn.chiship.sdk.third.core.wx.pub.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复视频消息MediaId
 *
 * @author lijian
 */
class ReplyVideoMessageItem {

	/**
	 * 通过素材管理中的接口上传多媒体文件，得到的id
	 */
	@XStreamAlias("MediaId")
	public String mediaId;

	/**
	 * 视频消息的标题
	 */
	@XStreamAlias("Title")
	public String title;

	/**
	 * 视频消息的描述
	 */
	@XStreamAlias("Description")
	public String description;

	public ReplyVideoMessageItem(String mediaId, String title, String description) {
		this.mediaId = mediaId;
		this.title = title;
		this.description = description;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
