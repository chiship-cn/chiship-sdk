package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.core.util.XmlUtil;
import cn.chiship.sdk.third.core.wx.enums.WxPubMessageTypeEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author lijian 回复语音消息
 */
@XStreamAlias("xml")
public class ReplyVoiceMessage extends WxBaseMessageBean {

	@XStreamAlias("Voice")
	public ReplyVoiceMessageItem replyVoiceMessageItem;

	/**
	 * 回复消息推荐构造器
	 * @param toUserName
	 * @param fromUserName
	 * @param mediaId
	 */
	public ReplyVoiceMessage(String toUserName, String fromUserName, String mediaId) {
		super(toUserName, fromUserName, System.currentTimeMillis(),
				WxPubMessageTypeEnum.WX_PUB_MESSAGE_VOICE.getType());
		this.replyVoiceMessageItem = new ReplyVoiceMessageItem(mediaId);
	}

	/**
	 * 实体转XML <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName> <CreateTime>12345678</CreateTime>
	 * <MsgType><![CDATA[voice]]></MsgType> <Voice>
	 * <MediaId><![CDATA[media_id]]></MediaId> </Voice> </xml>
	 * @return String
	 */
	public String toXml() {
		return XmlUtil.toXml(this);
	}

	public ReplyVoiceMessageItem getReplyVoiceMessageItem() {
		return replyVoiceMessageItem;
	}

	public void setReplyVoiceMessageItem(ReplyVoiceMessageItem replyVoiceMessageItem) {
		this.replyVoiceMessageItem = replyVoiceMessageItem;
	}

}
