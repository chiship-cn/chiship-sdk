package cn.chiship.sdk.third.core.wx.pub.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复语音消息MediaId
 *
 * @author lijian
 */
class ReplyVoiceMessageItem {

	/**
	 * 通过素材管理中的接口上传多媒体文件，得到的id
	 */
	@XStreamAlias("MediaId")
	public String mediaId;

	public ReplyVoiceMessageItem(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

}
