package cn.chiship.sdk.third.core.wx.pub.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 微信消息基本实体
 *
 * @author lijian
 */
public class WxBaseMessageBean {

	/**
	 * 接收方帐号->被动回复 开发者微信号->接收消息
	 */
	@XStreamAlias("ToUserName")
	private String toUserName;

	/**
	 * 开发者微信号->被动回复 接收方帐号->接收消息
	 */
	@XStreamAlias("FromUserName")
	private String fromUserName;

	/**
	 * 消息创建时间
	 */
	@XStreamAlias("CreateTime")
	private long createTime;

	/**
	 * 消息类型 默认text
	 */
	@XStreamAlias("MsgType")
	private String msgType;

	public WxBaseMessageBean(String toUserName, String fromUserName, long createTime, String msgType) {
		this.toUserName = toUserName;
		this.fromUserName = fromUserName;
		this.createTime = createTime;
		this.msgType = msgType;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
