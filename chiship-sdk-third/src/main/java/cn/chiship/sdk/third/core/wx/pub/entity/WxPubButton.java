package cn.chiship.sdk.third.core.wx.pub.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * 菜单按钮实体类
 *
 * @author lj
 */
public class WxPubButton {

	/**
	 * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
	 */
	@JSONField(name = "type")
	private String type;

	/**
	 * 菜单标题，不超过16个字节，子菜单不超过60个字节
	 */
	@JSONField(name = "name")
	private String name;

	/**
	 * 二级菜单数组，个数应为1~5个
	 */
	@JSONField(name = "sub_button")
	private List<WxPubButton> wxPubButtons;

	public WxPubButton(String name) {
		this.name = name;
	}

	public WxPubButton(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WxPubButton> getWxPubButtons() {
		return wxPubButtons;
	}

	public void setWxPubButtons(List<WxPubButton> wxPubButtons) {
		this.wxPubButtons = wxPubButtons;
	}

}
