package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.third.core.wx.enums.WxPubMenuTypeEnum;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * 单击类型的菜单实体类
 *
 * @author lj
 */
public class WxPubClickButton extends WxPubButton {

	/**
	 * 菜单KEY值，用于消息接口推送，不超过128字节
	 */
	@JSONField(name = "key")
	private String key;

	public WxPubClickButton(String name, String key) {
		super(WxPubMenuTypeEnum.WX_PUB_MENU_CLICK.getType(), name);
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
