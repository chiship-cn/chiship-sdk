package cn.chiship.sdk.third.core.wx.pub.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * 公众号菜单
 *
 * @author lijian
 */
public class WxPubMenu {

	@JSONField(name = "button")
	private List<WxPubButton> wxPubButton;

	public List<WxPubButton> getWxPubButton() {
		return wxPubButton;
	}

	public void setWxPubButton(List<WxPubButton> wxPubButton) {
		this.wxPubButton = wxPubButton;
	}

}
