package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.third.core.wx.enums.WxPubMenuTypeEnum;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author lijian x小程序类型菜单
 */
public class WxPubMpButton extends WxPubButton {

	/**
	 * 小程序的appid（仅认证公众号可配置）
	 */
	@JSONField(name = "appid")
	private String appId;

	/**
	 * 小程序的页面路径
	 */
	@JSONField(name = "pagepath")
	private String pagePath;

	/**
	 * 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
	 */
	@JSONField(name = "url")
	private String url;

	public WxPubMpButton(String name, String appId, String pagePath, String url) {
		super(WxPubMenuTypeEnum.WX_PUB_MENU_MP.getType(), name);
		this.appId = appId;
		this.pagePath = pagePath;
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

}
