package cn.chiship.sdk.third.core.wx.pub.entity;

import cn.chiship.sdk.third.core.wx.enums.WxPubMenuTypeEnum;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author lijian 浏览型菜单
 */
public class WxPubViewButton extends WxPubButton {

	/**
	 * 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
	 */
	@JSONField(name = "url")
	private String url;

	public WxPubViewButton(String name, String url) {
		super(WxPubMenuTypeEnum.WX_PUB_MENU_VIEW.getType(), name);
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
