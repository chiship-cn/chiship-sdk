package cn.chiship.sdk.third.core.wx.pub.entity.message;

/**
 * 客服发送图文消息
 *
 * @author lj
 */
public class ImageText {

	private String touser;

	private String msgtype;

	private ImageTextNews news;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public ImageTextNews getNews() {
		return news;
	}

	public void setNews(ImageTextNews news) {
		this.news = news;
	}

}
