package cn.chiship.sdk.third.core.wx.pub.entity.message;

import java.util.List;

/**
 * @author lijian 图文
 */
public class ImageTextNews {

	private List<ImageTextNewsArticles> articles;

	public List<ImageTextNewsArticles> getArticles() {
		return articles;
	}

	public void setArticles(List<ImageTextNewsArticles> articles) {
		this.articles = articles;
	}

}
