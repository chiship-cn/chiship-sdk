package cn.chiship.sdk.third.core.wx.pub.entity.message;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Map;

/**
 * @author lijian 模板消息
 */
public class TemplateMessage {

	/**
	 * 接收者openid
	 */
	@JSONField(name = "touser")
	public String toUser;

	/**
	 * 模板ID
	 */
	@JSONField(name = "template_id")
	public String templateId;

	/**
	 * 模板跳转链接
	 */
	@JSONField(name = "url")
	public String url;

	/**
	 * 小程序跳转
	 */
	@JSONField(name = "miniprogram")
	public TemplateMessageMp templateMessageMp;

	/**
	 * 数据
	 */
	@JSONField(name = "data")
	public Map<String, TemplateMessageData> templateMessageDataMap;

	/**
	 * 跳转URL
	 * @param toUser
	 * @param templateId
	 * @param url
	 * @param templateMessageDataMap
	 */
	public TemplateMessage(String toUser, String templateId, String url,
			Map<String, TemplateMessageData> templateMessageDataMap) {
		this.toUser = toUser;
		this.templateId = templateId;
		this.url = url;
		this.templateMessageDataMap = templateMessageDataMap;
	}

	/**
	 * 跳转小程序
	 * @param toUser
	 * @param templateId
	 * @param templateMessageMp
	 * @param templateMessageDataMap
	 */
	public TemplateMessage(String toUser, String templateId, TemplateMessageMp templateMessageMp,
			Map<String, TemplateMessageData> templateMessageDataMap) {
		this.toUser = toUser;
		this.templateId = templateId;
		this.templateMessageMp = templateMessageMp;
		this.templateMessageDataMap = templateMessageDataMap;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public TemplateMessageMp getTemplateMessageMp() {
		return templateMessageMp;
	}

	public void setTemplateMessageMp(TemplateMessageMp templateMessageMp) {
		this.templateMessageMp = templateMessageMp;
	}

	public Map<String, TemplateMessageData> getTemplateMessageDataMap() {
		return templateMessageDataMap;
	}

	public void setTemplateMessageDataMap(Map<String, TemplateMessageData> templateMessageDataMap) {
		this.templateMessageDataMap = templateMessageDataMap;
	}

}
