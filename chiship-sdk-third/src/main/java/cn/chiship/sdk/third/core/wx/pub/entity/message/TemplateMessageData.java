package cn.chiship.sdk.third.core.wx.pub.entity.message;

/**
 * @author lijian 模板消息数据项
 */
public class TemplateMessageData {

	public String value;

	public TemplateMessageData(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
