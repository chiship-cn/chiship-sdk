package cn.chiship.sdk.third.core.wx.pub.entity.message;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author lijian 模板消息小程序消息
 */
public class TemplateMessageMp {

	/**
	 * 所需跳转到的小程序appid
	 */
	@JSONField(name = "appid")
	public String appId;

	/**
	 * 所需跳转到的小程序页面路径
	 */
	@JSONField(name = "pagepath")
	public String pagePath;

	public TemplateMessageMp(String appId) {
		this.appId = appId;
	}

	public TemplateMessageMp(String appId, String pagePath) {
		this.appId = appId;
		this.pagePath = pagePath;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

}
