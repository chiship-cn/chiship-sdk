package cn.chiship.sdk.third.core.wx.pub.entity.message;

/**
 * @author lijian
 */
public class Text {

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
