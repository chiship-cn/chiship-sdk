package cn.chiship.sdk.third.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipDingTalkProperties.PREFIX)
public class ChishipDingTalkProperties {

	public static final String PREFIX = "chiship.third.ding-talk";

	/**
	 * Client Id
	 */
	private String appKey = "*****";

	/**
	 * Client Secret
	 */
	private String appSecret = "*****";

	/**
	 * 企业corpId
	 */
	private String corpId = "*****";

	/**
	 * 应用appId
	 */
	private String miniAppId = "*****";

	/**
	 * 原企业内部应用ID
	 */
	private String agentId = "*****";

	/**
	 * 钉钉服务主机
	 */
	private String serverDomain = "http://**************";

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getMiniAppId() {
		return miniAppId;
	}

	public void setMiniAppId(String miniAppId) {
		this.miniAppId = miniAppId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getServerDomain() {
		return serverDomain;
	}

	public void setServerDomain(String serverDomain) {
		this.serverDomain = serverDomain;
	}
}
