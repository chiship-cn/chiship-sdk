package cn.chiship.sdk.third.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipWxMiniProgramProperties.PREFIX)
public class ChishipWxMiniProgramProperties {

	public static final String PREFIX = "chiship.third.wx-mini";

	/**
	 * 小程序App Key
	 */
	private String appKey = "*****";

	/**
	 * 小程序App Secret
	 */
	private String appSecret = "*****";

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

}
