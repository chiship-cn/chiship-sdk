package cn.chiship.sdk.third.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipWxPubProperties.PREFIX)
public class ChishipWxPubProperties {

	public static final String PREFIX = "chiship.third.wx-pub";

	/**
	 * 公众号App Key
	 */
	private String appKey = "*****";

	/**
	 * 公众号App Secret
	 */
	private String appSecret = "*****";

	/**
	 * 微信公众号服务主机
	 */
	private String serverDomain = "http://**************";

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getServerDomain() {
		return serverDomain;
	}

	public void setServerDomain(String serverDomain) {
		this.serverDomain = serverDomain;
	}

}
