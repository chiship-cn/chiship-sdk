package cn.chiship.sdk.third.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipWxWorkProperties.PREFIX)
public class ChishipWxWorkProperties {

    public static final String PREFIX = "chiship.third.wx-work";

    /**
     * 企业微信企业ID
     */
    private String corpId = "*****";

    /**
     * 企业微信应用ID agentId
     */
    private String appKey = "*****";

    /**
     * 企业微信应用密钥
     */
    private String appSecret = "*****";

    /**
     * 企业微信服务主机
     */
    private String serverDomain = "http://**************";

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getServerDomain() {
        return serverDomain;
    }

    public void setServerDomain(String serverDomain) {
        this.serverDomain = serverDomain;
    }
}
