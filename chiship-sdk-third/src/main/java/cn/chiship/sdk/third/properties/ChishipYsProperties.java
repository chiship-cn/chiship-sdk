package cn.chiship.sdk.third.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipYsProperties.PREFIX)
public class ChishipYsProperties {

    public static final String PREFIX = "chiship.third.ys";

    /**
     * 萤石App Key
     */
    private String appKey = "*****";

    /**
     * 萤石App Secret
     */
    private String appSecret = "*****";

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

}
