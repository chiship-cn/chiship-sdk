package cn.chiship.sdk.third.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(ChishipZfbMpProperties.PREFIX)
public class ChishipZfbMpProperties {

    public static final String PREFIX = "chiship.third.zfb-mp";

    /**
     * 支付宝小程序appId
     */
    private String appId = "*****";

    /**
     * 支付宝小程序应用私钥
     */
    private String privateKey = "*****";

    /**
     * 支付宝公钥
     */
    private String publicKey = "*****";

    /**
     * AES解密Key
     */
    private String aesDecryptKey;

    /**
     * 支付宝小程序服务主机
     */
    private String serverDomain = "http://**************";

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAesDecryptKey() {
        return aesDecryptKey;
    }

    public void setAesDecryptKey(String aesDecryptKey) {
        this.aesDecryptKey = aesDecryptKey;
    }

    public String getServerDomain() {
        return serverDomain;
    }

    public void setServerDomain(String serverDomain) {
        this.serverDomain = serverDomain;
    }
}
