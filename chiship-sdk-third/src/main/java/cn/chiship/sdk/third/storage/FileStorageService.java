package cn.chiship.sdk.third.storage;

import cn.chiship.sdk.core.base.BaseResult;

import java.io.InputStream;
import java.util.List;

/**
 * 文件存储操作接口定义
 *
 * @author lj
 */
public interface FileStorageService {

	/**
	 * 通过流上传文件，指定文件名
	 * @param inputStream 文件流
	 * @param fileName 文件名称
	 * @return 操作结果
	 */
	BaseResult upload(InputStream inputStream, String fileName);

	/**
	 * 通过流上传文件，指定文件名和文件夹名称
	 * @param inputStream 文件流
	 * @param catalog 文件存储目录
	 * @param fileName 文件名称
	 * @return 操作结果
	 */
	BaseResult upload(InputStream inputStream, String catalog, String fileName);

	/**
	 * 通过base64上传文件，指定文件名和Base64
	 * @param fileName 文件名称
	 * @param base64 文件base64
	 * @return 操作结果
	 */
	BaseResult uploadBase64(String base64, String fileName);

	/**
	 * 通过base64上传文件，指定文文件名、Base64及文件目录
	 * @param base64 文件base64
	 * @param catalog 文件存储目录
	 * @param fileName 文件名称
	 * @return 操作结果
	 */
	BaseResult uploadBase64(String base64, String catalog, String fileName);

	/**
	 * 根据文件名删除文件
	 * @param fileName 文件名称
	 * @return 操作结果
	 */
	BaseResult removeFile(String fileName);

	/**
	 * 根据文件名列表批量删除文件
	 * @param fileNames 文件名称集合
	 * @return 操作结果
	 */
	BaseResult removeFiles(List<String> fileNames);

}
