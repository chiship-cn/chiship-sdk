package cn.chiship.sdk.third.storage.enums;

/**
 * @author lijian 分布式存储工厂类枚举 ，因dfs表存的是数据字典表的id，这里省一次数据库查询，所以就用数据字典的id
 */
public enum DfsClassEnum {

	/**
	 * 阿里云
	 */
	ALI(1, "cn.chiship.sdk.third.storage.factory.AliOssFactory"),

	/**
	 * 腾讯云
	 */
	TENCENT(2, "cn.chiship.sdk.third.storage.factory.TencentCosFactory"),

	;

	public Integer code;

	public String value;

	DfsClassEnum(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static String getValue(Integer code) {
		DfsClassEnum[] smsFactoryClassEnums = values();
		for (DfsClassEnum smsFactoryClassEnum : smsFactoryClassEnums) {
			if (smsFactoryClassEnum.getCode().equals(code)) {
				return smsFactoryClassEnum.getValue();
			}
		}
		return null;
	}

	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
