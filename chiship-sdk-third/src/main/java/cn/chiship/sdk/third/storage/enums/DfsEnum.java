package cn.chiship.sdk.third.storage.enums;

/**
 * @author lijian DfsEnum 分布式存储枚举 ，因dfs表存的是数据字典表的id，这里省一次数据库查询，所以就用数据字典的id
 */
public enum DfsEnum {

	/**
	 * 阿里云
	 */
	ALI(1),

	/**
	 * 腾讯云
	 */
	TENCENT(2),

	;

	public Integer value;

	DfsEnum(Integer value) {
		this.value = value;
	}

	/**
	 * 根据标识返回枚举
	 * @param value 标识
	 * @return DfsEnum
	 */
	public static DfsEnum getDfsEnum(Integer value) {
		DfsEnum[] dfsEnums = values();
		for (DfsEnum dfsEnum : dfsEnums) {
			if (dfsEnum.getValue().equals(value)) {
				return dfsEnum;
			}
		}
		return null;
	}

	public Integer getValue() {
		return value;
	}

}
