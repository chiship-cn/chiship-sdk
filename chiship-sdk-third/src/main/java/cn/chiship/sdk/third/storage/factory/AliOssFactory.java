package cn.chiship.sdk.third.storage.factory;

import cn.chiship.sdk.third.core.model.DfsDTO;
import cn.chiship.sdk.third.core.model.ali.AliOssConfigModel;
import cn.chiship.sdk.third.storage.FileStorageService;
import cn.chiship.sdk.third.storage.impl.AliOssServiceImpl;

/**
 * 阿里云上传服务接口工厂类
 *
 * @author lj
 */
public class AliOssFactory {

	private AliOssFactory() {
	}

	/**
	 * 默认配置形式生成对象
	 * @return FileStorageService
	 */
	public static FileStorageService getFileStorageService() {
		return new AliOssServiceImpl();
	}

	/**
	 * 使用指定参数配置生成对象
	 * @param dfsDTO 分布式存储配置
	 * @return FileStorageService
	 */
	public static FileStorageService getFileStorageService(DfsDTO dfsDTO) {
		AliOssConfigModel aliOssConfigModel = new AliOssConfigModel();
		aliOssConfigModel.setAppKey(dfsDTO.getAppKey());
		aliOssConfigModel.setAppSecret(dfsDTO.getAppSecret());
		aliOssConfigModel.setOssEndPort(dfsDTO.getEndPort());
		aliOssConfigModel.setBuckName(dfsDTO.getBuckName());
		aliOssConfigModel.setRoot(dfsDTO.getRoot());

		return new AliOssServiceImpl(aliOssConfigModel);
	}

}
