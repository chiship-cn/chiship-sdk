package cn.chiship.sdk.third.storage.factory;

import cn.chiship.sdk.third.core.model.DfsDTO;
import cn.chiship.sdk.third.core.model.tencent.TencentCosConfigModel;
import cn.chiship.sdk.third.storage.FileStorageService;
import cn.chiship.sdk.third.storage.impl.TencentCosServiceImpl;

/**
 * 腾讯云上传服务接口工厂类
 *
 * @author lj
 */
public class TencentCosFactory {

	private TencentCosFactory() {
	}

	/**
	 * 默认配置形式生成对象
	 * @return FileStorageService
	 */
	public static FileStorageService getFileStorageService() {
		return new TencentCosServiceImpl();
	}

	/**
	 * 使用指定参数配置生成对象
	 * @param dfsDTO 分布式存储配置
	 * @return FileStorageService
	 */
	public static FileStorageService getFileStorageService(DfsDTO dfsDTO) {
		TencentCosConfigModel tencentCosConfigModel = new TencentCosConfigModel();
		tencentCosConfigModel.setAppKey(dfsDTO.getAppKey());
		tencentCosConfigModel.setAppSecret(dfsDTO.getAppSecret());
		tencentCosConfigModel.setOssEndPort(dfsDTO.getEndPort());
		tencentCosConfigModel.setBuckName(dfsDTO.getBuckName());
		tencentCosConfigModel.setRoot(dfsDTO.getRoot());

		return new TencentCosServiceImpl(tencentCosConfigModel);
	}

}
